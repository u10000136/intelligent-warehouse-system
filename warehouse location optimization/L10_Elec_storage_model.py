import os
from datetime import datetime, timedelta
from fii_ai_api.utils.dbio.mysql import BasicMySQL
from stock.config import *
import pandas as pd
import numpy as np
import re


# 選取儲位優化時間 function
def SELECT_TIME_in_db():
    y = datetime.now().year
    m = datetime.now().month
    d = datetime.now().day

    if m % 3 == 0:
        if m != 12:
            if d < 28:
                re_m = m - 3
            else:
                re_m = m
        else:
            if d < 28:
                re_m = m - 3
            else:
                re_m = 0
    else:
        re_m = (m // 3) * 3
    re_m += 1

    return datetime(y, re_m, 1) + timedelta(-1)

def generate_time_sql(time_col='WORK_TIME'):
    y = datetime.now().year
    m = datetime.now().month
    if m // 3 == 0:
        y = y - 1
        m = 12
    else:
        season = m // 3
        m = season * 3
        
    if m == 2:
        result_y = y - 1
        result_m = 12
    elif m == 1:
        result_y = y - 1
        result_m = 11
    else:
        result_y = y
        result_m = m - 2
    time_result = datetime(result_y, result_m, 1)
    result = "and %(WORK_TIME)s >= '%(time)s' "% ({'WORK_TIME': time_col, 'time': time_result})
    return result

# 儲位 class
class location_space():
    # input location_name  , cust_no, no_of_cust
    def __init__(self, location_name, rack_id, row_level, col_level,
                 storage_space, fre_ratio, frequency, charge_emp):
        self.location_name = location_name # 貨架 / 料車 id
        self.rack_id = rack_id
        self.row_level = row_level
        self.col_level = col_level
        self.storage_space = storage_space
        self.fre_ratio = fre_ratio
        self.frequency = frequency
        self.charge_emp = charge_emp

    def show_information(self):
        print('location_name:', self.location_name)
        print('rack_id:', self.rack_id)
        print('row_level:', self.row_level)
        print('col_level:', self.col_level)
        print('fre_ratio:', self.fre_ratio)
        print('frequency:', self.frequency)
        print('storage_space:', self.storage_space)
        print('charge_emp:', self.charge_emp)

# 料車 class
class car_space():
    # input 料車名稱 
    def __init__(self, storage_name, priority, row_level, column_level, row_level_list):
        self.storage_name = storage_name
        self.priority = priority
        self.row_level = row_level
        self.column_level = column_level
        self.row_level_list = row_level_list
    
    def show_information(self):
        print('storage_name:', self.storage_name)
        print('row_level:', self.row_level)
        print('column_level:', self.column_level)
        print('row_level_list:', self.row_level_list)
        
# 貨架 class
class rack_space():
    # input 貨架名稱 storage_name, priority, row_level, column_level, row_level_list
    def __init__(self, storage_name, priority, row_level, column_level, row_level_list):
        self.storage_name = storage_name
        self.priority = priority
        self.row_level = row_level
        self.column_level = column_level
        self.row_level_list = row_level_list
    
    def show_information(self):
        print('storage_name:', self.storage_name)
        print('row_level:', self.row_level)
        print('column_level:', self.column_level)
        print('row_level_list:', self.row_level_list)

# 料段 class
class material_section(): # section_name, component_list
    # input 料段名稱
    def __init__(self, section_name, component_list):
        self.section_name = section_name
        self.component_list = component_list
    
    def show_information(self):
        print(self.section_name)
        print(self.component_list)

''' class above this line '''
# for sql
def cust_lst2sql(lst):
    times = len(lst) // 1000
    if len(lst) != 0:
        if len(lst) % 1000 != 0:
            times += 1
        cust_sql = 'and ('
        for i in range(times):
            index = 1000 * i
            cust_sql += 'CUST_KP_NO in (%(cust_sql)s) or ' % ({'cust_sql': str(lst[index: index + 1000])[1:-1]})
        cust_sql = cust_sql[:-3]
        cust_sql += ')'
    else:
        cust_sql = "and CUST_KP_NO in ('')"
    return cust_sql

# for sql
def last_three_month_sql():
    y = datetime.now().year
    m = datetime.now().month
    if m // 3 == 0:
        y = y - 1
        m = 12
    else:
        season = m // 3
        m = season * 3
    if m == 2:
        y2 = y
        m2 = 1
        y3 = y - 1
        m3 = 12
    elif m == 1:
        y2 = y - 1
        m2 = 12
        y3 = y - 1
        m3 = 11
    else:
        y2 = y
        y3 = y
        m2 = m - 1
        m3 = m - 2

    sql = ''' and (YEAR = %(y1)s and MONTH = %(m1)s )
            or (YEAR = %(y2)s and MONTH = %(m2)s ) 
            or (YEAR = %(y3)s and MONTH = %(m3)s ) 
        ''' % ({'y1': y, 'm1': m,
                'y2': y2, 'm2': m2,
                'y3': y3, 'm3': m3})
    return sql

# 生成儲位時間
def create_TIME_in_db():
    y = datetime.now().year
    m = datetime.now().month
    if m // 3 == 0:
        y = y
        m = 1
    else:
        season = m // 3
        m = season * 3
        m += 1
    
    return datetime(y, m, 1) + timedelta(-1)

# # 計算3個月各料段之物料使用頻率
# def cust_usage_frquency(location_data, result):
#     all_cust = location_data[location_data.LOCATION.str.startswith(tuple(result))].sort_values('KEEPER_EMP')
#     all_cust.loc[:, 'RATIO_IN_EMP'] = 1 - all_cust.groupby(['KEEPER_EMP'])['TOTAL_TIMES'].rank(pct=True)
#     return all_cust

''' 處理儲位字串'''
# 前處理儲位 for rack 
# 1: double- /2:rack_level contain 0  /3:column_level not having two digit /4 :contain other useless location 
def clean_location(x):
    # case 1
    if '--' in x:
        i = x.index('--')
        x = x[0:i] + x[i+1:]
    # case2
    if x[5] == '0':
        x = x[0:5] + x[6:]
    # case3
    regex = re.compile(r'-\d+-(\d*)')
    try:
        i = regex.search(x).group(1)
        if len(i) == 1:
            x = x[:7] + '0' + x[7:9] + '0' + x[9:]
        # case4 
        if 'Q' in x:
            i = x.index('Q')
            x = x[: (i-1)]
        return x
    except:
        return 'error'

# for car
def clean_location_car_version(x):
    # case 1
    if '--' in x:
        i = x.index('--')
        x = x[0:i] + x[i+1:]
    if ';' in x:
        i = x.index(';')
        x = x[0:i] + x[i+1:]
    if '.' in x:
        i = x.index('.')
        x = x[0:i] + x[i+1:]
    return x

# for rack
def calculate_location_space_rack_version(rack_df):
    rack_df.loc[:, 'space'] = rack_df.LOCATION.str.slice(7, 20).apply(lambda x : re.findall(r"\d+\.?\d*", x))
    rack_df.loc[:, 'space'] = rack_df.space.apply(lambda x : list(map(int, x)))
    rack_df.loc[:, 'temp_space'] = rack_df.space.apply(lambda x: len(x))
    rack_df.loc[:, 'LOCATION_SPACE'] = rack_df.space.apply(lambda x: max(x) - min(x) + 1)

    rack_df = rack_df[rack_df.LOCATION_SPACE < 3]

    return rack_df.drop(columns=['L_LOCATION', 'space', 'temp_space'])

# for car
def calculate_location_space_car_version(car_df):
    car_df.loc[:, 'space'] = car_df.LOCATION.str.slice(3, 20).apply(lambda x : re.findall(r"\d+\.?\d*", x))
    car_df.loc[:, 'space'] = car_df.space.apply(lambda x : list(map(int, x)))
    car_df.loc[:, 'temp_space'] = car_df.space.apply(lambda x: len(x))
    car_df.loc[:, 'LOCATION_SPACE'] = car_df.space.apply(lambda x: max(x) - min(x) + 1)

    car_df = car_df[car_df.LOCATION_SPACE < 3]

    return car_df.drop(columns=['L_LOCATION', 'space', 'temp_space'])


''' 計算儲位使用空間數量'''

def create_rack_and_car_df(season_frquency, storage_space):
    rack_l = storage_space[storage_space.VALID_LOCATION.str.startswith('BC')]
    car_l = storage_space[-storage_space.VALID_LOCATION.str.startswith('BC')]

    BC = list(rack_l.VALID_LOCATION.str.slice(0,4).unique())
    result = list(car_l.VALID_LOCATION.unique())
    result += BC
    # 原本是計算ratio 但後改為
    season_frquency = season_frquency[season_frquency.LOCATION.str.startswith(tuple(result))]
#     season_frquency = cust_usage_frquency(season_frquency.reset_index(drop=True), result)

    # 區分擺放類型
    rack_cust = season_frquency[season_frquency.LOCATION.str.startswith('BC')]
    car_cust = season_frquency[-season_frquency.LOCATION.str.startswith('BC')]

    ## 處理貨架區
    rack_cust.loc[:, 'LOCATION'] = rack_cust.LOCATION.apply(lambda x: clean_location(x))#.LOCATION.unique()
    rack_cust = rack_cust[-rack_cust.LOCATION.isin(['error'])]
    rack_cust.loc[:, 'L_LOCATION'] = rack_cust.LOCATION.str.slice(0, 9)
    temp_df = pd.merge(rack_cust, rack_l, left_on='L_LOCATION', right_on='VALID_LOCATION', how='left')
    rack_df = temp_df[-temp_df.PRIORITY.isna()]

    # 處理掛車區
    car_cust.loc[:, 'LOCATION'] = car_cust.LOCATION.apply(lambda x: clean_location_car_version(x))
    car_cust.loc[:, 'L_LOCATION'] = car_cust.LOCATION.str.slice(0, 3)
    temp_df = pd.merge(car_cust, car_l, left_on='L_LOCATION', right_on='VALID_LOCATION', how='left')
    car_df = temp_df[-temp_df.PRIORITY.isna()]
    searchfor = ['PTH', '2F']
    car_df = car_df[-car_df.LOCATION.str.contains('|'.join(searchfor))]

    # regular expression 
    rack_df = calculate_location_space_rack_version(rack_df)
    car_df = calculate_location_space_car_version(car_df)
    
    # 計算各儲位 ratio
    all_cust_df = pd.concat([rack_df, car_df])
    all_cust_df.loc[:, 'RATIO_IN_EMP'] = 1 - all_cust_df.groupby(['KEEPER_EMP'])['TOTAL_USE_TIMES'].rank(pct=True)
    
    rack_df = all_cust_df[all_cust_df.VALID_LOCATION.str.startswith('BC')]
    car_df = all_cust_df[-all_cust_df.VALID_LOCATION.str.startswith('BC')]
    
    return rack_df, car_df

def range_compression(x, max_val=10000):
    return np.log10(x) / np.log10(max_val)

def normalize_rack_location_info(x):
    try:
        location = re.findall(r"(\d+\.?\d*)", x[5:9])
        row_level = int(location[0])
        col_loc = int(location[1]) - 1
        col_level = col_loc % 4 + 1
        rack_id = x[:4] + "'" + str(col_loc // 4 + 1)
    except:
        print(x)
        print(location)
    return rack_id, row_level, col_level

# 找出前N % 物料
def NSD_D10_elec_storage_heatmap_p(percentile):
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    month_sql = last_three_month_sql()
    sql = '''
    SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
        FROM NSD_L10_D_material_storage_cust_usage_frquency
    where 1=1 %(month_sql)s

    '''% ({'month_sql' : month_sql})
    cust_frquency = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    season_frquency = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].sum().reset_index()
    
    # 篩選決策物料料號
    sql = '''
    SELECT
        KEEPER_EMP, CUST_KP_NO,
        ORIGIN_LOCATION, original_TOTAL_TIMES, original_TOTAL_USE_TIMES,
        change_CUST_KP_NO,
        IMPROVE_LOCATION, improved_TOTAL_TIMES, improved_TOTAL_USE_TIMES,
        TIME, UPDATE_TIME
    FROM
        NSD_L10_D_material_storage_space_decicion_result
    WHERE
        CONDITION_USAGE_RATIO = %(CONDITION_USAGE_RATIO)s
        and TIME = (SELECT max(TIME) FROM NSD_L10_D_material_storage_space_decicion_result)
    '''% ({'CONDITION_USAGE_RATIO' : percentile})
    de_result = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    cust_list = de_result.CUST_KP_NO.unique()
    season_frquency = season_frquency[season_frquency.CUST_KP_NO.isin(cust_list)]
    
    # 若沒有結果直接回傳空df
    if len(season_frquency) == 0:
        null_df = pd.DataFrame(columns=['KEEPER_EMP', 'LOC_ID', 'TOTAL_TIMES', 'TOTAL_USE_TIMES', 'NO_of_CUST',
                       'MAX_IN_EMP', 'maxcolor_in_emp', 'loc_color'])
        return null_df

    # 讀取儲位順位
    sql = '''
    SELECT VALID_LOCATION, PRIORITY
        FROM NSD_L10_D_material_storage_space
    '''
    storage_space = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    # 處理 貨架 與 料車區
    # 現在為未計算 RATIO_IN_EMP
    rack_df, car_df = create_rack_and_car_df(season_frquency, storage_space)

    # 找儲位有多少料
    loc_cust = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP', 'CUST_KP_NO'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].sum().reset_index()
    loc_cust = loc_cust.groupby(['LOCATION'])['CUST_KP_NO'].count().reset_index().sort_values('CUST_KP_NO')
    loc_cust = loc_cust.rename(columns={'CUST_KP_NO': 'NO_of_CUST'})

    car_df.loc[:, 'LOC_ID'] = car_df.VALID_LOCATION
    rack_df.loc[:, 'LOC_ID'] = rack_df.VALID_LOCATION.apply(lambda x :normalize_rack_location_info(x)[0])

    rack_005 = rack_df[rack_df.RATIO_IN_EMP < percentile] #2369 > 153
    car_005 = car_df[car_df.RATIO_IN_EMP < percentile]  # 5391 > 248
    # 合併貨架料車儲位 合併
    heat_map_df = pd.concat([rack_005, car_005]).reset_index(drop=True)
    heat_map_df = pd.merge(heat_map_df, loc_cust, on='LOCATION',how='left')

    heat_map = heat_map_df.groupby(['KEEPER_EMP', 'LOC_ID'])['TOTAL_TIMES', 'TOTAL_USE_TIMES', 'NO_of_CUST'].sum().reset_index()
    
    # heat_map前處理 計算料段最大出庫次數 計算熱圖顏色深度
    max_df = heat_map.groupby(['KEEPER_EMP'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].max().reset_index()
    max_val = max_df.TOTAL_USE_TIMES.max()
    max_df.loc[:, 'maxcolor_in_emp'] = max_df.TOTAL_USE_TIMES.apply(lambda x: range_compression(x,max_val))
    max_df = max_df.rename(columns={'TOTAL_USE_TIMES':'MAX_IN_EMP'})

    # 合併料段最大工作次數
    heat_map = pd.merge(heat_map, max_df, on='KEEPER_EMP')
    heat_map.loc[:, 'loc_color'] = np.where(1, heat_map['TOTAL_USE_TIMES'] / heat_map['MAX_IN_EMP'] * heat_map['maxcolor_in_emp'], -10000)

    return heat_map

# 找出出庫次數 超過 N 次物料
def NSD_D10_elec_storage_heatmap_f(threshold):
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    month_sql = last_three_month_sql()
    sql = '''
    SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
        FROM NSD_L10_D_material_storage_cust_usage_frquency
    where 1=1 %(month_sql)s

    '''% ({'month_sql' : month_sql})
    cust_frquency = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    season_frquency = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].sum().reset_index()
    
    # 篩選決策物料料號
    sql = '''
    SELECT
        KEEPER_EMP, CUST_KP_NO,
        ORIGIN_LOCATION, original_TOTAL_TIMES, original_TOTAL_USE_TIMES,
        change_CUST_KP_NO,
        IMPROVE_LOCATION, improved_TOTAL_TIMES, improved_TOTAL_USE_TIMES,
        TIME, UPDATE_TIME
    FROM
        NSD_L10_D_material_storage_space_decicion_result
    WHERE
        CONDITION_USAGE_TIME = %(CONDITION_USAGE_TIME)s
        and TIME = (SELECT max(TIME) FROM NSD_L10_D_material_storage_space_decicion_result)

    ''' % ({'CONDITION_USAGE_TIME' : threshold})
    de_result = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    cust_list = de_result.CUST_KP_NO.unique()
    season_frquency = season_frquency[season_frquency.CUST_KP_NO.isin(cust_list)]

    # 若沒有結果直接回傳空df
    if len(season_frquency) == 0:
        null_df = pd.DataFrame(columns=['KEEPER_EMP', 'LOC_ID', 'TOTAL_TIMES', 'TOTAL_USE_TIMES', 'NO_of_CUST',
                       'MAX_IN_EMP', 'maxcolor_in_emp', 'loc_color'])
        return null_df

    # 讀取儲位順位
    sql = '''
    SELECT VALID_LOCATION, PRIORITY
        FROM NSD_L10_D_material_storage_space
    '''
    storage_space = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    # 處理 貨架 與 料車區
    rack_df, car_df = create_rack_and_car_df(season_frquency, storage_space)

    # 找儲位有多少料
    loc_cust = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP', 'CUST_KP_NO'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].sum().reset_index()
    loc_cust = loc_cust.groupby(['LOCATION'])['CUST_KP_NO'].count().reset_index().sort_values('CUST_KP_NO')
    loc_cust = loc_cust.rename(columns={'CUST_KP_NO': 'NO_of_CUST'})

    car_df.loc[:, 'LOC_ID'] = car_df.VALID_LOCATION
    rack_df.loc[:, 'LOC_ID'] = rack_df.VALID_LOCATION.apply(lambda x :normalize_rack_location_info(x)[0])

    # 以次數進行篩選
    thres = threshold
    heat_map_df = pd.concat([rack_df, car_df]).reset_index(drop=True)
    heat_map_df = heat_map_df[heat_map_df.TOTAL_USE_TIMES > thres]

    # 合併貨架料車儲位 合併
    heat_map_df = pd.merge(heat_map_df, loc_cust, on='LOCATION',how='left')

    heat_map = heat_map_df.groupby(['KEEPER_EMP', 'LOC_ID'])['TOTAL_TIMES', 'TOTAL_USE_TIMES', 'NO_of_CUST'].sum().reset_index()

    # heat_map前處理 計算料段最大出庫次數 計算熱圖顏色深度
    max_df = heat_map.groupby(['KEEPER_EMP'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].max().reset_index()
    max_val = max_df.TOTAL_USE_TIMES.max()
    max_df.loc[:, 'maxcolor_in_emp'] = max_df.TOTAL_USE_TIMES.apply(lambda x: range_compression(x,max_val))
    max_df = max_df.rename(columns={'TOTAL_USE_TIMES':'MAX_IN_EMP'})

    # 合併料段最大工作次數
    heat_map = pd.merge(heat_map, max_df, on='KEEPER_EMP')
    heat_map.loc[:, 'loc_color'] = np.where(1, heat_map['TOTAL_USE_TIMES'] / heat_map['MAX_IN_EMP'] * heat_map['maxcolor_in_emp'], -10000)

    return heat_map

''' 儲位 標準化 function'''
'''car'''
def normalize_car_location_info(x):
    try:
        res = re.findall(r"(\d+?\d*)", x)
        car_id = x[:3]
        loc = int(res[1])
        if int(car_id[1:3]) % 3 == 0:
            pre_col = loc % 9
            if pre_col == 0:
                col_level = 9
                row_level = loc // 9
            else:
                col_level = pre_col
                row_level = (loc // 9) + 1
        else:
            pre_col = loc % 7
            if pre_col == 0:
                col_level = 7
                row_level = loc // 7
            else:
                col_level = pre_col
                row_level = (loc // 7) + 1
        return car_id, row_level, col_level
    except:
        print(x)
        return 'NO_SPACE'

def generate_car_section_dict(car_df):
    car_df.loc[:, 'NORM_LOCATION'] = car_df.LOCATION.apply(lambda x : normalize_car_location_info(x))
    car_df.loc[:, 'car_id'] = car_df.NORM_LOCATION.apply(lambda x: x[0])
    car_df.loc[:, 'row_level'] = car_df.NORM_LOCATION.apply(lambda x: x[1])
    car_df.loc[:, 'col_level'] = car_df.NORM_LOCATION.apply(lambda x: x[2])
    car_df = car_df[-car_df.NORM_LOCATION.isin(['NO_SPACE'])]
    car_emp = car_df.KEEPER_EMP.unique()

    # 找出第j個貨架
    D10_location_car_dict = {}
    for i in car_emp:
        t = car_df[car_df.KEEPER_EMP.isin([i])]

        priority_dict = {}
        for i_pri in [3, 2, 1]:
            pri_t = t[t.PRIORITY.isin([i_pri])]
            Priority = 'P' + str(i_pri)
            cars_id = np.sort(pri_t.car_id.unique())

            car_dict = {}
            for j in cars_id:
                tt = pri_t[pri_t.car_id.isin([j])]
                # row_level_space(): #row_name, row_limit, location_list
                rows_id = np.sort(tt.row_level.unique())

                row_dict = {}
                for k in rows_id:
                    ttt = tt[tt.row_level.isin([k])]
                    cols_id = np.sort(ttt.col_level.unique())

                    col_dict = {}
                    for l in cols_id:
                        # location_info
                        l_inf = ttt[ttt.col_level.isin([l])]

                        col_dict[l] = location_space(location_name = l_inf.values[0][0],
                                                     rack_id = l_inf.values[0][8], 
                                                     row_level = l_inf.values[0][9],
                                                     col_level = l_inf.values[0][10],
                                                     storage_space = l_inf.values[0][6],
                                                     fre_ratio = l_inf.values[0][3],
                                                     frequency = l_inf.values[0][2],
                                                     charge_emp = l_inf.values[0][1])

                    row_dict[k] = col_dict
                if int(j[1:]) % 3 == 0:
                    col_num = 9
                else:
                    col_num = 7
                    
                j_rack = car_space(j, 1, 7, col_num, row_dict)
                car_dict[j] = j_rack

            priority_dict[Priority] = car_dict
        i_emp = material_section(i, priority_dict)
        D10_location_car_dict[i] = i_emp

    return D10_location_car_dict

def change_space_upper(rack_class, fre_ratio_=0.05, location_space=1):
    re_freq = 10000000
    re_loc = ''
    if 8 in rack_class.row_level_list.keys():
        key_list = list(rack_class.row_level_list.keys())[:-1]
    else:
        key_list = list(rack_class.row_level_list.keys())
    for i in key_list:
        for j in rack_class.row_level_list[i].keys():
            if rack_class.row_level_list[i][j].storage_space == location_space:
                if rack_class.row_level_list[i][j].fre_ratio > fre_ratio_:
                    if rack_class.row_level_list[i][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[i][j].frequency
                        re_loc = rack_class.row_level_list[i][j].location_name
    
    return re_loc, re_freq

def change_space_lower(rack_class, fre_ratio_=0.05, location_space=1):
    re_freq = 10000000
    re_loc = ''
    if 8 in rack_class.row_level_list.keys():
        key_list = list(rack_class.row_level_list.keys())[-1]
        for j in rack_class.row_level_list[8].keys():
            if rack_class.row_level_list[8][j].storage_space == location_space:
                if rack_class.row_level_list[8][j].fre_ratio > fre_ratio_:
                    if rack_class.row_level_list[8][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[8][j].frequency
                        re_loc = rack_class.row_level_list[8][j].location_name

    return re_loc, re_freq


# 輸入料段 找出 P1區 使用頻率最小儲位
def search_space_upper(section_class, fre_ratio_=0.05, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space_upper(x[searching_space][rack_], fre_ratio_, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc
    return re_loc, re_freq

# 輸入料段 找出 P1區 使用頻率最小儲位
def search_space_lower(section_class, fre_ratio_=0.05, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space_lower(x[searching_space][rack_], fre_ratio_, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc
    return re_loc, re_freq

# for car and rack
def change_location_df_ratio_version(_005_cust, P1_cust, ratio):
    result = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    new = {'CONDITION_USAGE_RATIO': ratio,
           'IMPROVE_LOCATION' : P1_cust,
           'ORIGIN_LOCATION' : _005_cust}
    _old = {'CONDITION_USAGE_RATIO': ratio,
           'IMPROVE_LOCATION' : _005_cust,
           'ORIGIN_LOCATION' : P1_cust}
    # result.append(new)
    rows_list = []
    rows_list.append(new)
    rows_list.append(_old)
    # result = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])

    app = pd.DataFrame(rows_list) 
    result = result.append(app,sort=True)
    return result

# 生成car區換料決策 location
def location_decision_function_car_version(D10_location_car_dict, car_df, ratio=0.05):
    # input emp_class / test_df(emp, ratio < 0.05, )
    dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    for emp in D10_location_car_dict.keys():
        emp_class = D10_location_car_dict[emp]
        
        # 抓出前 N % 使用頻率物料
        test_df = car_df[(car_df.KEEPER_EMP == emp) &
                (car_df.RATIO_IN_EMP < ratio) &
                (car_df.PRIORITY > 1)].sort_values(['PRIORITY','RATIO_IN_EMP'], ascending=[False, True])

        _005_cust_info = test_df.to_dict('r')

        pre_dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
        
        for i in range(len(test_df)):
            i_cust_space = _005_cust_info[i]['LOCATION_SPACE']

            i_cust_loc = _005_cust_info[i]['LOCATION']
            i_cust_fre = _005_cust_info[i]['TOTAL_USE_TIMES']
            i_cust_fre_ratio = _005_cust_info[i]['RATIO_IN_EMP']
            i_cust_priority = _005_cust_info[i]['PRIORITY']
            loc_005 = re.findall(r"(\d+?\d*)", i_cust_loc)
            temp_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
            
            if i_cust_space == 1: # 針對P2/P3高頻物料 移至P1做優化
                # 找 p1中SPACE == 1之使用頻率最小儲位

                ''' 若是在最下層 ，則只搜索最下層物料
                若是再上層(col*row)範圍內， 則搜索上層'''
                # 在P3區 且lower層
                P3is_lower = (int(loc_005[0]) % 3 == 0) & (int(loc_005[1]) > 63)
                P2is_lower = (int(loc_005[0]) % 3 != 0) & (int(loc_005[1]) > 49)
                
                
                if (P3is_lower) | (P2is_lower):
                    re_loc, re_freq = search_space_lower(emp_class, fre_ratio_=ratio, searching_space='P1', location_space=1)
                else:
                    re_loc, re_freq = search_space_upper(emp_class, fre_ratio_=ratio, searching_space='P1', location_space=1)
                    
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                    # 替換儲位 fre, fre_ratio 資訊

                    loc = normalize_car_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre
                        P1_cust.fre_ratio = i_cust_fre_ratio
                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    if (P3is_lower) | (P2is_lower):
                        re_loc, re_freq = search_space_lower(emp_class, fre_ratio_=ratio, searching_space='P2', location_space=1)
                    else:
                        re_loc, re_freq = search_space_upper(emp_class, fre_ratio_=ratio, searching_space='P2', location_space=1)
                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_car_location_info(re_loc)
                        if loc != 'NO_SPACE':

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre
                            P2_cust.fre_ratio = i_cust_fre_ratio
                        
                        
            elif i_cust_space == 2: # 針對P2/P3高頻物料 移至P1做優化\
                # 確認是否為下層 若為下層 則
                P3is_lower = (int(loc_005[0]) % 3 == 0) & (int(loc_005[1]) > 63)
                P2is_lower = (int(loc_005[0]) % 3 != 0) & (int(loc_005[1]) > 49)
                if (P3is_lower) | (P2is_lower):
                    continue
                else:
                    re_loc, re_freq = search_space_upper(emp_class, fre_ratio_=ratio, searching_space='P1', location_space=2)
                    
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                    # 替換儲位 fre, fre_ratio 資訊

                    loc = normalize_car_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre
                        P1_cust.fre_ratio = i_cust_fre_ratio
                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    if (P3is_lower) | (P2is_lower):
                        continue
                    else:
                        re_loc, re_freq = search_space_upper(emp_class, fre_ratio_=ratio, searching_space='P2', location_space=2)
                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_car_location_info(re_loc)
                        if loc != 'NO_SPACE':

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre
                            P2_cust.fre_ratio = i_cust_fre_ratio
                    
            pre_dicision_df = pre_dicision_df.append(temp_df)
        dicision_df = dicision_df.append(pre_dicision_df)
        # dicision_df # 需去除空白 / 去除space ==2
    return dicision_df.reset_index(drop=True)

'''rack'''
# rack
def normalize_rack_location_info(x):
    try:
        location = re.findall(r"(\d+\.?\d*)", x[5:9])
        row_level = int(location[0])
        col_loc = int(location[1]) - 1
        col_level = col_loc % 4 + 1
        rack_id = x[:4] + "'" + str(col_loc // 4 + 1)
        return rack_id, row_level, col_level
    except:
        print(x)
        print(location)
        return 'NO_SPACE'

def generate_rack_section_dict(rack_df):
    rack_df.loc[:, 'NORM_LOCATION'] = rack_df.VALID_LOCATION.apply(lambda x : normalize_rack_location_info(x))

    rack_df.loc[:, 'rack_id'] = rack_df.NORM_LOCATION.apply(lambda x: x[0])
    rack_df.loc[:, 'row_level'] = rack_df.NORM_LOCATION.apply(lambda x: x[1])
    rack_df.loc[:, 'col_level'] = rack_df.NORM_LOCATION.apply(lambda x: x[2])
    rack_df = rack_df[-rack_df.NORM_LOCATION.isin(['NO_SPACE'])]
    rack_emp = rack_df.KEEPER_EMP.unique()

    # 找出第j個貨架
    D10_location_dict = {}
    for i in rack_emp:
        t = rack_df[rack_df.KEEPER_EMP.isin([i])]

        priority_dict = {}
        for i_pri in [3, 2, 1]:
            pri_t = t[t.PRIORITY.isin([i_pri])]
            Priority = 'P' + str(i_pri)
            racks_id = np.sort(pri_t.rack_id.unique())

            rack_dict = {}
            for j in racks_id:
                tt = pri_t[pri_t.rack_id.isin([j])]
                # row_level_space(): #row_name, row_limit, location_list
                rows_id = np.sort(tt.row_level.unique())
                row_dict={}
                for k in rows_id:
                    ttt = tt[tt.row_level.isin([k])]
                    cols_id = np.sort(ttt.col_level.unique())

                    col_dict = {}
                    for l in cols_id:
                        # location_info
                        l_inf = ttt[ttt.col_level.isin([l])]
    #     # input location_name  , cust_no, no_of_cust
    #     def __init__(self, location_name, rack_id, row_level, col_level,
    #                  storage_space, fre_ratio, frequency, charge_emp):

                        col_dict[l] = location_space(location_name = l_inf.values[0][0],
                                                     rack_id = l_inf.values[0][8], 
                                                     row_level = l_inf.values[0][9],
                                                     col_level = l_inf.values[0][10],
                                                     storage_space = l_inf.values[0][6],
                                                     fre_ratio = l_inf.values[0][3],
                                                     frequency = l_inf.values[0][2],
                                                     charge_emp = l_inf.values[0][1])

                    row_dict[k] = col_dict
                j_rack = rack_space(j, 1, 4, 4, row_dict)
                rack_dict[j] = j_rack

            priority_dict[Priority] = rack_dict
        i_emp = material_section(i, priority_dict)
        D10_location_dict[i] = i_emp
    return D10_location_dict

# 輸入貨架 class 找出使用頻率最小儲位
# change_space
def change_space(rack_class, fre_ratio_=0.05, location_space=1):
    re_freq = 10000000
    re_loc = ''

    for i in rack_class.row_level_list.keys():
        for j in rack_class.row_level_list[i].keys():
            if rack_class.row_level_list[i][j].storage_space == location_space:
                if rack_class.row_level_list[i][j].fre_ratio > fre_ratio_:
                    if rack_class.row_level_list[i][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[i][j].frequency
                        re_loc = rack_class.row_level_list[i][j].location_name
    
    return re_loc, re_freq


# 輸入料段 找出 P1區 使用頻率最小儲位
def search_space(section_class, fre_ratio_=0.05, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space(x[searching_space][rack_], fre_ratio_, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc

    return re_loc, re_freq

# 生成rack區換料決策
def location_decision_function_rack_version(D10_location_dict, rack_df, ratio=0.05):
    # input emp_class / test_df(emp, ratio < 0.05, )
    dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    for emp in D10_location_dict.keys():
        emp_class = D10_location_dict[emp]

        # 抓出前 N % 使用頻率物料
        test_df = rack_df[(rack_df.KEEPER_EMP == emp) &
                (rack_df.RATIO_IN_EMP < ratio) &
                (rack_df.PRIORITY > 1)].sort_values(['PRIORITY','RATIO_IN_EMP'], ascending=[False, True])

        _005_cust_info = test_df.to_dict('r')

        pre_dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])

        for i in range(len(test_df)):
            i_cust_space = _005_cust_info[i]['LOCATION_SPACE']

            i_cust_loc = _005_cust_info[i]['LOCATION']
            i_cust_fre = _005_cust_info[i]['TOTAL_USE_TIMES']
            i_cust_fre_ratio = _005_cust_info[i]['RATIO_IN_EMP']
            i_cust_priority = _005_cust_info[i]['PRIORITY']

            temp_df = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
            if i_cust_space == 1:
                # 找 p1中SPACE == 1之使用頻率最小儲位
                re_loc, re_freq = search_space(emp_class, fre_ratio_=ratio, searching_space='P1', location_space=1)
                
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                    # 替換儲位 fre, fre_ratio 資訊
                    loc = normalize_rack_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre
                        P1_cust.fre_ratio = i_cust_fre_ratio
                        
                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    re_loc, re_freq = search_space(emp_class, fre_ratio_=ratio, searching_space='P2', location_space=1)

                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_rack_location_info(re_loc)
                        if loc != 'NO_SPACE':

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre
                            P2_cust.fre_ratio = i_cust_fre_ratio
            
            elif i_cust_space == 2: # 針對P2/P3高頻物料 移至P1做優化\
                # 確認是否為下層 若為下層 則
                re_loc, re_freq = search_space(emp_class, fre_ratio_=ratio, searching_space='P1', location_space=2)
                    
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                    # 替換儲位 fre, fre_ratio 資訊

                    loc = normalize_rack_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre
                        P1_cust.fre_ratio = i_cust_fre_ratio
                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    re_loc, re_freq = search_space(emp_class, fre_ratio_=ratio, searching_space='P2', location_space=2)
                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_ratio_version(i_cust_loc, re_loc, ratio)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_rack_location_info(re_loc)
                        if loc != 'NO_SPACE':

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre
                            P2_cust.fre_ratio = i_cust_fre_ratio

            pre_dicision_df = pre_dicision_df.append(temp_df)
        dicision_df = dicision_df.append(pre_dicision_df)
    return dicision_df.reset_index(drop=True)

def decision_result_function(rack_decision, car_decision, cust_frquency):
    
    rack__ = rack_decision.ORIGIN_LOCATION.unique()
    car__ = car_decision.ORIGIN_LOCATION.unique()

    cust_loc = cust_frquency[['CUST_KP_NO', 'LOCATION', 'KEEPER_EMP']].drop_duplicates()
     # 9426 
    cust_loc.loc[:, 'LOCATION'] = cust_loc.LOCATION.apply(lambda x:clean_location_car_version(x))
    cust_loc_df_r = cust_loc[cust_loc.LOCATION.isin(rack__)].reset_index(drop=True).rename(columns={'LOCATION':'ORIGIN_LOCATION'})
    cust_loc_df_c = cust_loc[cust_loc.LOCATION.isin(car__)].reset_index(drop=True).rename(columns={'LOCATION':'ORIGIN_LOCATION'})

    r_decision = pd.merge(cust_loc_df_r, rack_decision, on='ORIGIN_LOCATION', how='left')
    c_decision = pd.merge(cust_loc_df_c, car_decision, on='ORIGIN_LOCATION', how='left')

    decision_result = pd.concat([r_decision, c_decision]).reset_index(drop=True)
    return decision_result

def generate_decision_df(threshold):
    thres = threshold
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    month_sql = last_three_month_sql()
    sql = '''
    SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
        FROM NSD_L10_D_material_storage_cust_usage_frquency
    where 1=1 %(month_sql)s

    '''% ({'month_sql' : month_sql})
    cust_frquency = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    season_frquency = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP'])['TOTAL_USE_TIMES'].sum().reset_index()

    # 讀取儲位順位
    sql = '''
    SELECT VALID_LOCATION, PRIORITY
        FROM NSD_L10_D_material_storage_space
    '''
    storage_space = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    # 處理 貨架 與 料車區
    rack_df, car_df = create_rack_and_car_df(season_frquency, storage_space)

    D10_location_car_dict = generate_car_section_dict(car_df)
    D10_location_rack_dict = generate_rack_section_dict(rack_df)

    car_decision = location_decision_function_car_version(D10_location_car_dict, car_df, ratio=thres)
    rack_decision = location_decision_function_rack_version(D10_location_rack_dict, rack_df, ratio=thres)

    decision_result = decision_result_function(rack_decision, car_decision, cust_frquency)
    return decision_result


def change_space_upper_fre_version(rack_class, frequency=0.05, location_space=1):
    re_freq = 10000000
    re_loc = ''
    if 8 in rack_class.row_level_list.keys():
        key_list = list(rack_class.row_level_list.keys())[:-1]
    else:
        key_list = list(rack_class.row_level_list.keys())
    for i in key_list:
        for j in rack_class.row_level_list[i].keys():
            if rack_class.row_level_list[i][j].storage_space == location_space:
                if rack_class.row_level_list[i][j].frequency < frequency:
                    if rack_class.row_level_list[i][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[i][j].frequency
                        re_loc = rack_class.row_level_list[i][j].location_name
    
    return re_loc, re_freq

def change_space_lower_fre_version(rack_class, frequency=150, location_space=1):
    re_freq = 10000000
    re_loc = ''

    if 8 in rack_class.row_level_list.keys():
        key_list = list(rack_class.row_level_list.keys())[-1]
        for j in rack_class.row_level_list[8].keys():
            if rack_class.row_level_list[8][j].storage_space == location_space:
                if rack_class.row_level_list[8][j].frequency < frequency:
                    if rack_class.row_level_list[8][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[8][j].frequency
                        re_loc = rack_class.row_level_list[8][j].location_name

    return re_loc, re_freq

# 輸入料段 找出 P1區 使用頻率最小儲位
def search_space_upper_fre_version(section_class, frequency=150, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space_upper_fre_version(x[searching_space][rack_], frequency, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc
    return re_loc, re_freq

# 輸入料段 找出 P1區 使用頻率最小儲位
def search_space_lower_fre_version(section_class, frequency=150, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space_lower_fre_version(x[searching_space][rack_], frequency, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc
    return re_loc, re_freq

# for car and rack
def change_location_df_fre_version(_005_cust, P1_cust, frequency):
    result = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    new = {'CONDITION_USAGE_TIME': frequency,
           'IMPROVE_LOCATION' : P1_cust,
           'ORIGIN_LOCATION' : _005_cust}
    _old = {'CONDITION_USAGE_TIME': frequency,
           'IMPROVE_LOCATION' : _005_cust,
           'ORIGIN_LOCATION' : P1_cust}
    # result.append(new)
    rows_list = []
    rows_list.append(new)
    rows_list.append(_old)
    # result = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])

    app = pd.DataFrame(rows_list) 
    result = result.append(app,sort=True)
    return result

# 輸入貨架 class 找出使用頻率最小儲位
def change_space_fre_version(rack_class, frequency=150, location_space=1):
    re_freq = 10000000
    re_loc = ''

    for i in rack_class.row_level_list.keys():
        for j in rack_class.row_level_list[i].keys():
            if rack_class.row_level_list[i][j].storage_space == location_space:
                if rack_class.row_level_list[i][j].frequency < frequency:
                    if rack_class.row_level_list[i][j].frequency < re_freq:

                        re_freq = rack_class.row_level_list[i][j].frequency
                        re_loc = rack_class.row_level_list[i][j].location_name
    
    return re_loc, re_freq

# 輸入料段 找出 P1區 使用頻率最小儲位 fre_ratio_
def search_space_fre_version(section_class, frequency=150, searching_space='P1', location_space=1):
    x = section_class.component_list
    re_freq = 10000000
    re_loc = ''
    for rack_ in x[searching_space].keys():
        temp_loc, temp_fre = change_space_fre_version(x[searching_space][rack_], frequency, location_space=location_space)
        if temp_fre < re_freq:
            re_freq = temp_fre
            re_loc = temp_loc
    return re_loc, re_freq


# 生成rack區換料決策
def location_decision_function_rack_version_fre(D10_location_dict, rack_df, thres=150):
    # input emp_class / test_df(emp, ratio < 0.05, )
    dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    for emp in D10_location_dict.keys():
        emp_class = D10_location_dict[emp]
        
        # 抓出前 N % 使用頻率物料
        test_df = rack_df[(rack_df.KEEPER_EMP == emp) &
                (rack_df.TOTAL_USE_TIMES >= thres) &
                (rack_df.PRIORITY > 1)].sort_values(['PRIORITY','RATIO_IN_EMP'], ascending=[False, True])

        _005_cust_info = test_df.to_dict('r')

        pre_dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])

        for i in range(len(test_df)):
            i_cust_space = _005_cust_info[i]['LOCATION_SPACE']

            i_cust_loc = _005_cust_info[i]['LOCATION']
            i_cust_fre = _005_cust_info[i]['TOTAL_USE_TIMES']
            i_cust_priority = _005_cust_info[i]['PRIORITY']

            temp_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
            if i_cust_space == 1:
                # 找 p1中SPACE == 1之使用頻率最小儲位
                re_loc, re_freq = search_space_fre_version(emp_class, frequency=thres, searching_space='P1', location_space=1)
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                    # 替換儲位 fre, fre_ratio 資訊
                    loc = normalize_rack_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre

                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    re_loc, re_freq = search_space_fre_version(emp_class, frequency=thres, searching_space='P2', location_space=1)

                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_rack_location_info(re_loc)
                        if loc != 'NO_SPACE':

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre

            elif i_cust_space == 2: # 針對P2/P3高頻物料 移至P1做優化\
                # 確認是否為下層 若為下層 則
                re_loc, re_freq = search_space_fre_version(emp_class, frequency=thres, searching_space='P1', location_space=2)
                    
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                    # 替換儲位 fre, fre_ratio 資訊

                    loc = normalize_rack_location_info(re_loc)
                    if loc != 'NO_SPACE':

                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre

                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    re_loc, re_freq = search_space_fre_version(emp_class, frequency=thres, searching_space='P2', location_space=1)
                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_rack_location_info(re_loc)
                        if loc != 'NO_SPACE': 

                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre

            pre_dicision_df = pre_dicision_df.append(temp_df)
        dicision_df = dicision_df.append(pre_dicision_df)
    return dicision_df.reset_index(drop=True)

# location_decision_function_rack_version_fre(D10_location_rack_dict, rack_df, thres=150)

# 生成car區換料決策 location
def location_decision_function_car_version_fre(D10_location_car_dict, car_df, thres=150):
    # input emp_class / test_df(emp, ratio < 0.05, )
    dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
    for emp in D10_location_car_dict.keys():
        emp_class = D10_location_car_dict[emp]
        
        # 抓出前 N % 使用頻率物料
        test_df = car_df[(car_df.KEEPER_EMP == emp) &
                (car_df.TOTAL_USE_TIMES >= thres) &
                (car_df.PRIORITY > 1)].sort_values(['PRIORITY', 'RATIO_IN_EMP'], ascending=[False, True])

        _005_cust_info = test_df.to_dict('r')

        pre_dicision_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
        
        for i in range(len(test_df)):
            i_cust_space = _005_cust_info[i]['LOCATION_SPACE']

            i_cust_loc = _005_cust_info[i]['LOCATION']
            i_cust_fre = _005_cust_info[i]['TOTAL_USE_TIMES']
            i_cust_priority = _005_cust_info[i]['PRIORITY']
            loc_005 = re.findall(r"(\d+?\d*)", i_cust_loc)

            temp_df = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'ORIGIN_LOCATION', 'IMPROVE_LOCATION'])
            if i_cust_space == 1:
                # 找 p1中SPACE == 1之使用頻率最小儲位

                ''' 若是在最下層 ，則只搜索最下層物料
                若是再上層(col*row)範圍內， 則搜索上層'''
                
                # 在P3區 且lower層
                P3is_lower = (int(loc_005[0]) % 3 == 0) & (int(loc_005[1]) > 63)
                P2is_lower = (int(loc_005[0]) % 3 != 0) & (int(loc_005[1]) > 49)
                if (P3is_lower) | (P2is_lower):
                    re_loc, re_freq = search_space_lower_fre_version(emp_class, frequency=thres, searching_space='P1', location_space=1)
                else:
                    re_loc, re_freq = search_space_upper_fre_version(emp_class, frequency=thres, searching_space='P1', location_space=1)

                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_car_location_info(re_loc)
                        if loc != 'NO_SPACE':
                            P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P1_cust.frequency = i_cust_fre
                    elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                        if (P3is_lower) | (P2is_lower):
                            re_loc, re_freq = search_space_lower_fre_version(emp_class, frequency=thres, searching_space='P2', location_space=1)
                        else:
                            re_loc, re_freq = search_space_upper_fre_version(emp_class, frequency=thres, searching_space='P2', location_space=1)
                        if re_loc != '':
                            # 儲存調換儲位df
                            temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                            # 替換儲位 fre, fre_ratio 資訊

                            loc = normalize_car_location_info(re_loc)
                            if loc != 'NO_SPACE':

                                P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                                P2_cust.frequency = i_cust_fre

            elif i_cust_space == 2: # 針對P2/P3高頻物料 移至P1做優化\
                # 確認是否為下層 若為下層 則
                P3is_lower = (int(loc_005[0]) % 3 == 0) & (int(loc_005[1]) > 63)
                P2is_lower = (int(loc_005[0]) % 3 != 0) & (int(loc_005[1]) > 49)
                if (P3is_lower) | (P2is_lower):
                    continue
                else:
                    re_loc, re_freq = search_space_upper_fre_version(emp_class, frequency=thres, searching_space='P1', location_space=2)
                    
                if re_loc != '':
                    # 儲存調換儲位df
                    temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                    # 替換儲位 fre, fre_ratio 資訊

                    loc = normalize_car_location_info(re_loc)
                    if loc != 'NO_SPACE':
                        P1_cust = emp_class.component_list['P1'][loc[0]].row_level_list[loc[1]][loc[2]]
                        P1_cust.frequency = i_cust_fre
            
                elif (re_loc == '') & (i_cust_priority == 3): # 找不到P1 的優化空間
                    if (P3is_lower) | (P2is_lower):
                        continue
                    else:
                        re_loc, re_freq = search_space_upper_fre_version(emp_class, frequency=thres, searching_space='P2', location_space=2)
                    if re_loc != '':
                        # 儲存調換儲位df
                        temp_df = change_location_df_fre_version(i_cust_loc, re_loc, frequency=thres)
                        # 替換儲位 fre, fre_ratio 資訊

                        loc = normalize_car_location_info(re_loc)
                        if loc != 'NO_SPACE':
                            P2_cust = emp_class.component_list['P2'][loc[0]].row_level_list[loc[1]][loc[2]]
                            P2_cust.frequency = i_cust_fre

            pre_dicision_df = pre_dicision_df.append(temp_df)
        dicision_df = dicision_df.append(pre_dicision_df)
        # dicision_df # 需去除空白 / 去除space ==2
    return dicision_df.reset_index(drop=True)


def generate_decision_df_fre_version(threshold):
    thres = threshold
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    month_sql = last_three_month_sql()
    sql = '''
    SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
        FROM NSD_L10_D_material_storage_cust_usage_frquency
     where 1=1 %(month_sql)s

    '''% ({'month_sql' : month_sql})
    cust_frquency = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    season_frquency = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP'])['TOTAL_USE_TIMES'].sum().reset_index()

    # 讀取儲位順位
    sql = '''
    SELECT VALID_LOCATION, PRIORITY
        FROM NSD_L10_D_material_storage_space
    '''
    storage_space = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    # 處理 貨架 與 料車區
    rack_df, car_df = create_rack_and_car_df(season_frquency, storage_space)

    D10_location_car_dict = generate_car_section_dict(car_df)
    D10_location_rack_dict = generate_rack_section_dict(rack_df)

    car_decision = location_decision_function_car_version_fre(D10_location_car_dict, car_df, thres=thres)
    rack_decision = location_decision_function_rack_version_fre(D10_location_rack_dict, rack_df, thres=thres)
    
    decision_result = decision_result_function(rack_decision, car_decision, cust_frquency)
    return decision_result


# 以下為儲位容載率
def storage_to_db_preprocessing(df, FLOOR='FLOOR', mat='MATERIAL_TYPE', part_use='PART_USE'):
    # PDA_IN[PDA_IN.MATERIAL_TYPE.isin(['COPY-IC'])]
    df = df[df[FLOOR].isin(['D', 'E', 'F'])]
    df['MATERIAL_TYPE'] = np.where(df[mat] == 'COPY-IC',
                                        np.where(df[part_use] == 'PTHS', 'A', 'BC'), df[mat])
    df = df[-df.MATERIAL_TYPE.isin(['BC'])]
    df['FLOOR'] = np.where(df[FLOOR] == 'E', 'E6',
                           np.where(df[FLOOR] == 'F', 'F21',
                                    np.where(df[mat] != 'PTH', 'D10',
                                             np.where(df[part_use] == 'PTHS', 'D10', 'D9'))))
    return df
