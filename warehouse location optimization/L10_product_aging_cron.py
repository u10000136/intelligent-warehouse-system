import requests
import re
from datetime import datetime
from stock.WHS.dbio import STOCKMySQLIO


# 讀取 API , 將 MFG 與入庫時間存入DB
def aging_data_cron():
    data_to_db = []
    for MFG in ['DSBU', 'AGBU', 'SAVBU', 'IPCBU', 'WNBU', 'SERVERBU', 'ERBU', 'PABU', 'TSBU', 'CORBU', 'BPD']:
        url = f'http://10.129.8.23:8080/StocktakeService.asmx/GetWareHouseAgingReport?bu={MFG}'

        resp = requests.get(url)
        pattern = re.compile(r'(\d+-\d+-\d+ \d+:\d+:\d+)')
        time_list_str = pattern.findall(resp.text)

        time_list_date = [datetime.strptime(x, "%Y-%m-%d %H:%M:%S") for x in time_list_str]

        insert_sql = " INSERT INTO all_bu_aging_data (MFG, STOCKINTIME) VALUES (%s, %s)"
        if len(time_list_date) == 0 :
            print(MFG)

        for t in time_list_date:
            data_to_db.append((MFG, t.strftime('%Y-%m-%d %H:%M:%S')))

    if len(data_to_db):
        db = STOCKMySQLIO()
        db.execute_sql("TRUNCATE TABLE all_bu_aging_data")
        db.insert_sql_list(insert_sql, data_list=data_to_db)
