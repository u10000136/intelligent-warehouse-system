from datetime import datetime, timedelta
from fii_ai_api.utils.dbio.mysql import BasicMySQL
from stock.DB.oracle import BasicORACLE
from stock.config import *
from stock.WHS.storage.L10_Elec_storage_model import *
import pandas as pd


# 存入每個月物料的使用頻率
def cust_usage_frquency_cron():
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                      hostname=MYSQL_login_info['hostname'],
                      port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    AllPart_DB_DE = BasicORACLE(username=Allpart_DE['username'], password=Allpart_DE['password'],
                                hostname=Allpart_DE['hostname'],
                                port=Allpart_DE['port'], db_name=Allpart_DE['db_name'])

    '''讀取 各類出庫次數資料'''
    AREA = 'D10'
    WORK_TIME_sql = generate_time_sql(time_col='WORK_TIME')
    LABEL_TIME_sql = generate_time_sql(time_col='LABEL_TIME')

    t = datetime.now()
    # WORK_TIME
    sql = '''
    SELECT CUST_KP_NO, YEAR(WORK_TIME) as YEAR, MONTH(WORK_TIME) as MONTH, count(*) as ADJUST_TIMES
        FROM NSD_L10_material_adjust
    WHERE 
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        group by CUST_KP_NO, YEAR(WORK_TIME), MONTH(WORK_TIME)
        order by YEAR desc, MONTH desc, ADJUST_TIMES desc
    '''% ({'AREA' : AREA, 'time_sql' : WORK_TIME_sql})
    adjust = Stock_DB.manipulate_db(sql, dtype='DataFrame')

    # WORK_TIME
    sql = '''
    WITH to_wo_CTE AS
    (
    SELECT CUST_KP_NO, YEAR(WORK_TIME) as YEAR, MONTH(WORK_TIME) as MONTH, WO, count(*) as WO_TIMES
        FROM NSD_L10_material_311_to_wo
    WHERE 
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        
        group by CUST_KP_NO, YEAR(WORK_TIME), MONTH(WORK_TIME), WO
        order by YEAR desc, MONTH desc, WO_TIMES desc
    )
    select CUST_KP_NO, YEAR, MONTH, count(WO) as WO_USE_TIMES, sum(WO_TIMES) as WO_TIMES from to_wo_CTE
    group by CUST_KP_NO, YEAR, MONTH
    '''% ({'AREA' : AREA, 'time_sql' : WORK_TIME_sql})
    _311_to_wo_USE = Stock_DB.manipulate_db(sql, dtype='DataFrame')


    # _311轉倉出庫 WORK_TIME
    sql = '''
    WITH trans_out_CTE AS
    (
    SELECT CUST_KP_NO, YEAR(WORK_TIME) as YEAR, MONTH(WORK_TIME) as MONTH, ACTIONID, count(*) as TRANS_TIMES
        FROM NSD_L10_material_311_trans_out
    WHERE
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        group by CUST_KP_NO, YEAR(WORK_TIME), MONTH(WORK_TIME), ACTIONID
    )
    select CUST_KP_NO, YEAR, MONTH, count(ACTIONID) as TRANS_USE_TIMES, sum(TRANS_TIMES) as TRANS_TIMES from trans_out_CTE
    group by CUST_KP_NO, YEAR, MONTH

    '''% ({'AREA' : AREA, 'time_sql' : WORK_TIME_sql})
    _311_out_USE = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    _311_out_USE

    # 261 出庫 WORK_TIME
    sql = '''
    SELECT CUST_KP_NO, YEAR(WORK_TIME) as YEAR, MONTH(WORK_TIME) as MONTH, count(*) as 261_TIMES
        FROM NSD_L10_material_261_all
    WHERE 
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        
        group by CUST_KP_NO, YEAR(WORK_TIME), MONTH(WORK_TIME)
        order by YEAR desc, MONTH desc, 261_TIMES desc
        
    '''% ({'AREA' : AREA, 'time_sql' : WORK_TIME_sql})
    _261_USE = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    _261_USE
    # and WORK_TIME >= '2021-01-01'

    # WORK_TIME
    sql = '''

    WITH trans_in_CTE AS
    (
    SELECT CUST_KP_NO, YEAR(WORK_TIME) as YEAR, MONTH(WORK_TIME) as MONTH, ACTIONID, count(*) as TRANS_IN_TIMES
        FROM NSD_L10_material_311_trans_in
    WHERE 
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        
        group by CUST_KP_NO, YEAR(WORK_TIME), MONTH(WORK_TIME), ACTIONID
    )
    select CUST_KP_NO, YEAR, MONTH, count(ACTIONID) as TRANS_IN_USE_TIMES, sum(TRANS_IN_TIMES) as TRANS_IN_TIMES 
    from trans_in_CTE
    group by CUST_KP_NO, YEAR, MONTH

    '''% ({'AREA' : AREA, 'time_sql' : WORK_TIME_sql})
    trans_in_USE = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    trans_in_USE

    # LABEL_TIME
    sql = '''
    WITH checkin_CTE AS
    (
    SELECT CUST_KP_NO, YEAR(LABEL_TIME) as YEAR, MONTH(LABEL_TIME) as MONTH, DOC_NO, count(*) as CHECKIN_TIMES
        FROM NSD_L10_material_checkin
    WHERE
        FLOOR = '%(AREA)s'
        and MATERIAL_TYPE = 'BC'
        %(time_sql)s
        group by CUST_KP_NO, YEAR(LABEL_TIME), MONTH(LABEL_TIME), DOC_NO
    )
    select CUST_KP_NO, YEAR, MONTH, count(DOC_NO) as CHECKIN_USE_TIMES, sum(CHECKIN_TIMES) as CHECKIN_TIMES from checkin_CTE
    group by CUST_KP_NO, YEAR, MONTH


    '''% ({'AREA' : AREA, 'time_sql' : LABEL_TIME_sql})
    checkin_USE = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    checkin_USE

    out_r = pd.merge(_311_to_wo_USE, _311_out_USE,how='outer',on=['CUST_KP_NO', 'YEAR', 'MONTH']).fillna(0)
    out_re = pd.merge(out_r, _261_USE, how='outer',on=['CUST_KP_NO', 'YEAR', 'MONTH']).fillna(0)
    out_result = pd.merge(out_re, adjust, how='outer',on=['CUST_KP_NO', 'YEAR', 'MONTH']).fillna(0)

    in_result = pd.merge(checkin_USE, trans_in_USE, how='outer',on=['CUST_KP_NO', 'YEAR', 'MONTH']).fillna(0)
    all_result = pd.merge(out_result, in_result, how='outer',on=['CUST_KP_NO', 'YEAR', 'MONTH']).fillna(0)

    all_result['TOTAL_TIMES'] = np.sum(all_result[['TRANS_TIMES', 'WO_TIMES', 'ADJUST_TIMES']], axis=1)
    all_result['TOTAL_USE_TIMES'] = np.sum(all_result[['TRANS_USE_TIMES', 'WO_USE_TIMES', 'ADJUST_TIMES']], axis=1)


    def cust_lst2sql(lst):
        times = len(lst) // 1000
        if len(lst) != 0:
            if len(lst) % 1000 != 0:
                times += 1
            cust_sql = 'and ('
            for i in range(times):
                index = 1000 * i
                cust_sql += 'CUST_KP_NO in (%(cust_sql)s) or ' % ({'cust_sql': str(lst[index: index + 1000])[1:-1]})
            cust_sql = cust_sql[:-3]
            cust_sql += ')'
        else:
            cust_sql = "and CUST_KP_NO in ('')"
        return cust_sql
    a = all_result.CUST_KP_NO.unique()
    list(a)
    cust_sql = cust_lst2sql(list(a))

    sql = '''
    SELECT CUST_KP_NO, LOCATION, KEEPER_EMP FROM MES1.C_KITTING_STOCK_CONFIG
    WHERE BUDING = '%(AREA)s'
    %(cust_sql)s
    ''' % ({'AREA': AREA[0], 'cust_sql': cust_sql})
    CUST_LOCATION = AllPart_DB_DE.manipulate_db(sql, dtype='DataFrame')
    location_data = pd.merge(all_result, CUST_LOCATION, how='left', on='CUST_KP_NO').dropna()
    # location_data.sort_values('LOCATION')
    float_list = ['WO_USE_TIMES', 'WO_TIMES',
        'TRANS_USE_TIMES', 'TRANS_TIMES', '261_TIMES', 'ADJUST_TIMES',
        'CHECKIN_USE_TIMES', 'CHECKIN_TIMES', 'TRANS_IN_USE_TIMES',
        'TRANS_IN_TIMES', 'TOTAL_TIMES', 'TOTAL_USE_TIMES']


    location_data[float_list] = location_data[float_list].astype(int)


    # order = ['CUST_KP_NO', 'YEAR', 'MONTH', 
    #         'WO_USE_TIMES', 'TRANS_USE_TIMES', 'ADJUST_TIMES',
    #         'WO_TIMES', 'TRANS_TIMES',
    #         'CHECKIN_USE_TIMES', 'TRANS_IN_USE_TIMES',
    #         'CHECKIN_TIMES', 'TRANS_IN_TIMES', '261_TIMES',
    #         'TOTAL_TIMES', 'TOTAL_USE_TIMES', 'LOCATION', 'KEEPER_EMP']
    
    # 更新儲位料號使用頻率
    try:
        insert_sql = '''
        INSERT IGNORE INTO NSD_L10_D_material_storage_cust_usage_frquency
            (CUST_KP_NO , YEAR , MONTH , WO_USE_TIMES , TRANS_USE_TIMES ,
            ADJUST_TIMES , WO_TIMES , TRANS_TIMES , CHECKIN_USE_TIMES ,
            TRANS_IN_USE_TIMES , CHECKIN_TIMES , TRANS_IN_TIMES , 261_TIMES ,
            TOTAL_TIMES , TOTAL_USE_TIMES , LOCATION , KEEPER_EMP )
        VALUES (%s, %s, %s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s, %s)
        on DUPLICATE KEY UPDATE 
            CUST_KP_NO=VALUES(CUST_KP_NO) , YEAR=VALUES(YEAR) , MONTH=VALUES(MONTH) , WO_USE_TIMES=VALUES(WO_USE_TIMES) , TRANS_USE_TIMES=VALUES(TRANS_USE_TIMES) ,
            ADJUST_TIMES=VALUES(ADJUST_TIMES) , WO_TIMES=VALUES(WO_TIMES) , TRANS_TIMES=VALUES(TRANS_TIMES) , CHECKIN_USE_TIMES=VALUES(CHECKIN_USE_TIMES) ,
            TRANS_IN_USE_TIMES=VALUES(TRANS_IN_USE_TIMES) , CHECKIN_TIMES=VALUES(CHECKIN_TIMES) , TRANS_IN_TIMES=VALUES(TRANS_IN_TIMES) , 261_TIMES=VALUES(261_TIMES) ,
            TOTAL_TIMES=VALUES(TOTAL_TIMES) , TOTAL_USE_TIMES=VALUES(TOTAL_USE_TIMES) , LOCATION=VALUES(LOCATION) , KEEPER_EMP=VALUES(KEEPER_EMP)'''
        #
        data_to_db = []
        for i, row in location_data.iterrows():
            temp_data = (row['CUST_KP_NO'], row['YEAR'], row['MONTH'], row['WO_USE_TIMES'], row['TRANS_USE_TIMES'],
                        row['ADJUST_TIMES'], row['WO_TIMES'], row['TRANS_TIMES'], row['CHECKIN_USE_TIMES'],
                        row['TRANS_IN_USE_TIMES'], row['CHECKIN_TIMES'], row['TRANS_IN_TIMES'], row['261_TIMES'],
                        row['TOTAL_TIMES'], row['TOTAL_USE_TIMES'], row['LOCATION'], row['KEEPER_EMP'])    
            data_to_db.append(temp_data)
        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)

        print('cust_usage_frquency_cron update successfully')
    except:
        print('update cust_usage_frquency_cron failed')

# 更新儲位優化決策
def decision_result_cron():
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                      hostname=MYSQL_login_info['hostname'],
                      port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])
    month_sql = last_three_month_sql()
    sql = '''
    SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
        FROM NSD_L10_D_material_storage_cust_usage_frquency
     where 1=1 %(month_sql)s

    '''% ({'month_sql' : month_sql})
    cust_frquency = Stock_DB.manipulate_db(sql, dtype='DataFrame')
    season_frquency = cust_frquency.groupby(['LOCATION', 'KEEPER_EMP'])['TOTAL_TIMES', 'TOTAL_USE_TIMES'].sum().reset_index()

    ori_times = season_frquency.rename(columns={'LOCATION': 'ORIGIN_LOCATION', 'TOTAL_TIMES': 'original_TOTAL_TIMES',
                                                'TOTAL_USE_TIMES': 'original_TOTAL_USE_TIMES'})
    imp_times = season_frquency.rename(columns={'LOCATION': 'IMPROVE_LOCATION',
                                                'TOTAL_TIMES': 'improved_TOTAL_TIMES',
                                                'TOTAL_USE_TIMES': 'improved_TOTAL_USE_TIMES'})

    # 處理次數
    try:
        result = pd.DataFrame(columns=['CONDITION_USAGE_RATIO', 'KEEPER_EMP',
                                        'CUST_KP_NO', 'ORIGIN_LOCATION', 'original_TOTAL_TIMES', 'original_TOTAL_USE_TIMES',
                                        'change_CUST_KP_NO', 'IMPROVE_LOCATION', 'improved_TOTAL_TIMES', 'improved_TOTAL_USE_TIMES'])
        for i in range(1, 101):
            _ratio = round(i * 0.01, 2)
            result_i = generate_decision_df(threshold=_ratio)
            
            right_cust = result_i[['CUST_KP_NO', 'ORIGIN_LOCATION']].rename(columns={'CUST_KP_NO': 'change_CUST_KP_NO',
                                                                    'ORIGIN_LOCATION': 'IMPROVE_LOCATION'})
            temp_re= pd.merge(result_i, right_cust, on='IMPROVE_LOCATION', how='left')
            temp_re_1 = pd.merge(temp_re, ori_times, on=['ORIGIN_LOCATION', 'KEEPER_EMP'], how='left')
            temp_result = pd.merge(temp_re_1, imp_times, on=['IMPROVE_LOCATION', 'KEEPER_EMP'], how='left')

            tempt_re_i = temp_result[temp_result.original_TOTAL_USE_TIMES > temp_result.improved_TOTAL_USE_TIMES]
            
            if len(tempt_re_i) != 0:
                result = pd.concat([result, tempt_re_i], ignore_index=True, sort=True)
    except Exception:
        print('create decision result by ratio failed')

    try:
        f_result = pd.DataFrame(columns=['CONDITION_USAGE_TIME', 'KEEPER_EMP',
                                        'CUST_KP_NO', 'ORIGIN_LOCATION', 'original_TOTAL_TIMES', 'original_TOTAL_USE_TIMES',
                                        'change_CUST_KP_NO', 'IMPROVE_LOCATION', 'improved_TOTAL_TIMES', 'improved_TOTAL_USE_TIMES'])
        for i in range(0, 505, 5):
            f_result_i = generate_decision_df_fre_version(threshold=i)
            # 做儲位 料號合併
            right_cust = f_result_i[['CUST_KP_NO', 'ORIGIN_LOCATION']].rename(columns={'CUST_KP_NO': 'change_CUST_KP_NO',
                                                                                        'ORIGIN_LOCATION': 'IMPROVE_LOCATION'})
            temp_re = pd.merge(f_result_i, right_cust, on='IMPROVE_LOCATION', how='left')
            # 將原始儲位使用次數/ 使用盤數合併
            temp_re_1 = pd.merge(temp_re, ori_times, on=['ORIGIN_LOCATION', 'KEEPER_EMP'], how='left')
            temp_result = pd.merge(temp_re_1, imp_times, on=['IMPROVE_LOCATION', 'KEEPER_EMP'], how='left')

            f_tempt_re_i = temp_result[temp_result.original_TOTAL_USE_TIMES > temp_result.improved_TOTAL_USE_TIMES]
            
            if len(f_tempt_re_i) != 0:
                f_result = pd.concat([f_result, f_tempt_re_i], ignore_index=True, sort=True)
    except Exception:
        print('create decision result by times failed')

    # 合併所有決策df
    real_result = pd.concat([result, f_result], ignore_index=True, sort=True).fillna(-1)
    real_result.loc[:, 'TIME'] = create_TIME_in_db()

    try: # 優化結果寫入資料庫 (先刪除再插入)
        sql = '''
        DELETE FROM 
            NSD_L10_D_material_storage_space_decicion_result
        where 
            TIME = '%(TIME)s'
        '''% ({'TIME' : create_TIME_in_db()})
        aa = Stock_DB.manipulate_db(sql)

        insert_sql = '''
        INSERT IGNORE INTO NSD_L10_D_material_storage_space_decicion_result
            (CONDITION_USAGE_RATIO, CONDITION_USAGE_TIME, CUST_KP_NO,
            IMPROVE_LOCATION, KEEPER_EMP, ORIGIN_LOCATION,
            change_CUST_KP_NO, improved_TOTAL_TIMES, improved_TOTAL_USE_TIMES,
            original_TOTAL_TIMES, original_TOTAL_USE_TIMES, TIME)
        VALUES (%s, %s, %s,
                %s, %s, %s,
                %s, %s, %s,
                %s, %s, %s)
        on DUPLICATE KEY UPDATE 
            CONDITION_USAGE_RATIO=VALUES(CONDITION_USAGE_RATIO), CONDITION_USAGE_TIME=VALUES(CONDITION_USAGE_TIME), 
            CUST_KP_NO=VALUES(CUST_KP_NO),
            IMPROVE_LOCATION=VALUES(IMPROVE_LOCATION), KEEPER_EMP=VALUES(KEEPER_EMP), ORIGIN_LOCATION=VALUES(ORIGIN_LOCATION),
            change_CUST_KP_NO=VALUES(change_CUST_KP_NO),
            improved_TOTAL_TIMES=VALUES(improved_TOTAL_TIMES), improved_TOTAL_USE_TIMES=VALUES(improved_TOTAL_USE_TIMES),
            original_TOTAL_TIMES=VALUES(original_TOTAL_TIMES), original_TOTAL_USE_TIMES=VALUES(original_TOTAL_USE_TIMES),
            TIME=VALUES(TIME) '''
        #
        data_to_db = []
        for i, row in real_result.iterrows():
            temp_data = (row['CONDITION_USAGE_RATIO'], row['CONDITION_USAGE_TIME'], row['CUST_KP_NO'],
            row['IMPROVE_LOCATION'], row['KEEPER_EMP'], row['ORIGIN_LOCATION'],
            row['change_CUST_KP_NO'], int(row['improved_TOTAL_TIMES']), int(row['improved_TOTAL_USE_TIMES']),
            int(row['original_TOTAL_TIMES']), int(row['original_TOTAL_USE_TIMES']), row['TIME'].strftime('%Y-%m-%d %H:%M:%S'))
            data_to_db.append(temp_data)

        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)
        print('success')
    except:
        print('update heatmap result failed')
    return True

# 更新儲位熱立圖
def heatmap_result_cron():
    Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                      hostname=MYSQL_login_info['hostname'],
                      port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])

    try:
        p_result = pd.DataFrame(columns=['KEEPER_EMP', 'LOC_ID', 'TOTAL_TIMES', 'NO_of_CUST', 'MAX_IN_EMP',
                            'maxcolor_in_emp', 'loc_color', 'CONDITION_USAGE_TIME'])
        for i in range(101):
            _ratio = round(i * 0.01, 2)
            p_tempt_re = NSD_D10_elec_storage_heatmap_p(percentile=_ratio)
            if len(p_tempt_re) != 0:
                p_tempt_re.loc[:, 'CONDITION_USAGE_RATIO'] = _ratio
                p_result = pd.concat([p_result, p_tempt_re], ignore_index=True)

    except Exception:
        print(Exception)
        print('create heatmap result by ratio failed')

    try:
        f_result = pd.DataFrame(columns=['KEEPER_EMP', 'LOC_ID', 'TOTAL_TIMES', 'NO_of_CUST', 'MAX_IN_EMP',
                            'maxcolor_in_emp', 'loc_color', 'CONDITION_USAGE_TIME'])
        for i in range(0, 505, 5):
            f_tempt_re = NSD_D10_elec_storage_heatmap_f(threshold=i)
            if len(f_tempt_re) != 0:
                f_tempt_re.loc[:, 'CONDITION_USAGE_TIME'] = i
                f_result = pd.concat([f_result, f_tempt_re], ignore_index=True)
    except Exception:
        print(Exception)
        print('create heatmap result by times failed')

    heatmap_result = pd.concat([p_result, f_result], ignore_index=True).fillna(-1)
    heatmap_result.loc[:, 'TIME'] = create_TIME_in_db()

    try:
        sql = '''
        DELETE FROM 
            NSD_L10_D_material_storage_space_heatmap
        where 
            TIME = '%(TIME)s'
        '''% ({'TIME' : create_TIME_in_db()})
        aa = Stock_DB.manipulate_db(sql)
        insert_sql = '''
        INSERT IGNORE INTO NSD_L10_D_material_storage_space_heatmap
            (KEEPER_EMP, CONDITION_USAGE_RATIO, CONDITION_USAGE_TIME,
            LOC_ID, TOTAL_TIMES, NO_of_CUST, MAX_IN_EMP,
            maxcolor_in_emp, loc_color, TIME)
        VALUES (%s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s)
        on DUPLICATE KEY UPDATE 
                KEEPER_EMP=VALUES(KEEPER_EMP), CONDITION_USAGE_RATIO=VALUES(CONDITION_USAGE_RATIO), CONDITION_USAGE_TIME=VALUES(CONDITION_USAGE_TIME),
                LOC_ID=VALUES(LOC_ID), TOTAL_TIMES=VALUES(TOTAL_TIMES), NO_of_CUST=VALUES(NO_of_CUST), MAX_IN_EMP=VALUES(MAX_IN_EMP),
                maxcolor_in_emp=VALUES(maxcolor_in_emp), loc_color=VALUES(loc_color), TIME=VALUES(TIME) '''
        #
        data_to_db = []
        for i, row in heatmap_result.iterrows():
            temp_data = (row['KEEPER_EMP'], row['CONDITION_USAGE_RATIO'], int(row['CONDITION_USAGE_TIME']),
                        row['LOC_ID'], row['TOTAL_TIMES'], int(row['NO_of_CUST']), int(row['MAX_IN_EMP']),
                        row['maxcolor_in_emp'], row['loc_color'], row['TIME'].strftime('%Y-%m-%d %H:%M:%S'))
            data_to_db.append(temp_data)
        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)

        print('success')
    except:
        print('update heatmap result failed')
    return True

# 更新儲位容載率
def storage_capacity_cron():
    # NSD 儲位容載率
    try:
        AllPart_DB_DE = BasicORACLE(username=Allpart_DE['username'], password=Allpart_DE['password'],
                                hostname=Allpart_DE['hostname'],
                                port=Allpart_DE['port'], db_name=Allpart_DE['db_name'])

        AllPart_DB_F = BasicORACLE(username=Allpart_F_ap3['username'], password=Allpart_F_ap3['password'],
                                hostname=Allpart_F_ap3['hostname'],
                                port=Allpart_F_ap3['port'], db_name=Allpart_F_ap3['db_name'])
        sql = '''
        SELECT 
            a.LOCATION, a.TR_SN, a.QTY, a.CUST_KP_NO,
            DECODE (part_type,
                '0', 'A',
                '1', 'A',
                '2', 'BC',
                '3', 'BC',
                '4', 'BC',
                '5', 'COPY-IC',
                '8', 'S',
                '9', 'PCB',
                '10', 'PTH'
                )  MATERIAL_TYPE,
                PART_USE, a.AREA
        FROM 
            MES4.R_WHS_LOCATION a,
            MES1.C_KITTING_STOCK_CONFIG E
        WHERE
            a.CUST_KP_NO = E.CUST_KP_NO
            AND E.PART_TYPE IN ('0', '1', '5', '8', '9', '10')
            and a.AREA = E.BUDING
            and a.LOCATION NOT LIKE 'HN%'
        '''
        WHS_LOCATION = AllPart_DB_DE.manipulate_db(sql, dtype='DataFrame')
        result_location = storage_to_db_preprocessing(WHS_LOCATION, FLOOR='AREA', mat='MATERIAL_TYPE', part_use='PART_USE')
        sql = '''
        SELECT 
            a.LOCATION, a.TR_SN, a.QTY, a.CUST_KP_NO,
            DECODE (part_type,
                '0', 'A',
                '1', 'A',
                '2', 'BC',
                '3', 'BC',
                '4', 'BC',
                '5', 'COPY-IC',
                '8', 'S',
                '9', 'PCB',
                '10', 'PTH'
                )  MATERIAL_TYPE,
                PART_USE, a.AREA
        FROM 
            MES4.R_WHS_LOCATION a,
            MES1.C_KITTING_STOCK_CONFIG E
        WHERE
            a.CUST_KP_NO = E.CUST_KP_NO
            AND E.PART_TYPE IN ('0', '1', '5', '8', '9', '10')
            and a.AREA = E.BUDING
            and a.LOCATION NOT LIKE 'HN%'

        '''
        WHS_LOCATION_F = AllPart_DB_F.manipulate_db(sql, dtype='DataFrame')
        result_location_F = storage_to_db_preprocessing(WHS_LOCATION_F, FLOOR='AREA', mat='MATERIAL_TYPE', part_use='PART_USE')

        result = pd.concat([result_location,result_location_F])
    
    except:
        print('connect_allpart_fail')
    try:
        Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                      hostname=MYSQL_login_info['hostname'],
                      port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])
        sql = '''
        SELECT *
        FROM all_whs_storage_capacity_constraint
        '''
        constraint = Stock_DB.manipulate_db(sql, dtype='DataFrame')
        all_whs_capacity = pd.DataFrame(columns= ['SITE', 'BU', 'WHS_TYPE', 'AREA', 'B_RACK_denominator', 'B_RACK_numerator', 'S_RACK_denominator', 'S_RACK_numerator', 'B_RACK_CAPACITY', 'S_RACK_CAPACITY', 'TIME'])
        for i, area in enumerate(['D10', 'D9', 'E6', 'F21']):
            area_df = constraint[(constraint.WHS_TYPE.isin(['L10電子倉'])) & (constraint.AREA.isin([area]))]
            area_constraint = area_df.RACK_LOCATION_START_STR.values[0].split(',')
            area_result = result[result.FLOOR.isin([area])]
            B_RACK_denominator = area_df.B_RACK_NUMBER.values[0]
            S_RACK_denominator = area_df.S_RACK_NUMBER.values[0]

            tuple(area_constraint)
            B_rack_num = len(area_result[area_result.LOCATION.str.startswith(tuple(area_constraint))].LOCATION.unique())
            S_rack_num = len(area_result[-area_result.LOCATION.str.startswith(tuple(area_constraint))].LOCATION.unique())
            fac = 'FOL'
            BU = 'NSD'
            whs_type = 1

            all_whs_capacity.loc[i] = [fac, BU, whs_type, area,
                                    B_RACK_denominator, B_rack_num,
                                    S_RACK_denominator, S_rack_num,
                                    round(100 * B_rack_num / B_RACK_denominator, 2),
                                    round(100 * S_rack_num / S_RACK_denominator, 2), datetime.now().date()]
        insert_sql = '''
        INSERT IGNORE INTO all_whs_storage_capacity
            (SITE, BU, WHS_TYPE, AREA, B_RACK_denominator,
                B_RACK_numerator, S_RACK_denominator, S_RACK_numerator,
                B_RACK_CAPACITY, S_RACK_CAPACITY, TIME)
        VALUES (%s, %s, %s, %s, %s,
                %s, %s, %s,
                %s, %s, %s)
        on DUPLICATE KEY UPDATE 
            SITE = VALUES(SITE), BU = VALUES(BU), WHS_TYPE = VALUES(WHS_TYPE), AREA = VALUES(AREA), B_RACK_denominator = VALUES(B_RACK_denominator),
            B_RACK_numerator = VALUES(B_RACK_numerator), S_RACK_denominator = VALUES(S_RACK_denominator), S_RACK_numerator = VALUES(S_RACK_numerator),
            B_RACK_CAPACITY = VALUES(B_RACK_CAPACITY), S_RACK_CAPACITY = VALUES(S_RACK_CAPACITY), TIME = VALUES(TIME)'''
        data_to_db = []
        for i, row in all_whs_capacity.iterrows():
            temp_data = (row['SITE'], row['BU'], row['WHS_TYPE'], row['AREA'], row['B_RACK_denominator'],
                            row['B_RACK_numerator'], row['S_RACK_denominator'], row['S_RACK_numerator'],
                            row['B_RACK_CAPACITY'], row['S_RACK_CAPACITY'], row['TIME'])    
            data_to_db.append(temp_data)
        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)
    except:
        print('update NSD storage capacity result failed')
    # HWD 儲位容載率
    try:
        HWD_Coop_DB = BasicORACLE(username=Allpart_HWD['username'], password=Allpart_HWD['password'],
                            hostname=Allpart_HWD['hostname'],
                            port=Allpart_HWD['port'], db_name=Allpart_HWD['db_name'])

        HWD_WHITE_DB = BasicORACLE(username=Allpart_HWD_white['username'], password=Allpart_HWD_white['password'],
                            hostname=Allpart_HWD_white['hostname'],
                            port=Allpart_HWD_white['port'], db_name=Allpart_HWD_white['db_name'])

        # HWD 儲位容載率
        sql ='''
        SELECT A.TR_SN, e.LOCATION as CUST_LOCATION, A.LOCATION, A.QTY, A.CUST_KP_NO, A.OVER_TIME, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME
                        FROM MES4.R_WHS_LOCATION_NEW  A,  MES4.R_TR_SN b, MES1.C_KITTING_STOCK_CONFIG e
                        WHERE a.TR_SN = b.TR_SN 
                        AND a.CUST_KP_NO = b.CUST_KP_NO
                        AND a.CUST_KP_NO = e.CUST_KP_NO
                        AND  b.WORK_FLAG = '0'
                        AND  b.LOCATION_FLAG in ('C')
                        '''
        Coop_LOCATION = HWD_Coop_DB.manipulate_db(sql, dtype='DataFrame')

        sql ='''
        SELECT A.TR_SN, e.LOCATION as CUST_LOCATION, A.LOCATION, A.QTY, A.CUST_KP_NO, A.OVER_TIME, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME
                        FROM MES4.R_WHS_LOCATION_NEW  A,  MES4.R_TR_SN b, MES1.C_KITTING_STOCK_CONFIG e
                        WHERE a.TR_SN = b.TR_SN 
                        AND a.CUST_KP_NO = b.CUST_KP_NO
                        AND a.CUST_KP_NO = e.CUST_KP_NO
                        AND  b.WORK_FLAG = '0'
                        AND  b.LOCATION_FLAG in ('0')
                        '''
        WHITE_LOCATION = HWD_WHITE_DB.manipulate_db(sql, dtype='DataFrame')
        result = pd.concat([Coop_LOCATION,WHITE_LOCATION])

        #小貨架
        s_constraint_str = 'CC1,CC2,CC3,CC4,CC5,CC6,CC7,CC8,CC15,CC16,CC17,CC18,CC19,CC20,CC21,CC22,CC23,CC24,CC48,CC85,CC90,0-0-0'
        #大貨架
        b_constraint_str = 'CC81,CC82,CC83,CC84,CC89,0-0-0'

        s_constraint = s_constraint_str.split(',')
        b_constraint = b_constraint_str.split(',')
        area_constraint = b_constraint+s_constraint

        s_rack_location = result[result.CUST_LOCATION.str.startswith(tuple(s_constraint))]
        b_rack_location = result[result.CUST_LOCATION.str.startswith(tuple(b_constraint))]

        s_rack_location['LOCATION'] = np.where(s_rack_location.LOCATION == '0-0-0', '0-0-0', s_rack_location.loc[:, 'CUST_LOCATION'])
        b_rack_location['LOCATION'] = np.where(b_rack_location.LOCATION == '0-0-0', '0-0-0', b_rack_location.loc[:, 'CUST_LOCATION'])

        Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])
        sql = '''
        SELECT *
        FROM all_whs_storage_capacity_constraint
        '''
        constraint = Stock_DB.manipulate_db(sql, dtype='DataFrame')

        hwd_whs_capacity = pd.DataFrame(columns= ['SITE', 'BU', 'WHS_TYPE', 'AREA', 'B_RACK_denominator', 'B_RACK_numerator', 'S_RACK_denominator', 'S_RACK_numerator', 'B_RACK_CAPACITY', 'S_RACK_CAPACITY', 'TIME'])

        area = 'F8'
        area_df = constraint[(constraint.WHS_TYPE.isin(['L10電子倉'])) & (constraint.AREA.isin([area]))]

        B_RACK_denominator = area_df.B_RACK_NUMBER.values[0]
        S_RACK_denominator = area_df.S_RACK_NUMBER.values[0]

        B_rack_num = len(b_rack_location.LOCATION.unique())
        S_rack_num = len(s_rack_location.LOCATION.unique())
        fac = 'FOL'
        BU = 'HWD'
        whs_type = 1

        hwd_whs_capacity.loc[0] = [fac, BU, whs_type, area,
                                B_RACK_denominator, B_rack_num,
                                S_RACK_denominator, S_rack_num,
                                round(100 * B_rack_num / B_RACK_denominator, 2),
                                round(100 * S_rack_num / S_RACK_denominator, 2), datetime.now().date()]
        insert_sql = '''
        INSERT IGNORE INTO all_whs_storage_capacity
            (SITE, BU, WHS_TYPE, AREA, B_RACK_denominator,
                B_RACK_numerator, S_RACK_denominator, S_RACK_numerator,
                B_RACK_CAPACITY, S_RACK_CAPACITY, TIME)
        VALUES (%s, %s, %s, %s, %s,
                %s, %s, %s,
                %s, %s, %s)
        on DUPLICATE KEY UPDATE 
            SITE = VALUES(SITE), BU = VALUES(BU), WHS_TYPE = VALUES(WHS_TYPE), AREA = VALUES(AREA), B_RACK_denominator = VALUES(B_RACK_denominator),
            B_RACK_numerator = VALUES(B_RACK_numerator), S_RACK_denominator = VALUES(S_RACK_denominator), S_RACK_numerator = VALUES(S_RACK_numerator),
            B_RACK_CAPACITY = VALUES(B_RACK_CAPACITY), S_RACK_CAPACITY = VALUES(S_RACK_CAPACITY), TIME = VALUES(TIME)'''
        data_to_db = []
        for i, row in hwd_whs_capacity.iterrows():
            temp_data = (row['SITE'], row['BU'], row['WHS_TYPE'], row['AREA'], row['B_RACK_denominator'],
                            row['B_RACK_numerator'], row['S_RACK_denominator'], row['S_RACK_numerator'],
                            row['B_RACK_CAPACITY'], row['S_RACK_CAPACITY'], row['TIME'])    
            data_to_db.append(temp_data)
        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)
    except:
        print('HWD update failed')

    # CBD 儲位容載率
    try:
        AllPart_DB_CBD = BasicORACLE(username=Allpart_CBD['username'], password=Allpart_CBD['password'],
                                    hostname=Allpart_CBD['hostname'],
                                    port=Allpart_CBD['port'], db_name=Allpart_CBD['db_name'])

        # CBD 儲位容載率
        sql = '''
        SELECT 
            A.TR_SN, e.KP_TYPE as MATERIAL_TYPE,
            A.LOCATION, A.QTY, A.CUST_KP_NO, A.OVER_TIME, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME
        FROM 
            MES4.R_WHS_LOCATION_NEW A, MES4.R_TR_SN b, MES1.C_WHS_MATERIA_TYPE_CONFIG e
        WHERE a.TR_SN = b.TR_SN 
            AND a.CUST_KP_NO = b.CUST_KP_NO
            AND a.CUST_KP_NO = e.CUST_KP_NO
            AND  b.WORK_FLAG = '0'
            AND  b.LOCATION_FLAG in ('0')
        '''
        CBD_LOCATION = AllPart_DB_CBD.manipulate_db(sql, dtype='DataFrame')

    #小貨架
        s_constraint_str = 'BC,BD,0-0-0'
        #大貨架
        b_constraint_str = 'BE,BH,0-0-0'

        s_constraint = s_constraint_str.split(',')
        b_constraint = b_constraint_str.split(',')
        area_constraint = b_constraint+s_constraint

        s_rack_location = CBD_LOCATION[CBD_LOCATION.LOCATION.str.startswith(tuple(s_constraint))]
        b_rack_location = CBD_LOCATION[CBD_LOCATION.LOCATION.str.startswith(tuple(b_constraint))]
        '''
        s_rack_location['LOCATION'] = np.where(s_rack_location.LOCATION == '0-0-0', '0-0-0', s_rack_location.loc[:, 'CUST_LOCATION'])
        b_rack_location['LOCATION'] = np.where(b_rack_location.LOCATION == '0-0-0', '0-0-0', b_rack_location.loc[:, 'CUST_LOCATION'])
        '''
        b_rack_location
        Stock_DB = BasicMySQL(username=MYSQL_login_info['username'], password=MYSQL_login_info['password'],
                            hostname=MYSQL_login_info['hostname'],
                            port=MYSQL_login_info['port'], db_name=MYSQL_login_info['db_name'])
        sql = '''
        SELECT *
        FROM all_whs_storage_capacity_constraint
        '''
        constraint = Stock_DB.manipulate_db(sql, dtype='DataFrame')

        csd_whs_capacity = pd.DataFrame(columns= ['SITE', 'BU', 'WHS_TYPE', 'AREA', 'B_RACK_denominator', 'B_RACK_numerator', 'S_RACK_denominator', 'S_RACK_numerator', 'B_RACK_CAPACITY', 'S_RACK_CAPACITY', 'TIME'])

        area = 'E5'
        area_df = constraint[(constraint.WHS_TYPE.isin(['L10電子倉'])) & (constraint.AREA.isin([area]))]

        B_RACK_denominator = area_df.B_RACK_NUMBER.values[0]
        S_RACK_denominator = area_df.S_RACK_NUMBER.values[0]

        B_rack_num = len(b_rack_location.LOCATION.unique())
        S_rack_num = len(s_rack_location.LOCATION.unique())
        fac = 'FOL'
        BU = 'CSD'
        whs_type = 1

        csd_whs_capacity.loc[0] = [fac, BU, whs_type, area,
                                B_RACK_denominator, B_rack_num,
                                S_RACK_denominator, S_rack_num,
                                round(100 * B_rack_num / B_RACK_denominator, 2),
                                round(100 * S_rack_num / S_RACK_denominator, 2), datetime.now().date()]

        insert_sql = '''
        INSERT IGNORE INTO all_whs_storage_capacity
            (SITE, BU, WHS_TYPE, AREA, B_RACK_denominator,
                B_RACK_numerator, S_RACK_denominator, S_RACK_numerator,
                B_RACK_CAPACITY, S_RACK_CAPACITY, TIME)
        VALUES (%s, %s, %s, %s, %s,
                %s, %s, %s,
                %s, %s, %s)
        on DUPLICATE KEY UPDATE 
            SITE = VALUES(SITE), BU = VALUES(BU), WHS_TYPE = VALUES(WHS_TYPE), AREA = VALUES(AREA), B_RACK_denominator = VALUES(B_RACK_denominator),
            B_RACK_numerator = VALUES(B_RACK_numerator), S_RACK_denominator = VALUES(S_RACK_denominator), S_RACK_numerator = VALUES(S_RACK_numerator),
            B_RACK_CAPACITY = VALUES(B_RACK_CAPACITY), S_RACK_CAPACITY = VALUES(S_RACK_CAPACITY), TIME = VALUES(TIME)'''
        data_to_db = []
        for i, row in csd_whs_capacity.iterrows():
            temp_data = (row['SITE'], row['BU'], row['WHS_TYPE'], row['AREA'], row['B_RACK_denominator'],
                            row['B_RACK_numerator'], row['S_RACK_denominator'], row['S_RACK_numerator'],
                            row['B_RACK_CAPACITY'], row['S_RACK_CAPACITY'], row['TIME'])    
            data_to_db.append(temp_data)
        Stock_DB.manipulate_db(insert_sql, data_list=data_to_db)
    except:
        print('CBD update failed')
   