from django.db import models
from fii_ai_api.utils.dbio.mysql import MySQL
from stock.config import *
import pandas as pd
from datetime import datetime
# cust frequency Table
MYSQL_login_info = {
    'username': 'dev',
    'password': 'Foxconn88',
    'hostname': '10.124.131.87',
    'port': 8876,
    'db_name': 'stock_db',
}

def last_three_month_sql():
    y = datetime.now().year
    m = datetime.now().month
    if m == 2:
        y2 = y
        m2 = 1
        y3 = y - 1
        m3 = 12
    elif m == 1:
        y2 = y - 1
        m2 = 12
        y3 = y - 1
        m3 = 11
    else:
        y2 = y
        y3 = y
        m2 = m - 1
        m3 = m - 2

    sql = ''' and (YEAR = %(y1)s and MONTH = %(m1)s )
            or (YEAR = %(y2)s and MONTH = %(m2)s ) 
            or (YEAR = %(y3)s and MONTH = %(m3)s ) 
        ''' % ({'y1': y, 'm1': m,
                'y2': y2, 'm2': m2,
                'y3': y3, 'm3': m3})
    return sql

class IAI_MySQLIO(MySQL):
    def __init__(self, debug=False, db_tables={}, custom_login_info={}, **kwargs):
        super().__init__(debug=debug, db_tables=db_tables, login_info=MYSQL_login_info, **kwargs)

    # 抓取過去超時未完成的DN/PN
    def cust_frquency_df(self):
        month_sql = last_three_month_sql()
        sql = '''
        SELECT CUST_KP_NO, YEAR, MONTH, TOTAL_TIMES, TOTAL_USE_TIMES, LOCATION, KEEPER_EMP
            FROM NSD_L10_D_material_storage_cust_usage_frquency
        where 1=1 %(month_sql)s
        '''% ({'month_sql' : month_sql})

        return self.manipulate_db(sql, dtype='DataFrame')

    def storage_space_df(self):
        sql = '''
        SELECT VALID_LOCATION, PRIORITY
            FROM NSD_L10_D_material_storage_space
        '''

        return self.manipulate_db(sql, dtype='DataFrame')
    
    def decicion_result_by_ratio(self, percentile):
        sql = '''
        SELECT
            KEEPER_EMP, CUST_KP_NO,
            ORIGIN_LOCATION, original_TOTAL_TIMES, original_TOTAL_USE_TIMES,
            change_CUST_KP_NO,
            IMPROVE_LOCATION, improved_TOTAL_TIMES, improved_TOTAL_USE_TIMES,
            TIME, UPDATE_TIME
        FROM
            NSD_L10_D_material_storage_space_decicion_result
        WHERE
            CONDITION_USAGE_RATIO = %(CONDITION_USAGE_RATIO)s
            and TIME = (SELECT max(TIME) FROM NSD_L10_D_material_storage_space_decicion_result)
        '''% ({'CONDITION_USAGE_RATIO' : percentile})
        return self.manipulate_db(sql, dtype='DataFrame')

    def decicion_result_by_frequency(self, threshold):
        sql = '''
        SELECT
            KEEPER_EMP, CUST_KP_NO,
            ORIGIN_LOCATION, original_TOTAL_TIMES, original_TOTAL_USE_TIMES,
            change_CUST_KP_NO,
            IMPROVE_LOCATION, improved_TOTAL_TIMES, improved_TOTAL_USE_TIMES,
            TIME, UPDATE_TIME
        FROM
            NSD_L10_D_material_storage_space_decicion_result
        WHERE
            CONDITION_USAGE_TIME = %(CONDITION_USAGE_TIME)s
            and TIME = (SELECT max(TIME) FROM NSD_L10_D_material_storage_space_decicion_result)

        ''' % ({'CONDITION_USAGE_TIME' : threshold})
        return self.manipulate_db(sql, dtype='DataFrame')