# -*- coding: utf-8 -*-
import pandas as pd
from stock.WHS.stock_out.Electric.Electrical_WHS_AR_Realtime_read_DB import *
from datetime import datetime, timedelta


# =============================================================================
# Tools function
# cal_whs_cust_qty : 計算庫存數，考慮FIFO概念，輸出可用 & old date code
# predict_function : 計算停線時間
# predict_realtime_unfin_stopline : 預估停線
# =============================================================================


def datetoweek(input):
    date = pd.to_datetime(input, format='%Y/%m/%d')
    wo_strat_time = date.floor('d') + timedelta(hours=12)
    wo_END_TIME = date.floor('d') + timedelta(days=1, hours=12)  # 抓24H
    return wo_strat_time, wo_END_TIME


def replace_time(row):
    row['NEW_START_TIME'] = row['NEW_START_TIME'].replace(minute=0, second=0, microsecond=0)
    return row


# 計算庫存數，考慮FIFO概念，輸出可用 & old date code
def cal_whs_cust_qty(whs_stock):

    location_cust = whs_stock['CUST_KP_NO'].drop_duplicates().reset_index(drop=True).to_frame()

    # 分ODC
    cust_qty_df = whs_stock[whs_stock['old_date_code'] == False].reset_index(drop=True)
    cust_qty_odc_df = whs_stock[whs_stock['old_date_code'] == True].reset_index(drop=True)

    cust_qty_df = cust_qty_df.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY'})
    cust_qty_odc_df = cust_qty_odc_df.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY_ODC'})

    # 結合表
    location_cust = location_cust.merge(cust_qty_df, on=['CUST_KP_NO'], how='left')
    location_cust = location_cust.merge(cust_qty_odc_df, on=['CUST_KP_NO'], how='left')
    location_cust = location_cust.fillna(0)

    return location_cust


# 計算庫存數，考慮FIFO概念，輸出可用 & old date code
def cal_whs_cust_qty_hwd(whs_stock):

    location_cust = whs_stock['CUST_KP_NO'].drop_duplicates().reset_index(drop=True).to_frame()
    over_cust = whs_stock[whs_stock['old_date_code']].groupby(
        ['CUST_KP_NO'])['DATA2'].min().reset_index().rename(columns={'DATA2' : 'old_date_time'})
    whs_stock = whs_stock.merge(over_cust, on='CUST_KP_NO', how='left')
    whs_stock.loc[whs_stock['old_date_time'].isna(), 'old_date_time'] = pd.Timestamp.now().floor('d') + timedelta(weeks=520)
    ## 進料日期 比 olddatecode的最早進料日還早的料
    whs_stock['can_use'] = whs_stock['old_date_time'] >= whs_stock['DATA2']
    ''' old_date_code    can_use
                0             0    =>後來進的料 若沒有發生old date code 則可用
                0             1    =>最早進的料 一定可用!!!!!!!
                1             0    =>較晚進的料 但old date code
                1             1    =>最早發生的old date code 料
    '''
    ## 先計算01   、   10,11   、  00  分三組
    cust_00_qty = whs_stock[((whs_stock['can_use'] == False) & (whs_stock['old_date_code'] == False))]
    cust_01_qty = whs_stock[((whs_stock['can_use'] == True) & (whs_stock['old_date_code'] == False))]
    cust_1x_qty = whs_stock[(whs_stock['old_date_code'] == True)]

    # 加總數量
    cust_00_qty = cust_00_qty.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'temp_cust'})
    cust_01_qty = cust_01_qty.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY'})
    cust_1x_qty = cust_1x_qty.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY_ODC'})

    # 結合表
    location_cust = location_cust.merge(cust_00_qty, on=['CUST_KP_NO'], how='left')
    location_cust = location_cust.merge(cust_01_qty, on=['CUST_KP_NO'], how='left')
    location_cust = location_cust.merge(cust_1x_qty, on=['CUST_KP_NO'], how='left').fillna(0)

    # old date code 為0的 temp加回正常庫存
    conditional = location_cust['CUST_QTY_ODC'] == 0
    location_cust.loc[conditional, 'CUST_QTY'] = location_cust.loc[conditional,
                                                                   'CUST_QTY'] + location_cust.loc[conditional, 'temp_cust']
    # old date code 為1的 temp加回ODC庫存
    conditional = location_cust['CUST_QTY_ODC'] > 0

    # location_cust.loc[conditional,'CUST_QTY_ODC'] = location_cust.loc[conditional,'CUST_QTY_ODC'] + location_cust.loc[conditional,'temp_cust']
    location_cust.loc[conditional, 'CUST_QTY_ODC'] = location_cust.loc[conditional, 'CUST_QTY_ODC'] + \
        location_cust.loc[conditional, 'temp_cust'] + location_cust.loc[conditional, 'CUST_QTY']
    location_cust.loc[conditional, 'CUST_QTY'] = 0
    location_cust = location_cust.drop(columns=['temp_cust'])

    return location_cust


# 計算停線時間
def predict_function(cust_df):

    start, end = cust_df.START_TIME.min(), cust_df.END_TIME.max()

    if (start == end):
        stop_time = start
    else:
        cust_df['cost_per_hour'] = cust_df['CUST_REQUEST_distribute'] / cust_df['due_time']
        cust_df['s_point'] = (cust_df['START_TIME'] - start).apply(lambda x: x.seconds / 3600 + x.days * 24)
        cust_df['e_point'] = cust_df['s_point'] + cust_df['due_time']

        deliver = cust_df['DELIVER_QTY_v2'].sum()
        cost_point = pd.concat([cust_df['s_point'], cust_df['e_point']]).drop_duplicates().sort_values().reset_index(drop=True)
        cost = 0
        produce_time = 0
        for i in range(len(cost_point) - 1):
            # 消耗率+
            cost = cost + cust_df[cust_df['s_point'] == cost_point[i]]['cost_per_hour'].sum()
            # 消耗率-
            cost = cost - cust_df[cust_df['e_point'] == cost_point[i]]['cost_per_hour'].sum()
            #開始扣料
            if deliver > (cost * (cost_point[i + 1] - cost_point[i])):  # 可發量大於這節需求
                deliver = deliver - (cost * (cost_point[i + 1] - cost_point[i]))
                produce_time = cost_point[i + 1]
            else:  # 可發量小於等於這節時間需求
                produce_time = (cost_point[i + 1] - cost_point[i]) * (deliver
                                                                      / (cost * (cost_point[i + 1] - cost_point[i]))) + cost_point[i]
                break

        stop_time = start + timedelta(hours=produce_time)

    return stop_time


# 預估停線
def predict_realtime_unfin_stopline(ALLPARTS_DB, unfin_realtime_table, AREA):

    # 預估停線清單
    realtime_temp_table = pd.DataFrame(unfin_realtime_table, columns=[
                                       'WO', 'V_WO', 'CUST_KP_NO', 'PART_TYPE', 'WO_REQUEST', 'DELIVER_QTY'])
    # (所有未完成料)工單轉字串
    wo_list = realtime_temp_table['WO'].drop_duplicates()
    wo_str = ("'" + "','".join(wo_list) + "'")

    # 載入APS 工單
    aps_wo = aps_schedule(AREA, mode=2, wo_list=wo_str)
    aps_wo = aps_wo.rename(columns={'SECTION' : 'PROCESS_FLAG', 'WO_REQUEST': 'APS_QTY'})
    aps_wo = aps_wo.drop_duplicates()
    aps_wo['due_time'] = aps_wo['END_TIME'] - aps_wo['START_TIME']
    predict_list = realtime_temp_table.merge(aps_wo, on=['WO'], how='left')

    # 載入 工單機種對照表
    wo_pno_table = wo_pno(ALLPARTS_DB, wo_str)
    predict_list = predict_list.merge(wo_pno_table, on=['WO'], how='left')
    predict_list.loc[~predict_list['P_NO_V2'].isna(), 'P_NO'] = predict_list.loc[~predict_list['P_NO_V2'].isna(), 'P_NO_V2']
    predict_list = predict_list.drop(columns=['P_NO_V2'])

    # (所有未完成料)機種轉字串
    pno_list = predict_list['P_NO'].drop_duplicates()
    pno_str = ("'" + "','".join(pno_list) + "'")

    # 載入 物料標準用量需求 #,A.SMT_CODE
    if AREA == 'F8':
        today_cust_std = cust_std_table_hwd(ALLPARTS_DB, pno_str)
    else:
        today_cust_std = cust_std_table(ALLPARTS_DB, pno_str)

    # PCB之用料
    predict_list_pcb = predict_list[predict_list['PART_TYPE'] == '9']

    # 非PCB
    predict_list = predict_list[predict_list['PART_TYPE'] != '9']

    # 讓表today_cust_std  每WO + CUST_KP_NO 僅取最大 std  唯一值  在合併
    today_cust_std = today_cust_std.groupby(['CUST_KP_NO', 'P_NO', 'PROCESS_FLAG'])['STANDARD_QTY'].max().reset_index()
    predict_list = predict_list.merge(today_cust_std, on=['P_NO', 'PROCESS_FLAG', 'CUST_KP_NO'], how='left')

    # 挑出 B、T階無對應之 工單+料   (無法剔除 以B、T面共同使用來計)
    # predict_list['STANDARD_QTY'] = predict_list['STANDARD_QTY'].fillna(0)
    predict_list_temp = predict_list.copy()
    predict_list_temp['STANDARD_QTY'] = predict_list_temp['STANDARD_QTY'].fillna(0)
    cant_delete = predict_list_temp.groupby(['WO', 'CUST_KP_NO'])['STANDARD_QTY'].sum().reset_index()
    cant_delete = cant_delete[cant_delete['STANDARD_QTY'] == 0].drop(columns='STANDARD_QTY')
    cant_delete['stay'] = 1  # 無法找到使用B、T面的清單
    predict_list = predict_list.merge(cant_delete, on=['WO', 'CUST_KP_NO'], how='left')

    #清除該料 在該面未使用的情形
    predict_list.loc[~predict_list['STANDARD_QTY'].isna(), 'stay'] = 1
    predict_list = predict_list.drop(predict_list[predict_list['stay'].isna()].index).drop(columns=['stay'])

    if len(predict_list_pcb) > 0:
        # PCB 料 case  只用單面    找最早上線面別，取該面別 並剔除另一面別訊息
        special_case = predict_list_pcb.groupby(['WO', 'CUST_KP_NO'])['START_TIME'].min().reset_index()
        special_case['can_use'] = 1
        predict_list_pcb = predict_list_pcb.merge(special_case, on=['WO', 'CUST_KP_NO', 'START_TIME'], how='left')
        predict_list_pcb['can_use'] = predict_list_pcb['can_use'].fillna(0)
        special_case = predict_list_pcb.groupby(['WO', 'CUST_KP_NO', 'PROCESS_FLAG'])['can_use'].sum().reset_index()
        need_delete = special_case[special_case['can_use'] == 0]
        predict_list_pcb = predict_list_pcb.drop(columns='can_use')
        predict_list_pcb = predict_list_pcb.merge(need_delete, on=['WO', 'CUST_KP_NO', 'PROCESS_FLAG'], how='left')
        predict_list_pcb = predict_list_pcb.drop(
            predict_list_pcb[predict_list_pcb['can_use'] == 0].index).reset_index(drop=True).drop(columns='can_use')
        predict_list_pcb['STANDARD_QTY'] = 1

        # 合併PCB的工單和料  每張皆對應 PROCESS_FLAG + STANDARD_QTY
        predict_list = pd.concat([predict_list, predict_list_pcb], sort=False).sort_values(
            ['START_TIME', 'WO', 'CUST_KP_NO']).reset_index(drop=True)
    predict_list['STANDARD_QTY'] = predict_list['STANDARD_QTY'].fillna(1)

    # 計算 同工單中 同線別 同面別 的使用時間總和   叫total time
    apstemp = predict_list.groupby(['WO', 'CUST_KP_NO', 'PROCESS_FLAG'])['due_time'].sum(
    ).reset_index().rename(columns={'due_time' : 'flag_total_time'})
    apstemp2 = predict_list.groupby(['WO', 'CUST_KP_NO', 'PROCESS_FLAG'])['STANDARD_QTY'].max().reset_index().groupby(
        ['WO', 'CUST_KP_NO'])['STANDARD_QTY'].sum().reset_index().rename(columns={'STANDARD_QTY' : 'total_std_qty'})
    predict_list = predict_list.merge(apstemp, on=['WO', 'CUST_KP_NO', 'PROCESS_FLAG'], how='left')
    predict_list = predict_list.merge(apstemp2, on=['WO', 'CUST_KP_NO'], how='left')

    # 計算 同綁定工單中 同料 已發總和
    apstemp3 = realtime_temp_table.groupby(['V_WO', 'CUST_KP_NO'])['DELIVER_QTY'].sum(
    ).reset_index().rename(columns={'DELIVER_QTY' : 'v_wo_total_deliver_qty'})
    predict_list = predict_list.merge(apstemp3, on=['V_WO', 'CUST_KP_NO'], how='left')

    # 估計每一段時間(線別)料的需求用量
    predict_list['CUST_REQUEST_distribute'] = predict_list['WO_REQUEST'] * \
        (predict_list['due_time'] / predict_list['flag_total_time'])
    predict_list['CUST_REQUEST_distribute'] = predict_list['CUST_REQUEST_distribute'] * \
        (predict_list['STANDARD_QTY'] / predict_list['total_std_qty'])

    # 檢查 同V_OW 同CUT_KP_NO下 每的線別德時間和以配的料是否有順序不合之問題 !! 有話重新分配料
    predict_list = predict_list.sort_values('START_TIME')

    # 將需求做cumsum
    predict_list['request_cumsum'] = predict_list.groupby(['V_WO', 'CUST_KP_NO'])['CUST_REQUEST_distribute'].cumsum()

    # 將以發總量 減去 需求cumsum = 分配各工單量
    predict_list['DE_distribute'] = predict_list['v_wo_total_deliver_qty'] - predict_list['request_cumsum']

    # DE_distribute >= 0 表示 已備量滿足需求 DELIVER_QTY = WO_REQUEST
    predict_list.loc[(predict_list['DE_distribute'] >= 0), 'DELIVER_QTY_v2'] = predict_list.loc[(
        predict_list['DE_distribute'] >= 0), 'CUST_REQUEST_distribute']

    # DE_distribute < 0 表示 已備量不足需求 DELIVER_QTY < WO_REQUEST   SO: DELIVER_QTY= WO_REQUEST+DE_distribute
    predict_list.loc[(predict_list['DE_distribute'] < 0), 'DELIVER_QTY_v2'] = predict_list.loc[(
        predict_list['DE_distribute'] < 0), 'CUST_REQUEST_distribute'] + predict_list.loc[(predict_list['DE_distribute'] < 0), 'DE_distribute']

    # DELIVER_QTY= WO_REQUEST+DE_distribute <0  設等於0
    predict_list.loc[(predict_list['DELIVER_QTY_v2'] < 0), 'DELIVER_QTY_v2'] = 0

    # 換算時間單位-> 小時
    predict_list['due_time'] = predict_list['due_time'].apply(lambda x: x.seconds / 3600 + x.days * 24)

    # 計算停線時間
    predict_stop_time = predict_list.groupby(['WO', 'CUST_KP_NO'])[
        'DELIVER_QTY_v2', 'START_TIME', 'END_TIME', 'due_time', 'CUST_REQUEST_distribute'].apply(lambda x: predict_function(x)).reset_index()
    predict_stop_time = predict_stop_time.rename(columns={0: 'STOP_LINE_POINT'})

    # 合併預估停線時間
    predict_stop = pd.DataFrame(unfin_realtime_table, columns=[
                                'WO', 'CUST_KP_NO', 'WO_REQUEST', 'DELIVER_QTY', 'START_TIME', 'END_TIME'])
    predict_stop = predict_stop.merge(predict_stop_time, on=['WO', 'CUST_KP_NO'], how='left')

    return predict_stop, predict_list


# =============================================================================
# General Function
# prepare_wo_list : Today wo cust 準備今日工單列表
# wo_request : 每一份工單的備料需求列表
# cust_loction_cal : 計算倉庫庫存資訊 和 備料需求合併
# cust_achieve_state : 計算現在備料狀態、預判庫存、原始庫存、準時超時
# generate_realtime_table : 合併作業員維護訊息、物料管理人訊息、(找料訊息)
# =============================================================================


def prepare_wo_list(ALLPARTS_DB, AREA, FLAG, WO_HEAD):

    # st, ed = pd.Timestamp.now().floor('d'), pd.Timestamp.now().floor('d') + timedelta(days=1, hours=12)
    (st, ed) = datetoweek(str(pd.Timestamp.now()))  # 今日

    # APS WO list
    aps_wo = aps_schedule(AREA, mode=1)  # mode 1 : 一般用

    # 工單整機台上線線時間
    wip_wo_time = real_start_end_time_wo()

    # 每日維護工單表單
    wo_pda_edit = pda_prepare_table(ALLPARTS_DB, AREA, FLAG)

    if len(wo_pda_edit) > 0:  # 有時會連線錯誤
        wo_pda_edit = wo_pda_edit.drop_duplicates('WO')

        # PDA WO 篩選APS有的工單
        wo_pda_edit = wo_pda_edit[wo_pda_edit.WO.isin(aps_wo.WO)]

    # 綁定工單列表
    vo_wo = v_wo_table(ALLPARTS_DB)

    # 工單篩選當日
    aps_wo['NEW_START_TIME'] = aps_wo['START_TIME']
    aps_wo = aps_wo.apply(replace_time, axis=1)
    today_aps = aps_wo[(aps_wo.NEW_START_TIME < ed) & (
        aps_wo.NEW_START_TIME >= pd.Timestamp.now().floor('d'))].WO.reset_index()
    today_aps = today_aps.merge(wo_pda_edit, on='WO', how='outer')  # 取pda和今日12:00-12:00的聯集

    # 將篩選完的工單，載入上下線時間資訊
    today_wo = aps_wo[aps_wo.WO.isin(today_aps.WO)]

    # 上線時間排序及去重複
    today_wo = today_wo.sort_values('START_TIME')

    # 去重複   start取最早   end 取最晚
    today_wo = today_wo.groupby('WO').agg({'START_TIME': 'min', 'END_TIME': 'max'}).reset_index()

    # 減去今天以前下線工單
    today_wo = pd.DataFrame(today_wo.loc[today_wo['END_TIME'] > pd.Timestamp.now().floor('d') , ('WO', 'START_TIME', 'END_TIME')])

    # 篩選D區工單
    today_wo = today_wo[today_wo.WO.str.startswith(WO_HEAD)]

    # 結合綁定工單info
    today_vo_wo = vo_wo[vo_wo.T_WO.isin(today_wo.WO)]  # 篩今日有的綁定
    today_vo_wo = vo_wo[vo_wo.V_WO.isin(today_vo_wo.V_WO)]  # 篩有綁定含非今日的工單(all)
    other_vo_wo = today_vo_wo.drop(vo_wo[vo_wo.T_WO.isin(today_wo.WO)].index)  # 篩非今日工單但，今日同綁定工單

    today_wo = today_wo.merge(vo_wo, left_on='WO', right_on='T_WO', how='left')
    other_wo = aps_wo[aps_wo.WO.isin(other_vo_wo.T_WO)].groupby('WO').agg({'START_TIME': 'min', 'END_TIME': 'max'}).reset_index()
    other_wo = other_wo.merge(other_vo_wo, left_on='WO', right_on='T_WO', how='right')

    # 將當天 和  非當天但同V_WO的工單串接
    today_wo = pd.concat([today_wo, other_wo], join="inner").sort_values('START_TIME')
    today_wo.loc[today_wo['WO'].isna(), 'WO'] = today_wo.loc[today_wo['WO'].isna(), 'T_WO']

    #合併 整機台上下線時間
    today_wo = today_wo.merge(wip_wo_time, on='WO', how='left')
    today_wo.loc[~today_wo['real_end'].isna(), 'END_TIME'] = today_wo.loc[~today_wo['real_end'].isna(), 'real_end']
    today_wo.loc[today_wo['START_TIME'].isna(), 'START_TIME'] = today_wo.loc[today_wo['START_TIME'].isna(), 'real_start']

    wo_list = today_wo.WO.drop_duplicates().reset_index().WO

    return today_wo, wo_list


#  每一份工單的備料需求列表
def wo_request(ALLPARTS_DB, today_wo, wo_list, AREA, FLAG, SAP_STOCK):

    # 從需求料量中篩選當日要備工單
    wo_list_str = ("'" + "','".join(wo_list) + "'")
    today_wo_request = wo_request_table(ALLPARTS_DB, wo_list_str)

    # 物料訊息
    part_config = part_config_table(ALLPARTS_DB, AREA, FLAG)
    part_config = part_config.sort_values(['CUST_KP_NO', 'EDIT_TIME'], ascending=False).drop_duplicates(['CUST_KP_NO'])
    today_wo_request = today_wo_request.merge(part_config, on='CUST_KP_NO', how='left').dropna()

    # 工單物料需求結合物料資訊 去除沒有資訊的物料
    if FLAG == 'A':
        today_A_S_request = wo_A_S_request_table(ALLPARTS_DB, SAP_STOCK, wo_list_str)
        today_wo_request = today_wo_request.merge(today_A_S_request, on=['WO', 'CUST_KP_NO'], how='left')

        if ALLPARTS_DB == 'HWD_white':
            today_wo_request['DELIVER_QTY'] = today_wo_request.apply(
                lambda row : row['CHECKOUT_QTY'] if row['DELIVER_QTY'] == 0 else row['DELIVER_QTY'], axis=1)

    # 加入WO上線時間  D區 當日 要備的wo&料單 需求 已備 上線時間
    today_wo_request = today_wo_request.merge(today_wo, on='WO', how='left')
    today_wo_request = today_wo_request.sort_values('START_TIME').reset_index(drop=True)

    # 當日表單(1/2) 選要的欄位、結合資訊、去除無資訊的料
    real_time_achieve = pd.DataFrame(today_wo_request, columns=('WO', 'V_WO', 'CUST_KP_NO',
                                     'WO_REQUEST', 'CHECKOUT_QTY', 'DELIVER_QTY', 'START_TIME', 'END_TIME', 'PART_TYPE'))
    real_time_achieve.loc[:, 'CHECKOUT_QTY'] = real_time_achieve.loc[:, 'CHECKOUT_QTY'].fillna(0)
    if len(real_time_achieve) > 0:

        # 綁定工單欄位為空時，帶入一般工單訊息
        real_time_achieve.loc[real_time_achieve['V_WO'].isna(
        ) == True, 'V_WO'] = real_time_achieve.loc[real_time_achieve['V_WO'].isna() == True, 'WO']

        # 計算綁定工單的總request & 總 deliverx量
        bb = real_time_achieve.groupby(['V_WO', 'CUST_KP_NO'])['DELIVER_QTY', 'CHECKOUT_QTY'].sum().reset_index()
        bb.loc[(bb['DELIVER_QTY'] < bb['CHECKOUT_QTY']), 'DELIVER_QTY'] = bb.loc[(
            bb['DELIVER_QTY'] < bb['CHECKOUT_QTY']), 'CHECKOUT_QTY']
        bb = bb.rename(columns={'DELIVER_QTY' : 'De', 'WO_REQUEST' : 'RE_QTY'}).drop(columns=['CHECKOUT_QTY'])

        # 將綁定資訊合併到時時工單中，並每個料號只留一筆最早的綁定工單上線時間
        real_time_achieve = real_time_achieve.merge(bb, on=['V_WO', 'CUST_KP_NO'], how='left')

        # 確定以上線時間排序
        real_time_achieve = real_time_achieve.sort_values('START_TIME')

        # 將需求做cumsum
        real_time_achieve['request_cumsum'] = real_time_achieve.groupby(['V_WO', 'CUST_KP_NO'])['WO_REQUEST'].cumsum()

        # 將以發總量 減去 需求cumsum = 分配各工單量
        real_time_achieve['DE_distribute'] = real_time_achieve['De'] - real_time_achieve['request_cumsum']

        # DE_distribute >= 0 表示 已備量滿足需求 DELIVER_QTY = WO_REQUEST
        real_time_achieve.loc[(real_time_achieve['DE_distribute'] >= 0), 'DELIVER_QTY'] = real_time_achieve.loc[(
            real_time_achieve['DE_distribute'] >= 0), 'WO_REQUEST']

        # DE_distribute < 0 表示 已備量不足需求 DELIVER_QTY < WO_REQUEST   SO: DELIVER_QTY= WO_REQUEST+DE_distribute
        real_time_achieve.loc[(real_time_achieve['DE_distribute'] < 0), 'DELIVER_QTY'] = real_time_achieve.loc[(
            real_time_achieve['DE_distribute'] < 0), 'WO_REQUEST'] + real_time_achieve.loc[(real_time_achieve['DE_distribute'] < 0), 'DE_distribute']

        # DELIVER_QTY= WO_REQUEST+DE_distribute <0  設等於0
        real_time_achieve.loc[(real_time_achieve['DELIVER_QTY'] < 0), 'DELIVER_QTY'] = 0

        # 將超發的料加回到綁定工單中的上線時間最後一張工單
        cc = real_time_achieve.groupby(['V_WO', 'CUST_KP_NO']).agg(
            {'START_TIME': 'max', 'DE_distribute': 'min'}).reset_index().dropna()
        cc = cc[cc['DE_distribute'] > 0].rename(columns={'DE_distribute' : 'over_deliver'})
        real_time_achieve['START_TIME'] = pd.to_datetime(real_time_achieve['START_TIME'])
        real_time_achieve = real_time_achieve.merge(cc, on=['V_WO', 'CUST_KP_NO', 'START_TIME'], how='left')
        real_time_achieve.loc[:, 'over_deliver'] = real_time_achieve.loc[:, 'over_deliver'].fillna(0)
        real_time_achieve.loc[:, 'DELIVER_QTY'] = real_time_achieve.loc[:,
                                                                        'DELIVER_QTY'] + real_time_achieve.loc[:, 'over_deliver']

        real_time_achieve = real_time_achieve.drop(['De', 'DE_distribute', 'request_cumsum', 'over_deliver'], axis=1)

        # 去除今天以前已下線工單
        real_time_achieve = real_time_achieve[real_time_achieve.END_TIME > pd.Timestamp.now().floor('d')]

    return real_time_achieve


# 計算倉庫庫存資訊 和 備料需求合併
def cust_loction_cal(ALLPARTS_DB, AREA, real_time_achieve, WO_HEAD):

    #今日料單
    cust_list = real_time_achieve.CUST_KP_NO.drop_duplicates().reset_index().CUST_KP_NO
    cust_array = [cust_list[i:i + 1000] for i in range(0, len(cust_list), 1000)]
    cust_str = "( A.CUST_KP_NO IN  " + "('" + "','".join(cust_array[0]) + "')"
    for j in range(len(cust_array) - 1):
        cust_str = cust_str + " or " + " A.CUST_KP_NO IN " + "('" + "','".join(cust_array[j + 1]) + "')"
    cust_str += ')'

    if AREA == 'D9':
        AREA = 'D'

    # 庫存資訊
    if AREA == 'F8':
        today_whs = whs_location_table_hwd(ALLPARTS_DB, AREA, cust_str)
    else:
        today_whs = whs_location_table(ALLPARTS_DB, AREA, cust_str)
        # today_whs = whs_location_table_d10(ALLPARTS_DB, AREA, cust_str)

    today_whs = today_whs.drop_duplicates()

    # 物料負責人工號
    empNO_table = cust_config_empNO(ALLPARTS_DB, AREA, cust_str)

    # 有特殊工單號資訊的資料
    today_whs_info = today_whs[today_whs['PRINT_WO'] != ''].drop_duplicates(['CUST_KP_NO', 'PRINT_WO'])
    today_whs_info = pd.DataFrame(today_whs_info, columns=['CUST_KP_NO', 'PRINT_WO'])

    # 存在重複項
    real_time_achieve = real_time_achieve.merge(today_whs_info, on='CUST_KP_NO', how='left').fillna(0)

    # PRINT_WO 統一設為str 避免error
    real_time_achieve['PRINT_WO'] = real_time_achieve['PRINT_WO'].astype('str')

    # SPECIAL CASE = 0  ==> 正常工單
    # SPECIAL CASE = 1  ==> CRONTROL RUN 工單
    real_time_achieve['SPECIAL_CASE'] = real_time_achieve.apply(lambda x : 1 if(x.WO in x.PRINT_WO) else 0 , axis=1)

    # 去除重複資訊 並且已有SPECIAL_CASE 不為0的優先留下
    real_time_achieve = real_time_achieve.sort_values('SPECIAL_CASE', ascending=False).drop_duplicates(['WO', 'CUST_KP_NO'])

    # 載入 DEVIATION 資訊
    # SPECIAL CASE = 2  ==> DEVIATION 工單
    deviation_wo = deviation_wo_table(ALLPARTS_DB, WO_HEAD)
    real_time_achieve = real_time_achieve.merge(deviation_wo, on=['WO', 'CUST_KP_NO'], how='left')
    real_time_achieve.loc[~real_time_achieve['DEVIATION_NO'].isna(), 'SPECIAL_CASE'] = 2
    real_time_achieve.loc[~real_time_achieve['DEVIATION_NO'].isna(
    ), 'PRINT_WO'] = real_time_achieve.loc[~real_time_achieve['DEVIATION_NO'].isna(), 'DEVIATION_NO']
    real_time_achieve = real_time_achieve.drop(columns=['DEVIATION_NO']).fillna(0)

    # 計算 Normal QTY
    today_whs_normal = today_whs[today_whs['PRINT_TYPE'].isin(['0', '1'])].reset_index(drop=True)

    if len(today_whs_normal):

        # 併入正常庫存
        if AREA != 'F8':
            whs_normal_qty = cal_whs_cust_qty(today_whs_normal)
            # whs_normal_qty = cal_whs_cust_qty_hwd(today_whs_normal) # tmp
            real_time_achieve = real_time_achieve.merge(whs_normal_qty, on='CUST_KP_NO', how='left')
        else:
            whs_normal_qty = cal_whs_cust_qty_hwd(today_whs_normal)
            real_time_achieve = real_time_achieve.merge(whs_normal_qty, on='CUST_KP_NO', how='left')

        real_time_achieve['CUST_QTY'] = real_time_achieve['CUST_QTY'].fillna(0)
        real_time_achieve['CUST_QTY_ODC'] = real_time_achieve['CUST_QTY_ODC'].fillna(0)
    else:
        real_time_achieve['CUST_QTY'] = 0
        real_time_achieve['CUST_QTY_ODC'] = 0

    # 計算 control run
    today_whs_controlrun = today_whs[(today_whs['PRINT_TYPE'] == '2')].reset_index(drop=True)

    if len(today_whs_controlrun):

        # 併入control run庫存
        if AREA != 'F8':
            C_qty = cal_whs_cust_qty(today_whs_controlrun)
            C_qty = C_qty.rename(columns={'CUST_QTY': 'CONTROLRUN_QTY', 'CUST_QTY_ODC': 'CONTROLRUN_QTY_ODC'})
            real_time_achieve = real_time_achieve.merge(C_qty, on=['CUST_KP_NO'], how='left')
            # C_qty = today_whs_controlrun.groupby(['CUST_KP_NO', 'PRINT_WO'])['CUST_KP_NO',
            #                                                                  'QTY', 'old_date_code', 'DATA2'].apply(lambda x: cal_whs_cust_qty_hwd(x))
            # C_qty = C_qty.drop(columns=['CUST_KP_NO']).reset_index()
            # C_qty = C_qty.drop(columns=['level_2'])
            # C_qty = C_qty.rename(columns={'CUST_QTY': 'CONTROLRUN_QTY', 'CUST_QTY_ODC': 'CONTROLRUN_QTY_ODC'})
            # real_time_achieve = real_time_achieve.merge(C_qty, on=['CUST_KP_NO', 'PRINT_WO'], how='left')

        else:
            C_qty = today_whs_controlrun.groupby(['CUST_KP_NO', 'PRINT_WO'])['CUST_KP_NO',
                                                                             'QTY', 'old_date_code', 'DATA2'].apply(lambda x: cal_whs_cust_qty_hwd(x))
            C_qty = C_qty.drop(columns=['CUST_KP_NO']).reset_index()
            C_qty = C_qty.drop(columns=['level_2'])
            C_qty = C_qty.rename(columns={'CUST_QTY': 'CONTROLRUN_QTY', 'CUST_QTY_ODC': 'CONTROLRUN_QTY_ODC'})
            real_time_achieve = real_time_achieve.merge(C_qty, on=['CUST_KP_NO', 'PRINT_WO'], how='left')

        real_time_achieve['CONTROLRUN_QTY'] = real_time_achieve['CONTROLRUN_QTY'].fillna(0)
    else:
        real_time_achieve['CONTROLRUN_QTY'] = 0
        real_time_achieve['CONTROLRUN_QTY_ODC'] = 0

    # 計算 deviation
    today_whs_deviation = today_whs[(today_whs['PRINT_TYPE'] == '3')].reset_index(drop=True)

    if len(today_whs_deviation) > 0:

        # 併入deviation庫存
        if AREA != 'F8':
            # D_qty = today_whs_deviation.groupby(['CUST_KP_NO', 'PRINT_WO'])['CUST_KP_NO',
            #                                                                 'QTY', 'old_date_code', 'DATA2'].apply(lambda x: cal_whs_cust_qty_hwd(x))
            # D_qty = D_qty.drop(columns=['CUST_KP_NO']).reset_index()
            # D_qty = D_qty.drop(columns=['level_2'])
            # D_qty = D_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            # real_time_achieve = real_time_achieve.merge(D_qty, on=['CUST_KP_NO', 'PRINT_WO'], how='left')
            D_qty = cal_whs_cust_qty(today_whs_deviation)
            D_qty = D_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            real_time_achieve = real_time_achieve.merge(D_qty, on=['CUST_KP_NO'], how='left')
        else:
            D_qty = today_whs_deviation.groupby(['CUST_KP_NO', 'PRINT_WO'])['CUST_KP_NO',
                                                                            'QTY', 'old_date_code', 'DATA2'].apply(lambda x: cal_whs_cust_qty_hwd(x))
            D_qty = D_qty.drop(columns=['CUST_KP_NO']).reset_index()
            D_qty = D_qty.drop(columns=['level_2'])
            D_qty = D_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            real_time_achieve = real_time_achieve.merge(D_qty, on=['CUST_KP_NO', 'PRINT_WO'], how='left')

        real_time_achieve['DEVIATION_QTY'] = real_time_achieve['DEVIATION_QTY'].fillna(0)
    else:
        real_time_achieve['DEVIATION_QTY'] = 0
        real_time_achieve['DEVIATION_QTY_ODC'] = 0

    # 將特殊工單的庫存數量 改設在CUST_QTY CUST_QTY_ODC
    controlrun_mask = real_time_achieve['SPECIAL_CASE'] == 1
    real_time_achieve.loc[controlrun_mask, 'CUST_QTY'] = real_time_achieve.loc[controlrun_mask, 'CONTROLRUN_QTY']
    real_time_achieve.loc[controlrun_mask, 'CUST_QTY_ODC'] = real_time_achieve.loc[controlrun_mask, 'CONTROLRUN_QTY_ODC']
    deviation_mask = real_time_achieve['SPECIAL_CASE'] == 2
    real_time_achieve.loc[deviation_mask, 'CUST_QTY'] = real_time_achieve.loc[deviation_mask, 'DEVIATION_QTY']
    real_time_achieve.loc[deviation_mask, 'CUST_QTY_ODC'] = real_time_achieve.loc[deviation_mask, 'DEVIATION_QTY_ODC']
    real_time_achieve = real_time_achieve.drop(columns=['CONTROLRUN_QTY_ODC', 'DEVIATION_QTY_ODC'])
    real_time_achieve = real_time_achieve.rename(columns={'DEVIATION_QTY': 'DEVIATION',
                                                          'CONTROLRUN_QTY': 'CONTROL_RUN', 'CUST_QTY_ODC': 'OLD_DATE_CODE', 'CUST_QTY': 'Real_QTY'})
    if AREA == 'F8':
        real_time_achieve = real_time_achieve.dropna()

    return real_time_achieve, empNO_table


# 計算現在備料狀態、預判庫存、原始庫存、準時超時
def cust_achieve_state(ALLPARTS_DB, real_time_achieve, db_realtime, range_time, db_errortrack, FLAG, AREA):

    # 此料尚缺多少沒備
    real_time_achieve['SHORTAGE_QTY'] = real_time_achieve['WO_REQUEST'] - real_time_achieve['DELIVER_QTY']
    real_time_achieve.loc[real_time_achieve['SHORTAGE_QTY'] < 0, 'SHORTAGE_QTY'] = 0

    # 計算 備料是否滿足request 備完了沒
    real_time_achieve['CUST_FINISH'] = real_time_achieve['WO_REQUEST'] <= real_time_achieve['DELIVER_QTY']  # True:備完  False: 還沒備完
    real_time_achieve['WO'] = real_time_achieve['WO'].astype(str)

    # 預估停線計算
    unfin_list = pd.DataFrame(real_time_achieve[real_time_achieve['CUST_FINISH'] == 0], columns=['V_WO', 'CUST_KP_NO'])

    if len(unfin_list) > 0:
        unfin_table = unfin_list.merge(real_time_achieve, on=['V_WO', 'CUST_KP_NO'], how='left').drop_duplicates()
        stop_point, predict_cust_std_table = predict_realtime_unfin_stopline(ALLPARTS_DB, unfin_table, AREA)

        # 輸出 停線時間清單 與計算用的標準用量表
        stop_point = stop_point.drop(columns=['START_TIME', 'END_TIME', 'DELIVER_QTY'])

        real_time_achieve = real_time_achieve.merge(stop_point, on=['WO', 'CUST_KP_NO', 'WO_REQUEST'], how='left')
        real_time_achieve.loc[real_time_achieve['CUST_FINISH'] == 1,
                              'STOP_LINE_POINT'] = real_time_achieve.loc[real_time_achieve['CUST_FINISH'] == 1, 'END_TIME']
    else:
        predict_cust_std_table = []
        real_time_achieve['STOP_LINE_POINT'] = real_time_achieve['START_TIME']
        real_time_achieve.loc[real_time_achieve['CUST_FINISH'] == 1,
                              'STOP_LINE_POINT'] = real_time_achieve.loc[real_time_achieve['CUST_FINISH'] == 1, 'END_TIME']

    # 原始庫存 存放欄位
    real_time_achieve['Real_Ori_QTY'] = real_time_achieve['Real_QTY']

    # 同料號需求累加
    mask = real_time_achieve['SPECIAL_CASE'] == 0
    real_time_achieve.loc[mask, 'cumsum'] = real_time_achieve.loc[mask, :].groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()

    mask = real_time_achieve['SPECIAL_CASE'] == 1
    real_time_achieve.loc[mask, 'cumsum'] = real_time_achieve.loc[mask, :].groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()

    mask = real_time_achieve['SPECIAL_CASE'] == 2
    real_time_achieve.loc[mask, 'cumsum'] = real_time_achieve.loc[mask, :].groupby(['CUST_KP_NO', 'PRINT_WO'])[
        'SHORTAGE_QTY'].cumsum()

    # 特殊工單造成的空值要補0
    real_time_achieve['cumsum'] = real_time_achieve['cumsum'].fillna(0)

    # 將累加需求量 扣去實際庫存  預測未來庫存量
    real_time_achieve['Real_QTY'] = real_time_achieve['Real_QTY'] - real_time_achieve['cumsum']

    # 物料對照準備時間表
    BU = 'HWD' if 'HWD' in ALLPARTS_DB else 'NSD'
    part_prepare_time = cust_prepare_time_table(FLAG, BU)

    ## 狀態判斷
    # 庫存夠不夠  'Real_QTY'為預判庫存，Real_QTY>0 表示以預設發給該工單的料後還有剩餘數量
    real_time_achieve['CUST_ENOUGH'] = real_time_achieve['Real_QTY'] >= 0

    # 依據料別 判別該提前的準備時間
    real_time_achieve = real_time_achieve.merge(part_prepare_time , left_on='PART_TYPE' , right_on='CUST_TYPE', how='left')
    real_time_achieve.loc[:, 'timedelta'] = real_time_achieve['PREP_TIME'].apply(lambda x : timedelta(hours=x))

    # 計算 結算時間點
    real_time_achieve['ON_TIME_POINT'] = real_time_achieve['START_TIME'] - real_time_achieve['timedelta']

    # 搜尋上一次的on time 資訊
    last_time_achieve = realtime_packageout(db_realtime, 1)
    real_time_achieve = real_time_achieve.merge(last_time_achieve, on=['WO', 'CUST_KP_NO'], how='left')
    real_time_achieve['M_ABNORMAL_MAINTAIN'] = real_time_achieve['M_ABNORMAL_MAINTAIN'].fillna('')
    mask = (real_time_achieve['M_ABNORMAL_MAINTAIN'].str.contains('CHIP')
            | real_time_achieve['M_ABNORMAL_MAINTAIN'].str.contains('PC'))
    real_time_achieve.loc[~mask, 'M_ABNORMAL_MAINTAIN'] = ''
    real_time_achieve['ON_TIME'] = real_time_achieve['ON_TIME'].fillna(2)
    real_time_achieve.loc[real_time_achieve['ON_TIME_POINT'] > pd.Timestamp.now(), 'ON_TIME'] = 2  # 校正用

    # 已過判定時間且又on_time==2 者 去errortrack 抓 on time 訊息
    mask = (real_time_achieve['ON_TIME'] == 2) & (real_time_achieve['ON_TIME_POINT']
                                                  < (pd.Timestamp.now() - timedelta(minutes=range_time)))
    wo_list = real_time_achieve.loc[mask, 'WO'].drop_duplicates()
    wo_list_str = ("'" + "','".join(wo_list) + "'")
    ET_table = error_track(db_errortrack, wo_list_str).drop_duplicates()
    real_time_achieve = real_time_achieve.merge(ET_table, on=['WO', 'CUST_KP_NO'], how='left')
    real_time_achieve.loc[mask, 'ON_TIME'] = real_time_achieve.loc[mask, 'early_on_time']
    real_time_achieve = real_time_achieve.drop(columns=['early_on_time'])

    # 判定時間區間
    judge_achieve = abs(real_time_achieve['ON_TIME_POINT'] - pd.Timestamp.now()
                        ) < timedelta(minutes=range_time)  # pd.Timestamp.now()('2020-06-26 12:45:10') #須改即時
    real_time_achieve['CUST_STATE'] = real_time_achieve.CUST_FINISH * (2**1) + real_time_achieve.CUST_ENOUGH * (2**0)
    temp = real_time_achieve[judge_achieve]
    real_time_achieve.loc[temp[real_time_achieve.loc[judge_achieve, 'CUST_STATE'] != 1].index, 'ON_TIME'] = 1  # 按時
    real_time_achieve.loc[temp[real_time_achieve.loc[judge_achieve, 'CUST_STATE'] == 1].index, 'ON_TIME'] = 0  # 超時

    # 若on time  = nan ，判定為急插插工單(只能首次5分鐘!??)
    mask = real_time_achieve['ON_TIME'].isna()
    real_time_achieve.loc[mask, 'ON_TIME'] = 1
    real_time_achieve.loc[mask, 'M_ABNORMAL_MAINTAIN'] = 'system: 急插工單請盡快被料，判定準時'

    # find_predict_cust 找料建議欄位
    real_time_achieve['ERROR_TRACK'] = 9

    return real_time_achieve, predict_cust_std_table


# 合併作業員維護訊息、物料管理人訊息、(找料訊息)
def generate_realtime_table(ALLPARTS_DB, real_time_achieve, FLAG, AREA, part_config_empNO):

    # 綁定工單 列表
    vo_wo = v_wo_table(ALLPARTS_DB)

    # 結合工號 物料負責人工號
    real_time_achieve = real_time_achieve.merge(part_config_empNO, on='CUST_KP_NO', how='left')

    # 重新命名
    real_time_achieve = real_time_achieve.rename(
        columns={'WO_REQUEST': 'WO_QTY', 'Real_QTY': 'STOCK_QTY' , 'PRINT_WO': 'SPECIAL_WO', 'EMP_NO': 'CUST_EMP_NO', 'DELIVER_QTY': 'DELIEVER_QTY'})
    real_time_achieve = real_time_achieve.drop(columns=['PREP_TIME', 'timedelta', 'PART_TYPE', 'cumsum'])

    # 結合PDA 維護訊息
    if ((AREA == 'E') | (AREA == 'D')):
        pda_config = pda_mantain_table(ALLPARTS_DB, AREA).dropna()
        vo_pda = vo_wo[vo_wo.T_WO.isin(pda_config.WO)].rename(columns={'T_WO': 'WO'})
        pda_config = pda_config.merge(vo_pda, on='WO', how='left')
        pda_config.loc[pda_config.V_WO.isna(), 'V_WO'] = pda_config.loc[pda_config.V_WO.isna(), 'WO']
        pda_config = pda_config.drop(columns=['REQUEST_QTY', 'DELIVER_QTY', 'SHORT_QTY', 'INVENTORY_QTY', 'SEQ_NO', 'WO'])
        pda_config = pda_config.drop_duplicates()
        real_time_achieve = real_time_achieve.merge(pda_config, on=['V_WO', 'CUST_KP_NO'], how='left')
        real_time_achieve['TYPE_DESCRIBE'] = real_time_achieve['TYPE_DESCRIBE'].fillna('')
        mask = (real_time_achieve['M_ABNORMAL_MAINTAIN'] == '')
        real_time_achieve.loc[mask, 'M_ABNORMAL_MAINTAIN'] = real_time_achieve.loc[mask, 'TYPE_DESCRIBE']
        real_time_achieve.loc[((real_time_achieve['ON_TIME'] == 0) & (
            real_time_achieve['M_ABNORMAL_MAINTAIN'].str.len() != 0)), 'ON_TIME'] = 1
        real_time_achieve = real_time_achieve.drop(columns=['TYPE_DESCRIBE'])
        mask = real_time_achieve['M_ABNORMAL_MAINTAIN'].str.contains('CHIP') & ~real_time_achieve['CUST_FINISH']
        real_time_achieve.loc[mask, 'DELIEVER_QTY'] = real_time_achieve.loc[mask, 'WO_QTY']
        real_time_achieve.loc[mask, 'CUST_FINISH'] = True
        mask = real_time_achieve['M_ABNORMAL_MAINTAIN'].str.contains('PC') & ~real_time_achieve['CUST_FINISH']
        real_time_achieve.loc[mask, 'DELIEVER_QTY'] = real_time_achieve.loc[mask, 'WO_QTY']
        real_time_achieve.loc[mask, 'CUST_FINISH'] = True

    # 把當日已下線工單變為完成
    mask = (real_time_achieve.END_TIME < pd.Timestamp.now()) & (real_time_achieve['CUST_FINISH'] == False)
    real_time_achieve.loc[mask, 'M_ABNORMAL_MAINTAIN'] = 'system: 已下線工單判定完成'
    real_time_achieve.loc[mask, 'CUST_FINISH'] = True

    real_time_achieve['BOARD_TYPE'] = FLAG

    return real_time_achieve


# 找料編碼
def encode(df):
    df = df.reset_index(drop=True)
    ans = 0

    for i in range(len(df)):
        ans += df.loc[i, 'FROM_SOURCE'] * 10**(i)

    return ans


# 找料資訊合併
def find_cust_state(rt_table, db_predict):

    find_cust_table = predict_find_cust(db_predict)
    find_cust_encode = find_cust_table.groupby(['WO', 'CUST_KP_NO']).apply(
        lambda x: encode(x)).reset_index().rename(columns={0: 'encode'})
    rt_table['ERROR_TRACK'] = 9
    rt_table = rt_table.merge(find_cust_encode, on=['WO', 'CUST_KP_NO'], how='left')
    rt_table.loc[~rt_table['encode'].isna(), 'ERROR_TRACK'] = rt_table.loc[~rt_table['encode'].isna(), 'encode']
    rt_table = rt_table.drop(columns=['encode'])

    return rt_table
