# -*- coding: utf-8 -*-  
import os
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from stock.WHS.dbio import STOCKMySQLIO
from stock.DB.oracle import BasicORACLE
from stock.config import *
import time

np.seterr(divide='ignore', invalid='ignore')  # RuntimeWarning: invalid value encountered in true_divide
pd.set_option('mode.chained_assignment', None)  # SettingWithCopyWarning



# 選擇Allparts DB
def chose_allpart_db(db, sql):
    
    AllPart_DB_DE = BasicORACLE(username=Allpart_DE['username'], password=Allpart_DE['password'],
                    hostname=Allpart_DE['hostname'], port=Allpart_DE['port'], db_name=Allpart_DE['db_name'])

    AllPart_DB_F = BasicORACLE(username=Allpart_F['username'], password=Allpart_F['password'],
                   hostname=Allpart_F['hostname'], port=Allpart_F['port'], db_name=Allpart_F['db_name'])
    AllPart_DB_HWD = BasicORACLE(username=Allpart_HWD['username'], password=Allpart_HWD['password'],
                    hostname=Allpart_HWD['hostname'], port=Allpart_HWD['port'], db_name=Allpart_HWD['db_name'])

    AllPart_DB_HWD_white = BasicORACLE(username=Allpart_HWD_white['username'], password=Allpart_HWD_white['password'],
                    hostname=Allpart_HWD_white['hostname'], port=Allpart_HWD_white['port'], db_name=Allpart_HWD_white['db_name'])

    if db == 'F':
        return AllPart_DB_F.manipulate_db(sql, dtype='DataFrame')
    elif db == 'HWD':
        return AllPart_DB_HWD.manipulate_db(sql, dtype='DataFrame')
    elif db == 'HWD_white':
        return AllPart_DB_HWD_white.manipulate_db(sql, dtype='DataFrame')
    else: # D、E area allparts
        return AllPart_DB_DE.manipulate_db(sql, dtype='DataFrame')


# 載入歷史備料資訊
def history_packageout(DB):
    
    db = STOCKMySQLIO()
    history_sql = f'''
    SELECT
        WO, CUST_KP_NO, CUST_TYPE, WO_QTY, Real_Ori_QTY, DELIEVER_QTY, START_TIME, END_TIME, STOCK_QTY, 
        OLD_DATE_CODE, CONTROL_RUN, DEVIATION, SHORTAGE_QTY, CUST_FINISH, SPECIAL_CASE, CUST_EMP_NO, Real_Ori_QTY 
    FROM
        stock_db.{DB}
    WHERE
        `END_TIME` BETWEEN (CURDATE() - INTERVAL 30 DAY) 
        AND CURDATE()
        AND (DELIEVER_QTY > WO_QTY)
    '''
                     
    return db.return_df(history_sql)


# 載入Realtime Packageout
def realtime_packageout(DB):
    
    db = STOCKMySQLIO()
    real_time_sql = f'''
    SELECT
        WO, V_WO, CUST_KP_NO, WO_QTY, DELIEVER_QTY, STOP_LINE_POINT,
        Real_Ori_QTY, SHORTAGE_QTY, CUST_FINISH, SPECIAL_CASE
    FROM
        stock_db.{DB}'''
    
    return db.return_df(real_time_sql)


# 載入APS 工單
def aps_schedule(area):
    
    db = STOCKMySQLIO()
    aps_schedule_sql = f'''
    SELECT
        WO, START_TIME, END_TIME, LINE, SECTION, V_WO
    FROM
        stock_db.latest_schedule_result
    WHERE
        LINE LIKE '{area}%'
    '''
    aps_wo = db.return_df(aps_schedule_sql)
    aps_wo2 = aps_wo.groupby(['WO'])['END_TIME','LINE'].max().reset_index().rename(columns={'WO':'FROM_LOCATION'})
   
    return aps_wo, aps_wo2


# 載入綁定工單
def v_wo_table(allparts_db):
    
    vo_wo_sql = '''
    SELECT
        V_WO, SEQ_NO, T_WO 
    FROM
        MES4.R_V_WO 
    WHERE
        EDIT_TIME> to_date('%s','yyyy-MM-dd  HH24:MI:SS') 
    ''' % (str((pd.Timestamp.now() - timedelta(weeks=40)).floor('D')))
    
    return chose_allpart_db(allparts_db, vo_wo_sql) 


# 整機台下線資訊
def real_startime_wo(allparts_db):
    
    sql = '''
    SELECT
        WO, MIN(START_TIME) START_TIME
    FROM
        MES4.R_CHANGE_WO_TIME 
    WHERE
        START_TIME> to_date('%s','yyyy-MM-dd  HH24:MI:SS') 
    GROUP BY
        WO ,START_TIME 
    '''%(str((pd.Timestamp.now() - timedelta(weeks=6)).floor('D')))
    
    return chose_allpart_db(allparts_db, sql)


# 載入IQC資料
def iqc_table(allparts_db, area, cust_list):
    iqc_sql = ''' select a.floor,B.CUST_KP_NO,b.RECEIVE_QTY,b.ACCEPT_QTY,b.REJECT_QTY,a.GRN_EVENT 
                  from mes4.r_grn a,mes4.r_whs b 
                  where a.doc_no=b.doc_no 
                  and GRN_EVENT in('WHS_RECEIVE','IQC_LABEL','IQC_INSPECT') 
                  and a.DOWNLOAD_TIME>sysdate-365 
                  and a.DOC_NO not like ('S%')
                  and GRN_FLAG<>'5' 
                  and (a.floor like ('{}%')) '''.format(area)
    iqc_stock = chose_allpart_db (allparts_db , iqc_sql) 
    iqc_stock = iqc_stock.rename(columns={'KP_NO':'CUST_KP_NO','REJECT_QTY':'IQC_REJECT_QTY'})
    iqc_stock['IQC_CAN_USE'] = iqc_stock['RECEIVE_QTY'] - iqc_stock['ACCEPT_QTY'] - iqc_stock['IQC_REJECT_QTY']
    iqc_stock1 = iqc_stock[iqc_stock['CUST_KP_NO'].isin(cust_list)]#track_cust_list
    iqc_stock1_sum = iqc_stock1.groupby(['CUST_KP_NO'])['IQC_CAN_USE','IQC_REJECT_QTY'].sum().reset_index()
    
    return iqc_stock1_sum


# 輸入 col_name(欄位名稱) 及 lst(工單號or 料號list)
# 生成SQL篩選條件 ex : and (WO in ('a', 'b', 'c'))
def wo_lst2sql(col_name, lst):
    times = len(lst) // 1000
    if len(lst) != 0:
        if len(lst) % 1000 != 0:
            times += 1
        sql = 'and ('
        for i in range(times):
            index = 1000 * i
            sql += col_name + ' in (%(trsn_sql)s) or ' % ({'trsn_sql': str(lst[index: index + 1000])[1:-1]})
        sql = sql[:-3]
        sql += ')'
    else:
        sql = "and "+ col_name + " in ('')"
    return sql


# 載入周轉最後紀錄為到產線之物料
def from_wo_2_line_table(allparts_db, area, cust_list):
    # 篩選 R_TR_SN LOCATION_FLAG=2：在產線
    # 篩選 R_WASTAGE_DOC STATUS != 'CLOSE' (超領單關結/該工單旗下子物料若在產線視為耗損) 關結工單
    # 以 V_WO
    wo_2_line_sql = '''
    WITH CTE AS
    (
        select distinct
            a.tr_sn, a.FROM_LOCATION, 
            ( CASE
                WHEN v.V_WO is null THEN a.FROM_LOCATION
                ELSE v.V_WO
            END ) as V_WO,
            a.cust_kp_no, a.qty,a.MOVE_TIME, b.EXT_QTY, b.LOCATION_FLAG, b.WORK_FLAG
        from
            MES4.R_KITTING_SCAN_DETAIL a,
            MES4.R_TR_SN b,
            MES4.R_V_WO v
        where
            a.TR_SN = b.TR_SN
            and a.FROM_LOCATION = v.T_WO(+)
            and a.MOVE_TIME = (select max(MOVE_TIME) from MES4.R_KITTING_SCAN_DETAIL c where a.tr_sn=c.tr_sn )
            and MOVE_TYPE='c'
            and a.MOVE_TIME> to_date('%s','yyyy-MM-dd  HH24:MI:SS')
            and b.LOCATION_FLAG = '2'
            and b.EXT_QTY > 0
            and b.FLOOR = '%s'
    ),
     WASTAGE_CTE AS
    (
    SELECT DISTINCT WO, STATUS, CREATE_TIME FROM MES4.R_WASTAGE_DOC w
    where 
    W.CREATE_TIME = (select max(CREATE_TIME) from MES4.R_WASTAGE_DOC c where w.WO=c.WO )   
    )
    SELECT DISTINCT CTE.tr_sn, CTE.FROM_LOCATION, CTE.V_WO,
        CTE.cust_kp_no, CTE.qty, CTE.MOVE_TIME, CTE.EXT_QTY, CTE.LOCATION_FLAG, CTE.WORK_FLAG
    FROM 
        CTE left join WASTAGE_CTE w_cte on (CTE.V_WO = w_cte.WO)
    WHERE
        w_cte.STATUS not in ('CLOSE', 'LINE_CONFIRM', 'MANAGER_CONFIRM', 'KITTING_CONFIRM') or w_cte.STATUS is null 
    ''' % (str((pd.Timestamp.now() - timedelta(weeks=12)).floor('D')), area)
    cust_sql = wo_lst2sql(col_name='CTE.cust_kp_no', lst=cust_list)
    wo_2_line_sql += cust_sql
    from_wo_2_line_table = chose_allpart_db (allparts_db , wo_2_line_sql)

    return from_wo_2_line_table


# 以超發WO 找 V_WO 找VWO下所有子工單
def find_all_WO_REQUEST(allparts_db, wo_list):
    wo_sql = wo_lst2sql(col_name='T_WO', lst=wo_list)

    wo_r_sql = '''
    WITH CTE AS (
    SELECT V_WO FROM MES4.R_V_WO
    WHERE 1= 1 
    %(wo_sql)s
    )
    SELECT a.V_WO, a.T_WO as WO
    FROM CTE, MES4.R_V_WO a
    WHERE CTE.V_WO = a.V_WO
    ''' % ({'wo_sql' : wo_sql})

    V_WO = chose_allpart_db (allparts_db , wo_r_sql)
    # 找子工單需求
    sql = '''SELECT WO, CUST_KP_NO, WO_REQUEST, DELIVER_QTY FROM MES4.R_WO_REQUEST WHERE 1 = 1 '''
    all_wo_list = list(set(V_WO.WO.unique()) | set(wo_list))
    test_sql = wo_lst2sql(col_name='WO', lst=all_wo_list)
    sql += test_sql
    all_request = chose_allpart_db (allparts_db , sql)

        # 以WO合併 V_WO 資訊
    all_VWO_request = all_request.merge(V_WO, on='WO', how='left').drop_duplicates()
    all_VWO_request.loc[:, 'V_WO'] = all_VWO_request.V_WO.fillna(all_VWO_request.WO)
    # 找綁訂工單中料號 發料最多之工單
    max_wo = all_VWO_request.groupby(['V_WO', 'CUST_KP_NO']).max().reset_index()
    # 計算綁定工單各料號總需求
    V_WO_REQUEST = all_VWO_request.groupby(['V_WO', 'CUST_KP_NO']).sum().reset_index().rename(columns={'WO_REQUEST':'VWO_REQUEST',
                                                                                                    'DELIVER_QTY':'VWO_DELIVER_QTY' })
    # 合併工單、 V工單資訊 / 篩選超發工單 / 計算超發數量
    V_WO_REQUEST = V_WO_REQUEST.merge(max_wo, how='left', on=['V_WO', 'CUST_KP_NO'])

    return V_WO_REQUEST


#未modify on line 設 LINE_STATE = 3
def unmodify_table(cust_list, aps_wo2, allparts_db, area, WO_HEAD):
    # cust_array = [cust_list[i:i+1000] for i in range(0,len(cust_list),1000)]
    if (area=='F'):
        unmodify_order = '''SELECT a.tr_sn, a.wo, a.kp_no, a.EXT_QTY, a.STATION, b.OVER_TIME
                            FROM   mes4.r_TR_SN_WIP A, MES4.R_TR_SN B  
                            WHERE  A.TR_SN = B.TR_SN
                            AND    B.WORK_FLAG = '0' 
                            AND    B.LOCATION_FLAG = '2' 
                            AND    wo <> 'Repair'
                            AND    b.FLOOR='%s' 
                            AND    (a.EXT_QTY > 0) 
                            ''' %(area)
    else:
        unmodify_order = '''SELECT a.tr_sn, a.wo, a.kp_no, a.EXT_QTY, a.STATION, b.OVER_TIME
                            FROM   mes4.r_TR_SN_WIP A, MES4.R_TR_SN B  
                            WHERE  A.TR_SN = B.TR_SN
                            AND    B.WORK_FLAG = '0' 
                            AND    B.LOCATION_FLAG = '2' 
                            AND    wo <> 'Repair'
                            AND    a.wo NOT IN (SELECT DISTINCT b.T_WO FROM mes4.r_wastage_doc a, mes4.r_v_wo b 
                                                WHERE (a.wo = B.T_WO OR a.wo = b.v_wo))
                            AND    a.wo NOT IN (SELECT DISTINCT wo FROM mes4.r_wastage_doc)
                            AND    b.FLOOR='%s' 
                            AND    (a.EXT_QTY > 0) 
                            ''' %(area)
    wo_sql = wo_lst2sql(col_name='a.kp_no', lst=list(cust_list.unique()))
    unmodify_order += wo_sql
    
    unmodify_stock = chose_allpart_db (allparts_db , unmodify_order) 
    unmodify_stock = unmodify_stock.rename(columns={'KP_NO':'CUST_KP_NO','WO':'FROM_LOCATION','EXT_QTY':'QTY'})
    
    if len(unmodify_stock)>0:
        # unmodify_stock會漏掉少部分 加上 from_wo_2_line_table 取聯集
        if area=='F':
            wo_list = list(unmodify_stock.FROM_LOCATION.unique())
        else:
            from_wo_2_line = from_wo_2_line_table(allparts_db, area, cust_list=list(cust_list.unique()))
            wo_list = list(set(from_wo_2_line.FROM_LOCATION.unique()) | set(unmodify_stock.FROM_LOCATION.unique()))

        V_WO_REQUEST = find_all_WO_REQUEST(allparts_db, wo_list)
        V_WO_REQUEST = V_WO_REQUEST[V_WO_REQUEST.CUST_KP_NO.isin(cust_list.unique())]
        V_WO_REQUEST = V_WO_REQUEST[V_WO_REQUEST.VWO_DELIVER_QTY > V_WO_REQUEST.VWO_REQUEST]
        V_WO_REQUEST.loc[:, 'QTY'] = V_WO_REQUEST.VWO_DELIVER_QTY - V_WO_REQUEST.VWO_REQUEST
        
        unmodify_stock = V_WO_REQUEST.rename(columns={'WO': 'FROM_LOCATION'})

        unmodify_stock = unmodify_stock.merge(aps_wo2, on = ['FROM_LOCATION'], how = 'left')
        unmodify_stock.columns
        unmodify_stock.loc[:, 'TR_SN'] = 'X00000000000'
        unmodify_stock.loc[:, 'OVER_TIME'] = '20990101'
        unmodify_stock['LINE_STATE'] = 3

        use_col = ['TR_SN', 'FROM_LOCATION', 'CUST_KP_NO', 'QTY', 'OVER_TIME',
       'LINE_STATE', 'END_TIME', 'LINE']

        unmodify_stock = unmodify_stock[use_col].dropna(subset=['LINE'])
    else:
        unmodify_stock = pd.DataFrame(columns = ['TR_SN', 'FROM_LOCATION', 'CUST_KP_NO', 'QTY', 'OVER_TIME',
       'LINE_STATE', 'END_TIME', 'LINE'])
    return unmodify_stock


# 未 modify at kitting or stock   設 LINE_STATE = 2
def unmodify_kitting(cust_list, realtime_PO_table, unmodify_stock, aps_wo2, allparts_db):
    overfitting_table = realtime_PO_table[realtime_PO_table['DELIEVER_QTY']>realtime_PO_table['WO_QTY']]
    overfitting_table = overfitting_table.rename(columns={'WO_QTY':'REQUEST_QTY'})
    real_startime = real_startime_wo(allparts_db)
    overfitting_table = overfitting_table.merge(real_startime, on = 'WO', how = 'left')
    overfitting_table = overfitting_table.drop(overfitting_table[~overfitting_table['START_TIME'].isna()].index)

    confirmlist = pd.DataFrame(unmodify_stock.loc[:,('FROM_LOCATION','CUST_KP_NO','LINE_STATE')])
    confirmlist = confirmlist.rename(columns={'FROM_LOCATION':'WO'})
    overfitting_table = overfitting_table.merge(confirmlist, on = ['WO','CUST_KP_NO'], how = 'left')
    overfitting_table = overfitting_table[overfitting_table['CUST_KP_NO'].isin(cust_list)]
    overfitting_table = overfitting_table.drop(overfitting_table[~overfitting_table['LINE_STATE'].isna()].index)
    overfitting_table = overfitting_table.drop(columns=['START_TIME','LINE_STATE']).rename(columns={'WO':'FROM_LOCATION'})
    overfitting_table = overfitting_table.merge(aps_wo2, on = ['FROM_LOCATION'], how = 'left')
    overfitting_table['LINE_STATE'] = 2
    overfitting_table['TR_SN'] = 'R00000000000'
    overfitting_table['OVER_TIME'] = '20990101'
    overfitting_table['QTY'] = overfitting_table['DELIEVER_QTY'] - overfitting_table['REQUEST_QTY']
    overfitting_table = pd.DataFrame(overfitting_table.loc[:,('TR_SN', 'FROM_LOCATION', 'CUST_KP_NO', 'QTY', 'OVER_TIME',
       'LINE_STATE', 'END_TIME', 'LINE')])
    return overfitting_table

# Modify    設 LINE_STATE = 1
# move type = 'd' 產線->kitting (modify 正常退料流程) ;  move type = 'h' 工單->kitting (少數case)
def modify_table(vo_wo,aps_wo,aps_wo2,cust_list,allparts_db,area):
    modify_order = '''select distinct a.tr_sn,a.FROM_LOCATION,a.cust_kp_no,b.qty,b.OVER_TIME 
                      from MES4.R_KITTING_SCAN_DETAIL a,mes4.r_kitting_location b  
                      where a.tr_sn=b.tr_sn 
                      and a.MOVE_TIME = (select max(MOVE_TIME) from MES4.R_KITTING_SCAN_DETAIL c where a.tr_sn=c.tr_sn )   
                      and (MOVE_TYPE='d' or MOVE_TYPE='h')
                      and AREA = '%s'  '''%(area)
    modify_stock = chose_allpart_db (allparts_db , modify_order).rename(columns={'KP_NO':'CUST_KP_NO','QTY':'QTY'})
    modify_stock1 = modify_stock[modify_stock['CUST_KP_NO'].isin(cust_list)] #track_cust_list 格式要統一
    if len(modify_stock1)>0: 
        modify_stock1['LINE_STATE'] = 1
        modify_stock1 = modify_stock1.merge(aps_wo2, on = ['FROM_LOCATION'], how = 'left')
        # 找已被APS清除工單
        problem_wo =  modify_stock1[modify_stock1['LINE'].isna()]['FROM_LOCATION'].drop_duplicates().to_frame()
        problem_wo = problem_wo.merge(vo_wo, left_on=['FROM_LOCATION'], right_on=['T_WO'], how='left')
        problem_wo = problem_wo.merge(aps_wo, on=['V_WO'], how='left')
        problem_wo = problem_wo.groupby(['FROM_LOCATION'])['END_TIME','LINE'].max().reset_index().rename(columns={'END_TIME':'END_TIME_2','LINE':'LINE_2'})
        modify_stock1 = modify_stock1.merge(problem_wo, on = ['FROM_LOCATION'], how = 'left')
        modify_stock1.loc[modify_stock1['LINE'].isna(),'LINE'] = modify_stock1.loc[modify_stock1['LINE'].isna(),'LINE_2']
        modify_stock1.loc[modify_stock1['END_TIME'].isna(),'END_TIME'] = modify_stock1.loc[modify_stock1['END_TIME'].isna(),'END_TIME_2']
        modify_stock1 = modify_stock1.drop(columns=['END_TIME_2','LINE_2'])
    else:
        modify_stock1 = pd.DataFrame(columns = ['TR_SN', 'FROM_LOCATION', 'CUST_KP_NO', 'QTY', 'OVER_TIME',
       'LINE_STATE', 'END_TIME', 'LINE'])
    
    return modify_stock1


def split_and_combined0( ser):
    ser = str(ser)
    ''' 輸入用'/'切割的陣列, 再補回與合併原始供單號輸出  '''
    if '/' in ser:
        text = ser.split('/')
        # ''.join(text)
        return ''.join(text)
    elif '-' in ser:
        text = ser.split('-')
        # ''.join(text)
        return ''.join(text)
    else:
        return ser
    
    
def split_and_combined( ser):
    ''' 輸入用'/'切割的陣列, 再補回與合併原始供單號輸出  '''
    if ser[0]:
        ser[0] = '00' + ser[0]
        for i in range(1, len(ser)):
            ser[i] = ser[0][:-len(ser[i])]+ser[i]
        return '_'.join(ser)
    else:
        return ser[0]
    
    
# 倉庫庫存
def stock_table(allparts_db, AREA, cust_list):
    
    cust_array = [cust_list[i:i + 1000] for i in range(0, len(cust_list), 1000)]
    cust_str = "( A.CUST_KP_NO IN  " + "('" + "','".join(cust_array[0]) + "')"
    for j in range(len(cust_array) - 1):
        cust_str = cust_str + " or " + " A.CUST_KP_NO IN " + "('" + "','".join(cust_array[j + 1]) + "')"
    cust_str += ')'
    
    # T1 針對不同 print_type 取得 whs_LOCATION
    # 利用 T2 過濾掉 QUALIFY_STATUS 不合格與NO_AVL
    sql = f'''
    SELECT
    	(
    		  CASE
    			  WHEN T2.QUALIFY_STATUS IS NOT NULL
    			  THEN (
    					CASE
    						WHEN T2.QUALIFY_STATUS IN ( 'In Process - Qual', 'Cancelled', 'Hold-RoHS', 
    						'End of Production', 'Development', 'Disqualified' )
    						THEN 1
    						ELSE 0
    					END					
    				)
    			ELSE 1
    			END
    		) AS ABNORMAL,
    		T2.QUALIFY_STATUS,
        T1.CUST_KP_NO, T1.TR_SN, T1.PRINT_TYPE, T1.QTY, T1.DATA2, T1.PRINT_DATA, T1.START_TIME, T1.OVER_TIME, T1.LOCATION
    FROM
    (  /* T1 print_type in 0-3 with different processing */
        (  
            SELECT DISTINCT
                A.TR_SN, A.QTY, A.CUST_KP_NO, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME, A.LOCATION,
                (
                    CASE
                        WHEN B.DUE_DATE IS NOT NULL
                        THEN B.DUE_DATE
                        ELSE
                            (
                            CASE
                                WHEN A.OVER_TIME IS NOT NULL
                                THEN TO_DATE((REGEXP_REPLACE(A.OVER_TIME,'-|/','')),'yyyymmdd')
                                ELSE TO_DATE('2099-12-31 00:00:00','yyyy/mm/dd hh24:mi:ss')
                            END
                            )
                    END 
                ) AS OVER_TIME
            FROM
                MES4.R_WHS_LOCATION A
            LEFT JOIN
                MES1.C_WAIVE_KP_ALARM B 
            ON
                ( A.PRINT_DATA = B.DATA ) 
            WHERE
                A.print_type IN ( 1, 2 ) 
                AND A.area = '{AREA}' 
                AND ({cust_str}) 
        ) 
        UNION
        (
            SELECT DISTINCT
                A.TR_SN, A.QTY, A.CUST_KP_NO, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME, A.LOCATION,
                ( 
                    CASE
                        WHEN B.INVALID_DATE IS NOT NULL
                        THEN B.INVALID_DATE
                        ELSE(
                            CASE
                                WHEN A.OVER_TIME IS NOT NULL
                                THEN TO_DATE((REGEXP_REPLACE(A.OVER_TIME,'-|/','')),'yyyymmdd')
                                ELSE TO_DATE('2099-12-31 00:00:00','yyyy/mm/dd hh24:mi:ss')
                            END
                        )
                    END
                ) AS OVER_TIME
            FROM
                MES4.R_WHS_LOCATION A
            LEFT JOIN
                ( SELECT DEVIATION_NO, CUST_KP_NO, max( INVALID_DATE ) INVALID_DATE FROM MES1.C_DEVIATION_CONFIG GROUP BY DEVIATION_NO, CUST_KP_NO ) B
            ON
                ( A.CUST_KP_NO = B.CUST_KP_NO AND A.PRINT_DATA = B.DEVIATION_NO ) 
            WHERE
                A.print_type IN ( 3 ) 
                AND A.area = '{AREA}'
                AND ({cust_str}) 
        )
        UNION
        (
            SELECT
                A.TR_SN, A.QTY, A.CUST_KP_NO, A.PRINT_TYPE, A.DATA2, A.PRINT_DATA, A.START_TIME, A.LOCATION,
                (
                    CASE
                        WHEN A.OVER_TIME IS NOT NULL
                        THEN TO_DATE((REGEXP_REPLACE(A.OVER_TIME,'-|/','')),'yyyymmdd')
                        ELSE TO_DATE('2099-12-31 00:00:00','yyyy/mm/dd hh24:mi:ss')
                    END
                ) AS OVER_TIME
            FROM
                MES4.R_WHS_LOCATION  A
            WHERE
                A.print_type in (0) 
                AND A.area = '{AREA}'
                AND ({cust_str}) 
        )
    ) T1
    LEFT JOIN
    /* remove the tr_sn with QUALIFY_STATUS */
    (
        SELECT 
                A.CUST_KP_NO, A.QUALIFY_STATUS, C.TR_SN
        FROM
                MES1.C_ROHS_AGILE_KP A,
                MES4.r_tr_sn C,
                MES1.C_MFR_CONFIG D
        WHERE
                A.MFR_KP_NO = C.mfr_kp_no
                AND A.MFR_NAME = D.MFR_NAME
                AND C.MFR_CODE = D.MFR_CODE
                AND ({cust_str}) 
    ) T2
    ON
        (T1.TR_SN = T2.TR_SN AND T1.CUST_KP_NO = T2.CUST_KP_NO)
    '''
    
    whs_LOCATION = chose_allpart_db(allparts_db, sql)
    whs_LOCATION = whs_LOCATION.drop_duplicates()

        
    # 品質是否異常
    whs_LOCATION['ABNORMAL'] = whs_LOCATION.ABNORMAL.astype(bool)
    
    # 改成時間格式
    whs_LOCATION['DATA2'] = whs_LOCATION['START_TIME'].dt.floor('D')  # 進料時間
    
    # 判斷OLD DATE CODE 增加'old_date_code'欄位
    whs_LOCATION['old_date_code'] = whs_LOCATION['OVER_TIME'] < pd.Timestamp.now()
    
    # 物料分類 - 有無過期、有無異常、異常但有無PRINT_TYPE123
    # 過期物料清單
    expired_no_list = whs_LOCATION[whs_LOCATION['old_date_code'] == True].CUST_KP_NO.tolist()
    
    # 過期物料
    expired_df = whs_LOCATION[whs_LOCATION['CUST_KP_NO'].isin(expired_no_list)].reset_index(drop=True)
    expired_df['AVAILABLE'] = False
    
    # 沒過期物料
    unexpired_df = whs_LOCATION[~whs_LOCATION['CUST_KP_NO'].isin(expired_no_list)].reset_index(drop=True)
    
    # 沒過期也無品質異常
    qualifed_df = unexpired_df[unexpired_df['ABNORMAL'] == False].reset_index(drop=True)
    qualifed_df['AVAILABLE'] = True
    
    # 沒過期但是有品質異常
    unqualifed_df = unexpired_df[unexpired_df['ABNORMAL'] == True].reset_index(drop=True)
    
    # 沒過期、有品質異常、且PRINT_TYPE 123 (代表可用)
    print_type_ok_df = unqualifed_df[unqualifed_df['PRINT_TYPE'].isin(['1', '2', '3'])].reset_index(drop=True)
    print_type_ok_df['AVAILABLE'] = True
    
    # 沒過期、有品質異常、且PRINT_TYPE 非 123 (代表不可用)
    print_type_no_df = unqualifed_df[~unqualifed_df['PRINT_TYPE'].isin(['1', '2', '3'])].reset_index(drop=True)
    print_type_no_df['AVAILABLE'] = False
    
    # 用 print_type_no_df min DATA2 過濾 qualifed_df + print_type_ok_df
    time_limit = print_type_no_df.groupby(['CUST_KP_NO'])['DATA2'].min().reset_index()
    
    def data2_filter(row):
    
        # 相同料號 不可以用 最早時間
        if len(print_type_no_df):
            if row['DATA2'] > time_limit[time_limit['CUST_KP_NO'] == row['CUST_KP_NO']].DATA2.values[0]:
                row['AVAILABLE'] = False
    
        return row
    
    qualifed_df_1 = qualifed_df[qualifed_df['CUST_KP_NO'].isin(time_limit.CUST_KP_NO.to_list())].reset_index(drop=True)
    qualifed_df_1 = qualifed_df_1.apply(data2_filter, axis=1)
    qualifed_df_2 = qualifed_df[~qualifed_df['CUST_KP_NO'].isin(time_limit.CUST_KP_NO.to_list())].reset_index(drop=True)
    
    print_type_ok_df_1 = print_type_ok_df[print_type_ok_df['CUST_KP_NO'].isin(
        time_limit.CUST_KP_NO.to_list())].reset_index(drop=True)
    print_type_ok_df_1 = print_type_ok_df_1.apply(data2_filter, axis=1)
    print_type_ok_df_2 = print_type_ok_df[~print_type_ok_df['CUST_KP_NO'].isin(
        time_limit.CUST_KP_NO.to_list())].reset_index(drop=True)
    
    whs_LOCATION = pd.concat([expired_df, qualifed_df_1, qualifed_df_2, print_type_ok_df_1,
                             print_type_ok_df_2, print_type_no_df], axis=0)
    whs_LOCATION = whs_LOCATION.reset_index(drop=True)
    
    # 處理特殊工單資訊  將特殊工單合併
    whs_LOCATION.loc[whs_LOCATION['PRINT_DATA'] == 'N/A', 'PRINT_DATA'] = ''
    whs_LOCATION.loc[:, 'PRINT_DATA'] = whs_LOCATION['PRINT_DATA'].fillna('')
    whs_LOCATION.loc[:, 'PRINT_WO'] = whs_LOCATION.loc[:, 'PRINT_DATA']
    tempa = whs_LOCATION.loc[whs_LOCATION.PRINT_DATA.str.contains('/'), :]
    whs_LOCATION.loc[tempa.index, 'PRINT_WO'] = tempa['PRINT_DATA'].str.split('/').apply(lambda x: split_and_combined(x))
    whs_LOCATION.loc[whs_LOCATION.PRINT_WO.str.startswith(
        '2'), 'PRINT_WO'] = "00" + whs_LOCATION.PRINT_WO[whs_LOCATION.PRINT_WO.str.startswith('2')]
    return whs_LOCATION


# ROHS & NO_AVL 
def rohs_table(allparts_db, area, cust_list):
    rohs = ''' SELECT B.TR_SN, B.QTY, A.CUST_KP_NO, A.MFR_KP_NO, A.MFR_NAME,
                    A.QUALIFY_STATUS, B.OVER_TIME, B.AREA, A.UPDATE_TIME
            FROM MES1.C_ROHS_AGILE_KP A,
                    MES4.R_WHS_LOCATION B,
                    MES4.r_tr_sn C,
                    MES1.C_MFR_CONFIG D
            WHERE A.CUST_KP_NO = B.CUST_KP_NO
                    AND B.AREA ='%s'
                    AND B.PRINT_TYPE = 0
                    AND A.MFR_KP_NO = C.mfr_kp_no
                    AND A.MFR_NAME = D.MFR_NAME
                    AND B.TR_SN = C.TR_SN
                    AND C.MFR_CODE = D.MFR_CODE
                    AND A.QUALIFY_STATUS in ( 'In Process - Qual', 'Cancelled', 'Hold-RoHS', 'End of Production', 'Development', 'Disqualified' )
            '''%(area)
    whs_rohs_LOCATION = chose_allpart_db(allparts_db, rohs)
    whs_rohs_LOCATION = whs_rohs_LOCATION[whs_rohs_LOCATION['CUST_KP_NO'].isin(cust_list)]#track_cust_list
    whs_ipq = whs_rohs_LOCATION[whs_rohs_LOCATION['QUALIFY_STATUS']=='In Process - Qual'].rename(columns={'QTY':'IPQ_QTY'})
    whs_rohs = whs_rohs_LOCATION[whs_rohs_LOCATION['QUALIFY_STATUS']=='Hold-RoHS'].rename(columns={'QTY':'RoHS_QTY'})       
    whs_noavl = whs_rohs_LOCATION[~((whs_rohs_LOCATION['QUALIFY_STATUS']=='Hold-RoHS')|(whs_rohs_LOCATION['QUALIFY_STATUS']=='In Process - Qual'))].rename(columns={'QTY':'NOAVL_QTY'})
    return whs_ipq, whs_rohs, whs_noavl


def deviation_wo_table(DB):
    
    # Deviation 對應料號
    sql = '''
	SELECT
		t2.DEVIATION_NO, t2.CUST_KP_NO, t2.VALID_DATE, t2.INVALID_DATE, t2.EDIT_TIME
	FROM
		(
		SELECT
			DEVIATION_NO, CUST_KP_NO, MAX(EDIT_TIME) EDIT_TIME
		FROM
			MES1.C_DEVIATION_CONFIG
		WHERE
			VALID_DATE > to_date('{}','yyyy-MM-dd HH24:MI:SS')
		GROUP BY
			DEVIATION_NO, CUST_KP_NO
		) t1
	INNER JOIN
		MES1.C_DEVIATION_CONFIG t2
	ON
		t1.EDIT_TIME = t2.EDIT_TIME
	ORDER BY
		t2.DEVIATION_NO DESC, t2.CUST_KP_NO DESC, t2.VALID_DATE DESC
    '''.format(str((pd.Timestamp.now() - timedelta(weeks=35)).floor('D')))

    deviation_df = chose_allpart_db(DB, sql).dropna()
    deviation_df = deviation_df.groupby(['DEVIATION_NO', 'CUST_KP_NO'])['INVALID_DATE'].min().reset_index()
    deviation_df['DEV_EXPIRED'] = deviation_df['INVALID_DATE'] < pd.Timestamp.now()
    
    return deviation_df




class find_cust_data ():

    def __init__(self, realtime_db={}, history_db={}, area='D', allparts_db='D', WO_HEAD={}):
        
        # 參數設定
        self.realtime_db = realtime_db
        self.history_db = history_db
        self.area = area
        self.allparts_db = allparts_db
        self.WO_HEAD = WO_HEAD        
        
        # 資料載入
        self.history_PO_table = history_packageout(history_db) # 歷史備料資訊
        self.realtime_PO_table = realtime_packageout(realtime_db) # realtime packageout
        self.track_cust_table = self.realtime_PO_table[(self.realtime_PO_table['CUST_FINISH']==0)].drop(columns='CUST_FINISH').rename(columns={'WO_QTY':'REQUEST_QTY'})
        self.track_cust_list = list(self.track_cust_table['CUST_KP_NO'].drop_duplicates())
        self.cust_list = self.track_cust_table.CUST_KP_NO.drop_duplicates().reset_index().CUST_KP_NO
        self.whs_LOCATION = stock_table(allparts_db, area, self.cust_list)
        self.aps_wo, self.aps_wo2 = aps_schedule(area) # APS工單     
        self.v_wo = v_wo_table(allparts_db) # 綁定工單
        self.iqc_stock = iqc_table(allparts_db, area, self.cust_list) # IQC資料
        
    # 計算庫存數，考慮FIFO概念，輸出可用 & old date code
    def cal_whs_cust_qty(self, whs_stock):
    
        location_cust = whs_stock['CUST_KP_NO'].drop_duplicates().reset_index(drop=True).to_frame()
    
        # 分ODC
        cust_qty_df = whs_stock[whs_stock['old_date_code'] == False].reset_index(drop=True)
        cust_qty_odc_df = whs_stock[whs_stock['old_date_code'] == True].reset_index(drop=True)
    
        cust_qty_df = cust_qty_df.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY'})
        cust_qty_odc_df = cust_qty_odc_df.groupby(['CUST_KP_NO'])['QTY'].sum().reset_index().rename(columns={'QTY' : 'CUST_QTY_ODC'})
    
        # 結合表
        location_cust = location_cust.merge(cust_qty_df, on=['CUST_KP_NO'], how='left')
        location_cust = location_cust.merge(cust_qty_odc_df, on=['CUST_KP_NO'], how='left')
        location_cust = location_cust.fillna(0)
    
        return location_cust

    # 載入需追蹤工單
    def output_track_list(self):
        whs = pd.DataFrame(self.whs_LOCATION.loc[:, ('CUST_KP_NO' ,'LOCATION', 'PRINT_DATA')]).drop_duplicates(['CUST_KP_NO', 'PRINT_DATA'])
        aps = pd.DataFrame(self.aps_wo.loc[:,('WO','LINE')]).drop_duplicates('WO')
        aps.loc[:,'LINE'] = aps.loc[:,'LINE'].str[:4]
        cust_table = self.track_cust_table.copy()
        cust_table = cust_table.merge(whs, on=['CUST_KP_NO'], how='left').rename(columns={'LOCATION':'CUST_LOCATION'})
        cust_table = cust_table.merge(aps, on=['WO'], how='left').rename(columns={'LINE':'WO_LOCATION'})
        
        return cust_table

    # 載入產線庫存資訊
    def output_wip_data(self):
        
        # #Modify    設 LINE_STATE = 1
        modify_stock = modify_table(self.v_wo,self.aps_wo,self.aps_wo2,self.cust_list,self.allparts_db,self.area)
        # #未modify   設 LINE_STATE = 3
        unmodify_stock = unmodify_table(self.cust_list, self.aps_wo2, self.allparts_db, self.area, self.WO_HEAD)
        # #未modify kitting   設 LINE_STATE = 2
        unmodify_kit = unmodify_kitting(self.cust_list, self.realtime_PO_table, unmodify_stock, self.aps_wo2, self.allparts_db)
        # 合併modify & unmodify
        wip_stock = pd.concat([unmodify_stock, modify_stock, unmodify_kit], sort=False).reset_index(drop=True)
        if len(wip_stock)>0:
            wip_stock['AREA'] = wip_stock['LINE'].str[:4]
            wip_stock['BUILD'] = wip_stock['LINE'].str[:2]
            wip_stock['FLOOR'] = wip_stock['LINE'].str[2:4]
        return wip_stock

    # 載入倉庫庫存訊息
    def output_stock_data(self, allparts_db):
        
        # 倉庫庫存        
        today_whs = self.whs_LOCATION.copy()
        
        # 結合stock表 (需求)
        stock_cust_qty = self.track_cust_table['CUST_KP_NO'].drop_duplicates().reset_index(drop = True).to_frame()

        # 計算 Normal QTY
        today_whs_normal = today_whs[today_whs['PRINT_TYPE'].isin(['0', '1'])].reset_index(drop=True)
    
        if len(today_whs_normal):    
            whs_normal_qty = self.cal_whs_cust_qty(today_whs_normal)
            stock_cust_qty = stock_cust_qty.merge(whs_normal_qty, on='CUST_KP_NO', how='left')    
            stock_cust_qty['CUST_QTY'] = stock_cust_qty['CUST_QTY'].fillna(0)
            stock_cust_qty['CUST_QTY_ODC'] = stock_cust_qty['CUST_QTY_ODC'].fillna(0)
        else:
            stock_cust_qty['CUST_QTY'] = 0
            stock_cust_qty['CUST_QTY_ODC'] = 0
    
        # 計算 control run, stock_cust_qty併入control run庫存
        today_whs_controlrun = today_whs[(today_whs['PRINT_TYPE'] == '2')].reset_index(drop=True)
    
        if len(today_whs_controlrun):
            C_qty = self.cal_whs_cust_qty(today_whs_controlrun)
            C_qty = C_qty.rename(columns={'CUST_QTY': 'CONTROLRUN_QTY', 'CUST_QTY_ODC': 'CONTROLRUN_QTY_ODC'})
            stock_cust_qty = stock_cust_qty.merge(C_qty, on=['CUST_KP_NO'], how='left')
            stock_cust_qty['CONTROLRUN_QTY'] = stock_cust_qty['CONTROLRUN_QTY'].fillna(0)
            stock_cust_qty['CONTROLRUN_QTY_ODC'] = stock_cust_qty['CONTROLRUN_QTY_ODC'].fillna(0)
        else:
            stock_cust_qty['CONTROLRUN_QTY'] = 0
            stock_cust_qty['CONTROLRUN_QTY_ODC'] = 0
    
        # 計算 deviation, stock_cust_qty併入deviation庫存
        today_whs_deviation = today_whs[(today_whs['PRINT_TYPE'] == '3')].reset_index(drop=True)
        
        # 載入 DEVIATION 資訊
        deviation_wo = deviation_wo_table(allparts_db)
        today_whs_deviation = today_whs_deviation.merge(deviation_wo, left_on=['CUST_KP_NO', 'PRINT_WO'], right_on=['CUST_KP_NO', 'DEVIATION_NO'], how = 'left')
        
        if len(today_whs_deviation) > 0:
            
            # DEV不匹配
            D1 = today_whs_deviation[today_whs_deviation['DEVIATION_NO'].isna()]
            D1_qty = self.cal_whs_cust_qty(D1)
            D1_qty = D1_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            D1_qty['DEV_TYPE'] = 0
            
            # DEV匹配
            today_whs_deviation = today_whs_deviation[today_whs_deviation['DEVIATION_NO'].notna()]
            
            # DEV過期            
            D2 = today_whs_deviation[today_whs_deviation['DEV_EXPIRED']==True]
            D2_qty = self.cal_whs_cust_qty(D2)
            D2_qty = D2_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            D2_qty['DEV_TYPE'] = 1

            # DEV沒過期
            D3 = today_whs_deviation[today_whs_deviation['DEV_EXPIRED']==False]
            D3_qty = self.cal_whs_cust_qty(D3)
            D3_qty = D3_qty.rename(columns={'CUST_QTY': 'DEVIATION_QTY', 'CUST_QTY_ODC': 'DEVIATION_QTY_ODC'})
            D3_qty['DEV_TYPE'] = 2

            D_qty = pd.concat([D1_qty, D2_qty, D3_qty], sort=False).reset_index(drop=True)
            stock_cust_qty = stock_cust_qty.merge(D_qty, on=['CUST_KP_NO'], how='left')            
            stock_cust_qty['DEVIATION_QTY'] = stock_cust_qty['DEVIATION_QTY'].fillna(0)
            stock_cust_qty['DEVIATION_QTY_ODC'] = stock_cust_qty['DEVIATION_QTY_ODC'].fillna(0)
        else:
            stock_cust_qty['DEVIATION_QTY'] = 0
            stock_cust_qty['DEVIATION_QTY_ODC'] = 0
                                
        # 載入 IQC
        stock_cust_qty = stock_cust_qty.merge(self.iqc_stock, on='CUST_KP_NO', how='left').fillna(0)
        
        #載入 IPQ & ROHS & NO_AVL 
        whs_ipq, whs_rohs, whs_noavl = rohs_table(self.allparts_db, self.area, self.cust_list)

        if len(whs_ipq)>0:
            whs_ipq = whs_ipq.groupby(['CUST_KP_NO'])['IPQ_QTY'].sum().to_frame()
            stock_cust_qty = stock_cust_qty.merge(whs_ipq, on=['CUST_KP_NO'], how = 'left')
        else:
            stock_cust_qty['IPQ_QTY'] = 0
        if len(whs_rohs)>0:
            whs_rohs = whs_rohs.groupby(['CUST_KP_NO'])['RoHS_QTY'].sum().to_frame()
            stock_cust_qty = stock_cust_qty.merge(whs_rohs, on=['CUST_KP_NO'], how = 'left')
        else:
            stock_cust_qty['RoHS_QTY'] = 0
        if len(whs_noavl)>0:
            whs_noavl = whs_noavl.groupby(['CUST_KP_NO'])['NOAVL_QTY'].sum().to_frame()
            stock_cust_qty = stock_cust_qty.merge(whs_noavl, on=['CUST_KP_NO'], how = 'left')
        else:
            stock_cust_qty['NOAVL_QTY'] = 0
            
        stock_cust_qty['Clearance_QTY'] = 0  #  暫時性
        stock_cust_qty = stock_cust_qty.fillna(0)
                
        return stock_cust_qty


## FUNCTION ##
# 找發料最佳決策  必須大於目標且最接近目標
def find_nearest_group(df, target):
    objective_num = []
    objective_abs = []
    objective_group = []
    objective_index = []

    for i in range(len(df)):
        df_copy = df.copy()
        sum_value = df.loc[df.index[i], 'QTY']
        last = target - sum_value
        num_list = [df.loc[df.index[i], 'QTY']]
        num_index = [df.index[i]]
        df_copy = df_copy.drop(df.index[i])
        while sum_value < target:
            df_copy['abs_error'] = df_copy.QTY.apply(lambda x : abs(x - last))
            df_copy = df_copy.sort_values(['abs_error', 'QTY'])
            sum_value += df_copy.iloc[0, 0]
            num_list.append(df_copy.loc[df_copy.index[0], 'QTY'])

            num_index.append(df_copy.index[0])
            # num_index = [num_index,]

            df_copy = df_copy.drop([df_copy.index[0]])
            last = target - sum_value
        objective_num.append(sum_value)
        objective_abs.append(abs(sum_value - target))
        objective_group.append(num_list)
        objective_index.append(num_index)

    index_of_best = objective_abs.index(min(objective_abs))
    
    # 回傳 最接近值 和 組合
    return objective_num[index_of_best], objective_group[index_of_best], objective_index[index_of_best]


# wip 依規則 分配並記錄
def allocation_function(target_df, source_df, target_table, wip_stock_v2):
    
    global result_table5
    temp_qty = int(target_df['SHORTAGE_QTY'])
    
    # 當資源池大於需求 表示可以供完需求 (有些盤分完；有些盤分後會有剩)
    if (source_df['QTY'].sum() > int(target_df['SHORTAGE_QTY'])):
        deliver_index = find_nearest_group(pd.DataFrame(source_df.loc[:, ('QTY')]), int(target_df['SHORTAGE_QTY']))[2]
        
        for j in deliver_index:
            temp_qty -= source_df.loc[j, ('QTY')]
            
            if temp_qty > 0:
                result_temp = target_table.copy()
                result_temp['TR_SN'] = source_df.loc[j, 'TR_SN']
                result_temp['FROM_LOCATION'] = source_df.loc[j, 'FROM_LOCATION']
                result_temp['GIVEN_QTY'] = source_df.loc[j, 'QTY']
                result_temp['FROM_BUILD'] = source_df.loc[j, 'BUILD']
                result_temp['FROM_FLOOR'] = source_df.loc[j, 'FLOOR']
                result_temp['LINE_STATE'] = source_df.loc[j, 'LINE_STATE']
                result_table5 = result_table5.append(result_temp,sort=False)
                wip_stock_v2.loc[j, 'can_use'] = 0
                
            else:
                result_temp = target_table.copy()
                result_temp['TR_SN'] = source_df.loc[j, 'TR_SN']
                result_temp['FROM_LOCATION'] = source_df.loc[j, 'FROM_LOCATION']
                result_temp['GIVEN_QTY'] = source_df.loc[j, 'QTY'] + temp_qty
                result_temp['FROM_BUILD'] = source_df.loc[j, 'BUILD']
                result_temp['FROM_FLOOR'] = source_df.loc[j, 'FLOOR']
                result_temp['LINE_STATE'] = source_df.loc[j, 'LINE_STATE']
                wip_stock_v2.loc[j, 'QTY'] = abs(temp_qty)
                result_table5 = result_table5.append(result_temp,sort=False)
        temp_qty = 0
    # 當資源池小於需求 表示無法供完需求 (全盤分給需求，要更新需求)
    else:
        
        for j in source_df.index :
            temp_qty -= source_df.loc[j, ('QTY')]
            result_temp = target_table.copy()
            result_temp['TR_SN'] = source_df.loc[j, 'TR_SN']
            result_temp['FROM_LOCATION'] = source_df.loc[j, 'FROM_LOCATION']
            result_temp['GIVEN_QTY'] = source_df.loc[j, 'QTY']
            result_temp['FROM_BUILD'] = source_df.loc[j, 'BUILD']
            result_temp['FROM_FLOOR'] = source_df.loc[j, 'FLOOR']
            result_temp['LINE_STATE'] = source_df.loc[j, 'LINE_STATE']
            result_table5 = result_table5.append(result_temp,sort=False)
            wip_stock_v2.loc[j, 'can_use'] = 0
            
    return temp_qty

# predict find cust table : for normal and Controlrun order
def generate_table(table_0, aps_wo2, track_cust_table, wip_stock, stock_cust_qty):
    
    
    # 正常和Control工單，倉庫物料去除Deviation資訊，重新調整
    dev_qty = stock_cust_qty.groupby(['CUST_KP_NO'])['DEVIATION_QTY', 'DEVIATION_QTY_ODC'].sum().reset_index()
    stock_cust_qty = stock_cust_qty.drop(columns=['DEVIATION_QTY','DEVIATION_QTY_ODC','DEV_TYPE'], axis=1).drop_duplicates() 
    stock_cust_qty = stock_cust_qty.merge(dev_qty, on = 'CUST_KP_NO')

    # Output format
    output_cols = ['WO', 'V_WO', 'CUST_KP_NO', 'REQUEST_QTY', 'DELIEVER_QTY', 'SHORTAGE_QTY', 'SOURCE_LOCATION', 'FROM_WO', 'FROM_SOURCE',
                   'CAN_USE_QTY', 'GIVEN_QTY', 'SPECIAL_CASE', 'STOP_LINE_POINT', 'CUST_LOCATION', 'WO_LOCATION']
    
    final_result_table = pd.DataFrame(columns=output_cols)
    
    if not len(table_0):
        return final_result_table
    
    # =============================================================================
    #     1. 倉庫正常物料是否存在ODC - 用ODC補
    # =============================================================================
    
    table_1 = table_0.copy()
    
    if len(table_1)>0 : 
        
        # 正常工單或是controlrun工單
        QTY_TYPE = 'CUST_QTY_ODC' if table_1['SPECIAL_CASE'].sum() == 0 else 'CONTROLRUN_QTY_ODC'
    
        table_1 = table_1.merge(pd.DataFrame(stock_cust_qty, columns = ['CUST_KP_NO', QTY_TYPE]), on='CUST_KP_NO', how='left')
        table_1['cumsum'] = table_1.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        table_1['normal_QTY_pre'] = table_1[QTY_TYPE] - table_1['cumsum']
        table_1['SHORTAGE_QTY_v2'] = table_1.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
    
        # Output
        table_1_fin = table_1[(table_1['normal_QTY_pre']>= 0) | (table_1['SHORTAGE_QTY_v2']<table_1['SHORTAGE_QTY'])]

        if len(table_1_fin)>0:            
            
            table_1_fin['GIVEN_QTY'] = table_1_fin.apply( lambda x: min(x[QTY_TYPE] , x.SHORTAGE_QTY) ,axis=1)
            table_1_fin.loc[table_1_fin['normal_QTY_pre']<0, 'GIVEN_QTY'] = table_1_fin.loc[table_1_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_1_fin.loc[table_1_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            table_1_fin = table_1_fin.rename(columns={QTY_TYPE:'CAN_USE_QTY'})
            
            final_table1 = pd.DataFrame(table_1_fin, columns=output_cols)    
            final_table1['FROM_SOURCE'] = 1
            # final_table1['SOURCE_LOCATION'] = '倉庫有old D/C物料'
            final_table1['SOURCE_LOCATION'] = '倉庫date code過期庫存'
            final_result_table = pd.concat([final_result_table, final_table1], sort=False).reset_index(drop=True)
        
        # 修正缺料數量
        table_1['SHORTAGE_QTY'] = table_1['SHORTAGE_QTY_v2']
    

    # =============================================================================
    #     2. 倉庫是否有品質異常庫存 - 用品質異常補
    # =============================================================================
    
    table_2 = table_1.copy() 
    table_2 = table_2[table_2['normal_QTY_pre']<0]
    
    if len(table_2)>0:
        table_2['cumsum'] = table_2.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        stock_cust_qty['lock_qty'] = stock_cust_qty['IPQ_QTY']+stock_cust_qty['RoHS_QTY']+stock_cust_qty['NOAVL_QTY']
        table_2 = table_2.merge(pd.DataFrame(stock_cust_qty, columns = ['CUST_KP_NO','lock_qty']), on='CUST_KP_NO', how='left')
        table_2['normal_QTY_pre'] = table_2['lock_qty'] - table_2['cumsum']
        table_2['SHORTAGE_QTY_v2'] = table_2.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
        
        # Output
        table_2_fin = table_2[(table_2['normal_QTY_pre']>= 0) | (table_2['SHORTAGE_QTY_v2']<table_2['SHORTAGE_QTY'])]
        
        if len(table_2_fin)>0:            
            table_2_fin['GIVEN_QTY'] = table_2_fin.apply( lambda x: min(x.lock_qty , x.SHORTAGE_QTY) ,axis=1)
            table_2_fin.loc[table_2_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_2_fin.loc[table_2_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_2_fin.loc[table_2_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            table_2_fin = table_2_fin.rename(columns={'lock_qty':'CAN_USE_QTY'})
            final_table2 = pd.DataFrame(table_2_fin, columns=output_cols)
            final_table2['FROM_SOURCE'] = 2
            final_table2['SOURCE_LOCATION'] = 'HOLD ROHS/NO-AVL/IPQ 庫存'
            # final_table2['SOURCE_LOCATION'] = '庫存有品質異常物料'
            final_result_table = pd.concat([final_result_table, final_table2], sort=False).reset_index(drop=True)
    
        # 修正缺料數量
        table_2['SHORTAGE_QTY'] = table_2['SHORTAGE_QTY_v2']
        

    # =============================================================================
    #     3. 正常工單 - 倉庫是否有正常庫存 - 用正常庫存補
    #     3. Controlrun 工單 -倉庫是否有Controlrun庫存 - 用Controlrun庫存補
    # =============================================================================
    
    table_3 = table_2.copy()
    table_3 = table_3[table_3['normal_QTY_pre']<0]
    
    if len(table_3)>0:
        
        # 正常工單或是controlrun工單
        QTY_TYPE = 'CUST_QTY' if table_1['SPECIAL_CASE'].sum() == 0 else 'CONTROLRUN_QTY'
        
        table_3 = table_3.merge(pd.DataFrame(stock_cust_qty, columns = ['CUST_KP_NO', QTY_TYPE]), on='CUST_KP_NO', how='left')
        table_3['cumsum'] = table_3.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        table_3['normal_QTY_pre'] = table_3[QTY_TYPE] - table_3['cumsum']
        table_3['SHORTAGE_QTY_v2'] = table_3.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
                    
        # Output
        table_3_fin = table_3[(table_3['normal_QTY_pre']>= 0) | (table_3['SHORTAGE_QTY_v2']<table_3['SHORTAGE_QTY'])]
        
        if len(table_3_fin)>0:        
            table_3_fin['GIVEN_QTY'] = table_3_fin.apply(lambda x: min(x[QTY_TYPE] , x.SHORTAGE_QTY) ,axis=1)
            table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            table_3_fin = table_3_fin.rename(columns={QTY_TYPE:'CAN_USE_QTY'})
            final_table3 = pd.DataFrame(table_3_fin, columns=output_cols)
            final_table3['FROM_SOURCE'] = 3
            
            if table_3['SPECIAL_CASE'].sum() == 0: # 正常工單
                final_table3['SOURCE_LOCATION'] = '倉庫有正常庫存'
                
            else: # Controlrun、Divation 
                final_table3['SOURCE_LOCATION'] = '倉庫有管控物料'
                
            final_result_table = pd.concat([final_result_table, final_table3], sort=False).reset_index(drop=True)
        
        # 修正缺料數量
        table_3['SHORTAGE_QTY'] = table_3['SHORTAGE_QTY_v2']
        
    # =============================================================================
    #     緊急插入 Controlrun 限定 : 倉庫是否有正常庫存 - 用正常庫存補
    # =============================================================================
    
    if table_0['SPECIAL_CASE'].sum() != 0:
        
        table_3_2 = table_3.copy()
        table_3_2 = table_3_2[table_3_2['normal_QTY_pre']<0]

        if len(table_3_2)>0:   
            
            temp = track_cust_table[track_cust_table['SPECIAL_CASE']!=0]
            temp = pd.DataFrame(temp,columns=['WO', 'V_WO','CUST_KP_NO'])
            table_3_2 = table_3_2.merge(temp, on=['WO', 'V_WO','CUST_KP_NO'], how='left')
            table_3_2 = table_3_2.drop_duplicates()
            table_3_2['cumsum'] = table_3_2.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
            stock_cust_qty['normal_qty'] = stock_cust_qty['CUST_QTY_ODC']+stock_cust_qty['CUST_QTY']
            table_3_2 = table_3_2.merge(pd.DataFrame(stock_cust_qty, columns=['CUST_KP_NO','normal_qty']), on='CUST_KP_NO', how='left')
            table_3_2['normal_QTY_pre'] = table_3_2['normal_qty'] - table_3_2['cumsum']
            table_3_2['SHORTAGE_QTY_v2'] = table_3_2.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
            
            # Output
            table_3_2_fin = table_3_2[(table_3_2['normal_QTY_pre']>= 0) | (table_3_2['SHORTAGE_QTY_v2']<table_3_2['SHORTAGE_QTY'])]
            
            if len(table_3_2_fin)>0:
                
                table_3_2_fin['GIVEN_QTY'] = table_3_2_fin.apply( lambda x: min(x.normal_qty , x.SHORTAGE_QTY) ,axis=1)
                table_3_2_fin = table_3_2_fin.rename(columns={'normal_qty':'CAN_USE_QTY'})
                    
                table_3_2_fin.loc[table_3_2_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_3_2_fin.loc[table_3_2_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_3_2_fin.loc[table_3_2_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
                final_table3_2 = pd.DataFrame(table_3_2_fin, columns=output_cols)
                final_table3_2['FROM_SOURCE'] = 6
        
                final_table3_2.loc[:,'SOURCE_LOCATION'] = '倉庫有正常庫存'
                    
                final_result_table = pd.concat([final_result_table, final_table3_2], sort=False).reset_index(drop=True)
        
            # 修正缺料數量
            table_3_2['SHORTAGE_QTY'] = table_3_2['SHORTAGE_QTY_v2']
            
            
    # =============================================================================
    #     4. IQC是否有庫存 - 用IQC庫存補
    # =============================================================================
    
    if table_0['SPECIAL_CASE'].sum() == 0:
        table_4 = table_3.copy()
    else:
        table_4 = table_3_2.copy()
        
    table_4 = table_4[table_4['normal_QTY_pre']<0]
    
    if len(table_4)>0:        
        
        table_4 = table_4.merge(pd.DataFrame(stock_cust_qty, columns = ['CUST_KP_NO','IQC_CAN_USE']), on='CUST_KP_NO', how='left')
        table_4['cumsum'] = table_4.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        table_4['normal_QTY_pre'] = table_4['IQC_CAN_USE'] - table_4['cumsum']
        table_4['SHORTAGE_QTY_v2'] = table_4.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

        # Output
        table_4_fin = table_4[(table_4['normal_QTY_pre']>= 0) | (table_4['SHORTAGE_QTY_v2']<table_4['SHORTAGE_QTY'])]
        
        if len(table_4_fin)>0:
            table_4_fin['GIVEN_QTY'] = table_4_fin.apply( lambda x: min(x.IQC_CAN_USE , x.SHORTAGE_QTY) ,axis=1)
            table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            table_4_fin = table_4_fin.rename(columns={'IQC_CAN_USE':'CAN_USE_QTY'})
            final_table4 = pd.DataFrame(table_4_fin, columns=output_cols)
            final_table4['FROM_SOURCE'] = 4
            # final_table4['SOURCE_LOCATION'] = 'IQC有庫存'
            final_table4['SOURCE_LOCATION'] = 'IQC正常庫存'
            final_result_table = pd.concat([final_result_table, final_table4], sort=False).reset_index(drop=True)

        # 修正缺料數量
        table_4['SHORTAGE_QTY'] = table_4['SHORTAGE_QTY_v2']
        
        
    # =============================================================================
    #     5. 產線是否有庫存 - 用產線庫存補
    # =============================================================================

    table_5 = table_4.copy()
    table_5 = table_5[table_5['normal_QTY_pre']<0]
        
    if len(table_5)>0:
                
        #加入 AREA 資訊
        table_5 = table_5.merge(aps_wo2, left_on=['WO'], right_on = ['FROM_LOCATION'], how='left')
        table_5['BUILD'] = table_5['LINE'].str[:2]
        table_5['FLOOR'] = table_5['LINE'].str[2:4]
        table_5 = pd.DataFrame(table_5.loc[:, ('WO', 'V_WO', 'CUST_KP_NO','STOP_LINE_POINT',
                               'SHORTAGE_QTY','BUILD','FLOOR', 'CUST_LOCATION', 'WO_LOCATION')])
    
        wip_stock_v2 = wip_stock.drop(columns=['OVER_TIME','END_TIME','LINE','AREA'])
        wip_stock_v2['can_use'] = 1
    
        # 定義分配結果df ，分配結果會在result_table5上
        global result_table5 
        result_table5 = pd.DataFrame(columns=['WO', 'V_WO', 'CUST_KP_NO', 'STOP_LINE_POINT', 'SHORTAGE_QTY', 'BUILD', 'FLOOR',
                'TR_SN', 'FROM_LOCATION', 'GIVEN_QTY', 'FROM_BUILD', 'FROM_FLOOR', 'LINE_STATE', 'CUST_LOCATION', 'WO_LOCATION'])
        
        # 將table5的需求 全部去wip stock內找
        for m in table_5['CUST_KP_NO'].drop_duplicates():
            cust_group = table_5[table_5['CUST_KP_NO']==m]
            for n in range(len(cust_group)):
                target_table = cust_group[cust_group.index==cust_group.index[n]]
                target_temp = target_table.copy()
                build = cust_group.loc[cust_group.index[n],'BUILD']
                floor = cust_group.loc[cust_group.index[n],'FLOOR']
                wip_cust_stock = wip_stock_v2[(wip_stock_v2['CUST_KP_NO']==m) &( wip_stock_v2['can_use']==1)]
                                
                # =============================================================================
                # 狀況分成三種 : 同棟同樓、同棟不同樓、不同棟
                # 線體分成三種 : 1-Modify、2-Unmodify KITTING、3-Unmodify LINE
                # =============================================================================
                
                # 同棟同樓
                same_b_f = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']==floor)&(wip_cust_stock['LINE_STATE']==1)]
                if len(same_b_f)>0:
                    remain_shortage = allocation_function(target_temp, same_b_f, target_table, wip_stock_v2)
                    target_temp['SHORTAGE_QTY'] = remain_shortage
                
                same_b_f = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']==floor)&(wip_cust_stock['LINE_STATE']==2)]
                if len(same_b_f)>0:
                    remain_shortage = allocation_function(target_temp, same_b_f, target_table, wip_stock_v2)
                    target_temp['SHORTAGE_QTY'] = remain_shortage
               
                same_b_f = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']==floor)&(wip_cust_stock['LINE_STATE']==3)]
                if len(same_b_f)>0:
                    remain_shortage = allocation_function(target_temp, same_b_f, target_table, wip_stock_v2)
                    target_temp['SHORTAGE_QTY'] = remain_shortage
                
                # 同棟不同樓
                same_b = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']!=floor)&(wip_cust_stock['LINE_STATE']==1)]
                if len(same_b)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0:
                        remain_shortage = allocation_function(target_temp, same_b, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
                same_b = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']!=floor)&(wip_cust_stock['LINE_STATE']==2)]
                if len(same_b)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0:
                        remain_shortage = allocation_function(target_temp, same_b, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
                same_b = wip_cust_stock[(wip_cust_stock['BUILD']==build)&(wip_cust_stock['FLOOR']!=floor)&(wip_cust_stock['LINE_STATE']==3)]
                if len(same_b)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0:
                        remain_shortage = allocation_function(target_temp, same_b, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
                        
                # 不同棟
                unsame_b_f = wip_cust_stock[(wip_cust_stock['BUILD']!=build)&(wip_cust_stock['LINE_STATE']==1)]
                if len(unsame_b_f)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0 :
                        remain_shortage = allocation_function(target_temp, unsame_b_f, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
                unsame_b_f = wip_cust_stock[(wip_cust_stock['BUILD']!=build)&(wip_cust_stock['LINE_STATE']==2)]
                if len(unsame_b_f)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0 :
                        remain_shortage = allocation_function(target_temp, unsame_b_f, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
                unsame_b_f = wip_cust_stock[(wip_cust_stock['BUILD']!=build)&(wip_cust_stock['LINE_STATE']==3)]
                if len(unsame_b_f)>0:
                    if int(target_temp['SHORTAGE_QTY'])>0 :
                        remain_shortage = allocation_function(target_temp, unsame_b_f, target_table, wip_stock_v2)
                        target_temp['SHORTAGE_QTY'] = remain_shortage
    
        after_allocated = result_table5.groupby(['WO','CUST_KP_NO'])['SHORTAGE_QTY','GIVEN_QTY'].apply(lambda x : max(x.SHORTAGE_QTY) - sum(x.GIVEN_QTY)).reset_index()
        
        if len(after_allocated) > 0:
            table_5 = table_5.merge(after_allocated,on=['WO','CUST_KP_NO'],how='left')
            table_5.loc[~table_5[0].isna(),'SHORTAGE_QTY'] = table_5.loc[~table_5[0].isna(),0]
            table_5 = table_5.drop(columns=[0]).drop(table_5[table_5['SHORTAGE_QTY']==0].index)
        
        # 分配名細 => result_table5
        if table_0['SPECIAL_CASE'].sum() == 0: # 正常工單
            temp = track_cust_table[track_cust_table['SPECIAL_CASE']==0]
            temp = pd.DataFrame(temp,columns=['WO', 'V_WO','CUST_KP_NO','REQUEST_QTY','DELIEVER_QTY','SPECIAL_CASE'])
            table5_temp = result_table5.merge(temp, on=['WO','V_WO','CUST_KP_NO'], how='left')
        
        else:  # Controlrun、Divation 
            temp = track_cust_table[track_cust_table['SPECIAL_CASE']!=0]
            temp = pd.DataFrame(temp,columns=['WO', 'V_WO','CUST_KP_NO','REQUEST_QTY','DELIEVER_QTY','SPECIAL_CASE'])
            table5_temp = result_table5.merge(temp, on=['WO','V_WO','CUST_KP_NO'], how='left')
        
        if len(table5_temp)>0:
            temp = pd.DataFrame(wip_stock, columns=['TR_SN','FROM_LOCATION','CUST_KP_NO','QTY'])
            table5_temp = table5_temp.merge(temp, on=['TR_SN','FROM_LOCATION','CUST_KP_NO'], how='left')
            table5_temp['SOURCE_LOCATION'] = '產線（' + table5_temp['FROM_BUILD'] + table5_temp['FROM_FLOOR'] + '）'
            table5_temp = table5_temp.rename(columns={'QTY':'CAN_USE_QTY','GIVEN_QTY':'GIVEN_QTY','FROM_LOCATION':'FROM_WO'})
            final_table5 = pd.DataFrame(table5_temp, columns=output_cols)
            final_table5['FROM_SOURCE'] = 5
            final_result_table = pd.concat([final_result_table, final_table5], sort=False).reset_index(drop=True)
    
        del result_table5
        
    # 修正缺料數量
    if not len(table_5):
        return final_result_table

    # =============================================================================
    #     6. 正常工單限定 : 倉庫是否有管控庫存 - 用Controlrun + Deviation庫存補
    # =============================================================================

    table_6 = table_5.copy()

    if len(table_6)>0 and table_0['SPECIAL_CASE'].sum() == 0:
                
        temp = track_cust_table[track_cust_table['SPECIAL_CASE']==0]
        temp = pd.DataFrame(temp,columns=['WO', 'V_WO','CUST_KP_NO','REQUEST_QTY','DELIEVER_QTY','SPECIAL_CASE'])
        table_6 = table_6.merge(temp, on=['WO', 'V_WO','CUST_KP_NO'], how='left')
        table_6['cumsum'] = table_6.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        stock_cust_qty['CONTROL_QTY'] = stock_cust_qty['CONTROLRUN_QTY_ODC']+stock_cust_qty['DEVIATION_QTY_ODC']+stock_cust_qty['CONTROLRUN_QTY']+stock_cust_qty['DEVIATION_QTY']
        table_6 = table_6.merge(pd.DataFrame(stock_cust_qty, columns=['CUST_KP_NO','CONTROL_QTY']), on='CUST_KP_NO', how='left')
        table_6['normal_QTY_pre'] = table_6['CONTROL_QTY'] - table_6['cumsum']
        table_6['SHORTAGE_QTY_v2'] = table_6.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
            
        # Output
        table_6_fin = table_6[(table_6['normal_QTY_pre']>= 0) | (table_6['SHORTAGE_QTY_v2']<table_6['SHORTAGE_QTY'])]
        
        if len(table_6_fin)>0:                        
            
            table_6_fin['GIVEN_QTY'] = table_6_fin.apply( lambda x: min(x.CONTROL_QTY , x.SHORTAGE_QTY) ,axis=1)
            table_6_fin = table_6_fin.rename(columns={'CONTROL_QTY':'CAN_USE_QTY'})            
            
            table_6_fin.loc[table_6_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_6_fin.loc[table_6_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_6_fin.loc[table_6_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            final_table6 = pd.DataFrame(table_6_fin, columns=output_cols)
            final_table6['FROM_SOURCE'] = 6
            final_table6.loc[:,'SOURCE_LOCATION'] = '倉庫有管控物料'
                
            final_result_table = pd.concat([final_result_table, final_table6], sort=False).reset_index(drop=True)
    
        # 修正缺料數量
        table_6['SHORTAGE_QTY'] = table_6['SHORTAGE_QTY_v2']

    # =============================================================================
    #     7. IQC判退
    # =============================================================================

    table_7 = table_6.copy()
    if 'normal_QTY_pre' in table_7.columns:
        table_7 = table_7[table_7['normal_QTY_pre']<0]
        
    if len(table_7)>0:
        table_7['cumsum'] = table_7.groupby(['CUST_KP_NO'])['SHORTAGE_QTY'].cumsum()
        table_7 = table_7.merge(pd.DataFrame(stock_cust_qty, columns = ['CUST_KP_NO','IQC_REJECT_QTY']), on='CUST_KP_NO', how='left')
        table_7['normal_QTY_pre'] = table_7['IQC_REJECT_QTY'] - table_7['cumsum']
        table_7['SHORTAGE_QTY_v2'] = table_7.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
    
        # Output
        table_7_fin = table_7[(table_7['normal_QTY_pre']>= 0) | (table_7['SHORTAGE_QTY_v2']<table_7['SHORTAGE_QTY'])]
        
        if len(table_7_fin)>0:
            table_7_fin['GIVEN_QTY'] = table_7_fin.apply( lambda x: min(x.IQC_REJECT_QTY , x.SHORTAGE_QTY) ,axis=1)
            table_7_fin.loc[table_7_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_7_fin.loc[table_7_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_6_fin.loc[table_6_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
            table_7_fin = table_7_fin.rename(columns={'IQC_REJECT_QTY':'CAN_USE_QTY'})
            final_table7 = pd.DataFrame(table_7_fin, columns=output_cols)
            final_table7['FROM_SOURCE'] = 7
            final_table7['SOURCE_LOCATION'] = 'IQC判退庫存'
            # final_table7['SOURCE_LOCATION'] = '有IQC判退物料'
            final_result_table = pd.concat([final_result_table, final_table7], sort=False).reset_index(drop=True)
        
        # 修正缺料數量
        table_7['SHORTAGE_QTY'] = table_7['SHORTAGE_QTY_v2']
        
    # =============================================================================
    #     8. SAP缺料
    # =============================================================================

    table_8 = table_7.copy()
    table_8 = table_8[table_8['normal_QTY_pre']<0]
    
    if len(table_8)>0:
        table_8['GIVEN_QTY'] = 0
        table_8['CAN_USE_QTY'] = 0
        final_table8 = pd.DataFrame(table_8, columns=output_cols)
        final_table8['FROM_SOURCE'] = 8
        final_table8['SOURCE_LOCATION'] = 'SAP缺料'
        final_result_table = pd.concat([final_result_table, final_table8], sort=False).reset_index(drop=True)

    return final_result_table
 
# predict find cust table : for Deviation order
def generate_table_dev(table_0, aps_wo2, track_cust_table, wip_stock, stock_cust_qty):
    
    # DEV是否ODC相加   
    stock_cust_qty['DEVIATION_TOTAL'] = stock_cust_qty['DEVIATION_QTY_ODC'] + stock_cust_qty['DEVIATION_QTY'] 
    
    
    # Output format
    output_cols = ['WO', 'V_WO', 'CUST_KP_NO', 'REQUEST_QTY', 'DELIEVER_QTY', 'SHORTAGE_QTY', 'SOURCE_LOCATION', 'FROM_WO', 'FROM_SOURCE',
                   'CAN_USE_QTY', 'GIVEN_QTY', 'SPECIAL_CASE', 'STOP_LINE_POINT', 'CUST_LOCATION', 'WO_LOCATION', 'DEV_EXPIRED']
    
    final_result_table = pd.DataFrame(columns=output_cols)
    
    if not len(table_0):
        return final_result_table
    
    # =============================================================================
    #     1. 倉庫有DEV_NO匹配 : 先用1-1過期DEV 再用1-2未過期DEV
    # =============================================================================
    
    table_1 = table_0.copy()
    
    # 1-1用過期 DEV_TYPE = 1
    stock_cust_qty_dev1 = stock_cust_qty[stock_cust_qty['DEV_TYPE']==1]    
    table_1 = table_1.merge(stock_cust_qty_dev1, on='CUST_KP_NO', how='left')
    
    # normal_QTY_pre >= 0 代表 倉庫物料可滿足累積需求
    table_1['normal_QTY_pre'] = table_1['DEVIATION_TOTAL'] - table_1['cumsum'] 
    table_1['SHORTAGE_QTY_v2'] = table_1.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

    table_1_fin = table_1[(table_1['normal_QTY_pre']>= 0) | (table_1['SHORTAGE_QTY_v2']<table_1['SHORTAGE_QTY'])]

    if len(table_1_fin)>0:        
        table_1_fin['GIVEN_QTY']=0
        table_1_fin.loc[table_1_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_1_fin.loc[table_1_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.DEVIATION_TOTAL , x.SHORTAGE_QTY) ,axis=1)
        table_1_fin.loc[table_1_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_1_fin.loc[table_1_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_1_fin.loc[table_1_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_1_fin.loc[table_1_fin['SPECIAL_CASE']==2,'CAN_USE_QTY'] = table_1_fin.loc[table_1_fin['SPECIAL_CASE']==2,'DEVIATION_TOTAL']
    
        final_table1 = pd.DataFrame(table_1_fin, columns=output_cols)    
        final_table1['FROM_SOURCE'] = 1.1
        final_table1['SOURCE_LOCATION'] = '倉庫Deviation過期庫存' 
        # final_table1['SOURCE_LOCATION'] = '庫存有相匹配的Deviation物料已過有效期' 
        final_result_table = pd.concat([final_result_table, final_table1], sort=False).reset_index(drop=True)
        
    # 更新累積需求 cumsum
    table_1['cumsum_tmp'] = table_1['cumsum']
    table_1['cumsum'] = table_1.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_1 = table_1.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_1['SHORTAGE_QTY'] = table_1.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)

    # 1-2用過期 DEV_TYPE = 1
    table_1_2 = table_1.copy().reset_index(drop=True)
    table_1_2 = table_1_2[(table_1_2['normal_QTY_pre'].isna())|(table_1_2['normal_QTY_pre']<0)]  
    table_1_2 = table_1_2[table_0.columns]
    stock_cust_qty_dev2 = stock_cust_qty[stock_cust_qty['DEV_TYPE']==2]
    table_1_2 = table_1_2.merge(stock_cust_qty_dev2, on='CUST_KP_NO', how='left')

    # normal_QTY_pre >= 0 代表 倉庫物料可滿足累積需求
    table_1_2['normal_QTY_pre'] = table_1_2['DEVIATION_TOTAL'] - table_1_2['cumsum']
    table_1_2['SHORTAGE_QTY_v2'] = table_1_2.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

    table_1_2_fin = table_1_2[(table_1_2['normal_QTY_pre']>= 0) | (table_1_2['SHORTAGE_QTY_v2']<table_1_2['SHORTAGE_QTY'])]

    if len(table_1_2_fin)>0:
        table_1_2_fin['GIVEN_QTY']=0
        table_1_2_fin.loc[table_1_2_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_1_2_fin.loc[table_1_2_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.DEVIATION_TOTAL , x.SHORTAGE_QTY) ,axis=1)
        table_1_2_fin.loc[table_1_2_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_1_2_fin.loc[table_1_2_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_1_2_fin.loc[table_1_2_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_1_2_fin.loc[table_1_2_fin['SPECIAL_CASE']==2,'CAN_USE_QTY'] = table_1_2_fin.loc[table_1_2_fin['SPECIAL_CASE']==2,'DEVIATION_TOTAL']
    
        final_table1_2 = pd.DataFrame(table_1_2_fin, columns=output_cols)    
        final_table1_2['FROM_SOURCE'] = 1.2
        final_table1_2['SOURCE_LOCATION'] = '倉庫管控庫存'
        # final_table1_2['SOURCE_LOCATION'] = '庫存有相匹配的Deviation物料在有效期內'
        final_result_table = pd.concat([final_result_table, final_table1_2], sort=False).reset_index(drop=True)

    
    # 更新累積需求 cumsum
    table_1_2['cumsum_tmp'] = table_1_2['cumsum']
    table_1_2['cumsum'] = table_1_2.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_1_2 = table_1_2.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_1_2['SHORTAGE_QTY'] = table_1_2.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)


    # =============================================================================
    #     2. 產線有DEV_NO匹配 : 暫時跳過
    # =============================================================================
    
    
    table_2 = table_1_2.copy().reset_index(drop=True)
    
    # =============================================================================
    #     3. 倉庫有DEV_NO不匹配 
    # =============================================================================

    table_3 = table_2.copy().reset_index(drop=True)
    table_3 = table_3[(table_3['normal_QTY_pre'].isna())|(table_3['normal_QTY_pre']<0)]
    
    if not len(table_3):
        return final_result_table

    table_3 = table_3[table_0.columns]
    stock_cust_qty_dev_no = stock_cust_qty[~stock_cust_qty['DEV_TYPE'].isin([1,2])]
    table_3 = table_3.merge(stock_cust_qty_dev_no, on='CUST_KP_NO', how='left')


    # normal_QTY_pre >= 0 代表 倉庫物料可滿足累積需求
    table_3['normal_QTY_pre'] = table_3['DEVIATION_TOTAL'] - table_3['cumsum'] 
    table_3['SHORTAGE_QTY_v2'] = table_3.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

    table_3_fin = table_3[(table_3['normal_QTY_pre']>= 0) | (table_3['SHORTAGE_QTY_v2']<table_3['SHORTAGE_QTY'])]

    if len(table_3_fin)>0:        
        table_3_fin['GIVEN_QTY']=0
        table_3_fin.loc[table_3_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_3_fin.loc[table_3_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.DEVIATION_TOTAL , x.SHORTAGE_QTY) ,axis=1)
        table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_3_fin.loc[table_3_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_3_fin.loc[table_3_fin['SPECIAL_CASE']==2,'CAN_USE_QTY'] = table_3_fin.loc[table_3_fin['SPECIAL_CASE']==2,'DEVIATION_TOTAL']
    
        final_table3 = pd.DataFrame(table_3_fin, columns=output_cols)    
        final_table3['FROM_SOURCE'] = 3
        final_table3['SOURCE_LOCATION'] = '倉庫Deviation不匹配庫存'
        # final_table3['SOURCE_LOCATION'] = '庫存有相匹配的Deviation與工單不匹配'
        final_result_table = pd.concat([final_result_table, final_table3], sort=False).reset_index(drop=True)
        
    # 更新累積需求 cumsum
    table_3['cumsum_tmp'] = table_3['cumsum']
    table_3['cumsum'] = table_3.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_3 = table_3.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_3['SHORTAGE_QTY'] = table_3.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)

        
    # =============================================================================
    #     4. 推送非DEVIATION物料 : 正常物料 + Controlrun (不管是否ODC)
    # =============================================================================
    
    # 倉庫物料重新整理(不分DEV_NO)
    dev_qty = stock_cust_qty.groupby(['CUST_KP_NO'])['DEVIATION_QTY', 'DEVIATION_QTY_ODC'].sum().reset_index()
    stock_cust_qty_012 = stock_cust_qty.drop(columns=['DEVIATION_QTY','DEVIATION_QTY_ODC','DEV_TYPE'], axis=1).drop_duplicates() 
    stock_cust_qty_012 = stock_cust_qty_012.merge(dev_qty, on = 'CUST_KP_NO')

    table_4 = table_3.copy().reset_index(drop=True)
    table_4 = table_4[(table_4['normal_QTY_pre'].isna())|(table_4['normal_QTY_pre']<0)]
    
    if not len(table_4):
        return final_result_table

    table_4 = table_4[table_0.columns]
    table_4 = table_4.merge(stock_cust_qty_012, on='CUST_KP_NO', how='left')

    table_4['NON_DEV_QTY'] = table_4['CUST_QTY'] + table_4['CUST_QTY_ODC'] + table_4['CONTROLRUN_QTY'] + table_4['CONTROLRUN_QTY_ODC']
    table_4['normal_QTY_pre'] = table_4['NON_DEV_QTY'] - table_4['cumsum'] 
    table_4['SHORTAGE_QTY_v2'] = table_4.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)
    
    table_4_fin = table_4[(table_4['normal_QTY_pre']>= 0) | (table_4['SHORTAGE_QTY_v2']<table_4['SHORTAGE_QTY'])]

    if len(table_4_fin)>0:        
        table_4_fin['GIVEN_QTY']=0
        table_4_fin.loc[table_4_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_4_fin.loc[table_4_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.NON_DEV_QTY , x.SHORTAGE_QTY) ,axis=1)
        table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_4_fin.loc[table_4_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_4_fin.loc[table_4_fin['SPECIAL_CASE']==2,'CAN_USE_QTY'] = table_4_fin.loc[table_4_fin['SPECIAL_CASE']==2,'NON_DEV_QTY']
    
        final_table4 = pd.DataFrame(table_4_fin, columns=output_cols)    
        final_table4['FROM_SOURCE'] = 4
        final_table4['SOURCE_LOCATION'] = '倉庫正常庫存'
        # final_table4['SOURCE_LOCATION'] = '工單是DEVIATION工單，物料是非DEVIATION物料'
        final_result_table = pd.concat([final_result_table, final_table4], sort=False).reset_index(drop=True)
        
    # 更新累積需求 cumsum
    table_4['cumsum_tmp'] = table_4['cumsum']
    table_4['cumsum'] = table_4.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_4 = table_4.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_4['SHORTAGE_QTY'] = table_4.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)


    # =============================================================================
    #     5. IQC有庫存
    # =============================================================================
    
    table_5 = table_4.copy().reset_index(drop=True)
    table_5 = table_5[(table_5['normal_QTY_pre'].isna())|(table_5['normal_QTY_pre']<0)]
    
    if not len(table_5):
        return final_result_table

    table_5['normal_QTY_pre'] = table_5['IQC_CAN_USE'] - table_5['cumsum'] 
    table_5['SHORTAGE_QTY_v2'] = table_5.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

    table_5_fin = table_5[(table_5['normal_QTY_pre']>= 0) | (table_5['SHORTAGE_QTY_v2']<table_5['SHORTAGE_QTY'])]

    if len(table_5_fin)>0:
        table_5_fin['GIVEN_QTY']=0
        table_5_fin.loc[table_5_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_5_fin.loc[table_5_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.IQC_CAN_USE , x.SHORTAGE_QTY) ,axis=1)
        table_5_fin.loc[table_5_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_5_fin.loc[table_5_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_5_fin.loc[table_5_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_5_fin.loc[table_5_fin['SPECIAL_CASE']==2,'CAN_USE_QTY'] = table_5_fin.loc[table_5_fin['SPECIAL_CASE']==2,'IQC_CAN_USE']
    
        final_table5 = pd.DataFrame(table_5_fin, columns=output_cols)    
        final_table5['FROM_SOURCE'] = 5
        final_table5['SOURCE_LOCATION'] = 'IQC正常庫存'
        # final_table5['SOURCE_LOCATION'] = 'IQC有庫存' 
        final_result_table = pd.concat([final_result_table, final_table5], sort=False).reset_index(drop=True)
        
    # 更新累積需求 cumsum
    table_5['cumsum_tmp'] = table_5['cumsum']
    table_5['cumsum'] = table_5.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_5 = table_5.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_5['SHORTAGE_QTY'] = table_5.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)

    
    # =============================================================================
    #     6. 產線有超發不匹配 : 暫時跳過
    # =============================================================================
    
    table_6 = table_5.copy().reset_index(drop=True)


    # =============================================================================
    #     7. 有IQC判退
    # =============================================================================
    
    table_7 = table_6.copy().reset_index(drop=True)
    table_7 = table_7[(table_7['normal_QTY_pre'].isna())|(table_7['normal_QTY_pre']<0)]
    
    if not len(table_7):
        return final_result_table

    table_7['normal_QTY_pre'] = table_7['IQC_REJECT_QTY'] - table_7['cumsum'] 
    table_7['SHORTAGE_QTY_v2'] = table_7.apply(lambda x: x.SHORTAGE_QTY if(abs(x.normal_QTY_pre)>x.SHORTAGE_QTY) else abs(x.normal_QTY_pre) ,axis = 1)

    table_7_fin = table_7[(table_7['normal_QTY_pre']>= 0) | (table_7['SHORTAGE_QTY_v2']<table_7['SHORTAGE_QTY'])]

    if len(table_7_fin)>0:        
        table_7_fin['GIVEN_QTY']=0
        table_7_fin.loc[table_7_fin['SPECIAL_CASE']==2,'GIVEN_QTY'] = table_7_fin.loc[table_7_fin['SPECIAL_CASE']==2].apply( lambda x: min(x.IQC_REJECT_QTY , x.SHORTAGE_QTY) ,axis=1)
        table_7_fin.loc[table_7_fin['normal_QTY_pre']<0,'GIVEN_QTY'] = table_7_fin.loc[table_7_fin['normal_QTY_pre']<0,'normal_QTY_pre'] + table_7_fin.loc[table_7_fin['normal_QTY_pre']<0,'SHORTAGE_QTY']
        table_7_fin.loc[table_7_fin['SPECIAL_CASE']==2,'IQC_REJECT_QTY'] = table_7_fin.loc[table_7_fin['SPECIAL_CASE']==2,'IQC_CAN_USE']
    
        final_table7 = pd.DataFrame(table_7_fin, columns=output_cols)    
        final_table7['FROM_SOURCE'] = 7
        final_table7['SOURCE_LOCATION'] = 'IQC判退庫存'
        # final_table7['SOURCE_LOCATION'] = '有IQC判退物料'
        final_result_table = pd.concat([final_result_table, final_table7], sort=False).reset_index(drop=True)
        
    # 更新累積需求 cumsum
    table_7['cumsum_tmp'] = table_7['cumsum']
    table_7['cumsum'] = table_7.apply( lambda x: (abs(x.normal_QTY_pre) if x.normal_QTY_pre<0 else 0) if pd.notna(x.normal_QTY_pre) else x.cumsum_tmp, axis=1)
    table_7 = table_7.drop('cumsum_tmp', axis = 1)

    # 更新單筆短缺 SHORTAGE_QTY
    table_7['SHORTAGE_QTY'] = table_7.apply( lambda x: x.SHORTAGE_QTY_v2 if pd.notna(x.SHORTAGE_QTY_v2) else x.SHORTAGE_QTY, axis=1)

    
    # =============================================================================
    #     8. 沒有庫存
    # =============================================================================
    
    table_8 = table_7.copy()
    table_8 = table_8[(table_8['normal_QTY_pre'].isna())|(table_8['normal_QTY_pre']<0)]
    
    if len(table_8)>0:
        table_8['SHORTAGE_QTY'] = table_8['SHORTAGE_QTY_v2']
        table_8['GIVEN_QTY'] = 0
        table_8['CAN_USE_QTY'] = 0
        final_table8 = pd.DataFrame(table_8, columns=output_cols)
        final_table8['FROM_SOURCE'] = 8
        final_table8['SOURCE_LOCATION'] = 'SAP缺料'
        # final_table8['SOURCE_LOCATION'] = '都沒有庫存'
        final_result_table = pd.concat([final_result_table, final_table8], sort=False).reset_index(drop=True)

    return final_result_table