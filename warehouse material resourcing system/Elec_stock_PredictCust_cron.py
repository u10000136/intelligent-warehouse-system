import os
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from stock.WHS.dbio import STOCKMySQLIO
from stock.DB.oracle import BasicORACLE
from stock.config import *
from stock.WHS.stock_out.Electric.Elec_stock_PredictCust_model import find_cust_data, generate_table, generate_table_dev
import warnings

warnings.filterwarnings("ignore", category=DeprecationWarning)

# 選擇Allparts DB


def chose_allpart_db(db , sql, data_to_db=[]):
    AllPart_DB_DE = BasicORACLE(username=Allpart_DE['username'], password=Allpart_DE['password'],
                                hostname=Allpart_DE['hostname'],
                                port=Allpart_DE['port'], db_name=Allpart_DE['db_name'])

    AllPart_DB_F = BasicORACLE(username=Allpart_F['username'], password=Allpart_F['password'],
                                hostname=Allpart_F['hostname'],
                                port=Allpart_F['port'], db_name=Allpart_F['db_name'])
    if db == 'F':
        if len(data_to_db) == 0:
            return AllPart_DB_F.manipulate_db(sql)
        else:
            return AllPart_DB_F.manipulate_db(sql, data_list=data_to_db)
    else:  # D、E area allparts
        if len(data_to_db) == 0:
            return AllPart_DB_DE.manipulate_db(sql)
        else:
            return AllPart_DB_DE.manipulate_db(sql, data_list=data_to_db)


def main(realtime_db, history_db, area, allparts_db, WO_HEAD, db_stock_qty, db_wip_qty, db_predeict_qty):

    # 載入基本DB資訊
    DB = find_cust_data(realtime_db, history_db, area, allparts_db, WO_HEAD)

    # 載入需追蹤工單(realtime packageout)
    track_cust_table = DB.output_track_list()

    # 載入倉庫庫存訊息
    stock_cust_qty = DB.output_stock_data(allparts_db)

    # 載入產線庫存資訊
    wip_stock = DB.output_wip_data().dropna()

    # 產線庫存資訊 : 將同工單 同料且不同盤的數量做總和
    tempsum = wip_stock.groupby(['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA'])[
        'QTY'].sum().reset_index().rename(columns={'QTY': 'temp_qty'})
    wip_stock = wip_stock.drop_duplicates(['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA'])
    wip_stock_sum = wip_stock.merge(tempsum, on=['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA'], how='left')
    wip_stock_sum['QTY'] = wip_stock_sum['temp_qty']
    wip_stock_sum = wip_stock_sum.drop(columns='temp_qty')

    # 工單按照SPECIAL_CASE分成正常、Controlrun、Deviation工單，分別「計算需求」與「建議找料」
    # 正常和Controlrun工單-按照料號累加需求，使用generate_table計算找料建議
    # Deviation工單-按照料號和DEV_NO累加，使用generate_table_dev計算找料建議

    # 工單依停線時間排序，快停線者優先配料
    track_cust_table = track_cust_table.sort_values(['CUST_KP_NO', 'STOP_LINE_POINT']).reset_index(drop=True)

    # 正常工單
    t0 = track_cust_table[track_cust_table['SPECIAL_CASE'] == 0].drop_duplicates(['WO', 'V_WO', 'CUST_KP_NO'])
    t0.loc[t0['SPECIAL_CASE'] == 0, 'cumsum'] = t0.loc[t0['SPECIAL_CASE'] == 0, :].groupby(['CUST_KP_NO'])[
        'SHORTAGE_QTY'].cumsum()
    t0 = t0.sort_values(['CUST_KP_NO', 'STOP_LINE_POINT']).reset_index(drop=True)
    t0['cumsum'] = t0['cumsum'].fillna(0)

    t0_track_cust_table = track_cust_table.drop_duplicates(['WO', 'V_WO', 'CUST_KP_NO', 'REQUEST_QTY'])

    result_table_0 = generate_table(t0, DB.aps_wo2, t0_track_cust_table, wip_stock_sum, stock_cust_qty)
    result_table_0 = result_table_0.fillna('-')

    mask = (result_table_0['FROM_SOURCE'] == 4) | (result_table_0['FROM_SOURCE'] == 5) | (
        result_table_0['FROM_SOURCE'] == 7) | (result_table_0['FROM_SOURCE'] == 8)
    result_table_0.loc[mask , 'CUST_LOCATION'] = '-'

    # Controlrun工單
    t1 = track_cust_table[track_cust_table['SPECIAL_CASE'] == 1].drop_duplicates(['WO', 'V_WO', 'CUST_KP_NO'])
    t1.loc[t1['SPECIAL_CASE'] == 1, 'cumsum'] = t1.loc[t1['SPECIAL_CASE'] == 1, :].groupby(['CUST_KP_NO'])[
        'SHORTAGE_QTY'].cumsum()
    t1 = t1.sort_values(['CUST_KP_NO', 'STOP_LINE_POINT']).reset_index(drop=True)
    t1['cumsum'] = t1['cumsum'].fillna(0)

    result_table_1 = generate_table(t1, DB.aps_wo2, t0_track_cust_table, wip_stock_sum, stock_cust_qty)
    result_table_1 = result_table_1.fillna('-')

    mask = (result_table_1['FROM_SOURCE'] == 4) | (result_table_1['FROM_SOURCE'] == 5) | (
        result_table_1['FROM_SOURCE'] == 7) | (result_table_1['FROM_SOURCE'] == 8)
    result_table_1.loc[mask , 'CUST_LOCATION'] = '-'

    # Deviation工單 - 又分有無DEV_NO(PRINT_DATA)
    t2 = track_cust_table[track_cust_table['SPECIAL_CASE'] == 2]
    t2_ok = t2[t2['PRINT_DATA'].notna()]
    t2_ok.loc[t2_ok['SPECIAL_CASE'] == 2, 'cumsum'] = t2_ok.loc[t2_ok['SPECIAL_CASE'] == 2, :].groupby(['CUST_KP_NO', 'PRINT_DATA'])[
        'SHORTAGE_QTY'].cumsum()

    t2_na = t2[t2['PRINT_DATA'].isna()]
    t2_na.loc[t2_na['SPECIAL_CASE'] == 2, 'cumsum'] = t2_na.loc[t2_na['SPECIAL_CASE'] == 2, :].groupby(['CUST_KP_NO'])[
        'SHORTAGE_QTY'].cumsum()

    t2 = pd.concat([t2_ok, t2_na], sort=False)
    t2 = t2.sort_values(['CUST_KP_NO', 'STOP_LINE_POINT']).reset_index(drop=True)
    t2['cumsum'] = t2['cumsum'].fillna(0)

    result_table_2 = generate_table_dev(t2, DB.aps_wo2, track_cust_table, wip_stock_sum, stock_cust_qty)
    result_table_2 = result_table_2.fillna('-')

    mask = (result_table_2['FROM_SOURCE'] == 5) | (result_table_2['FROM_SOURCE'] == 7) | (result_table_2['FROM_SOURCE'] == 8)
    result_table_2.loc[mask , 'CUST_LOCATION'] = '-'

    # 合併找料建議
    result_table = pd.concat([result_table_0, result_table_1, result_table_2], sort=False).reset_index(drop=True)
    result_table = result_table.drop(columns='DEV_EXPIRED', axis=1)

    # 上傳至 DB
    db = STOCKMySQLIO()

    # =============================================================================
    #     分別寫入 stock_qty(倉庫庫存)、wip_qty(產線庫存)、db_predeict_qty(找料建議)
    # =============================================================================

    data_to_db_1 = []
    data_to_db_2 = []
    data_to_db_3 = []

    # stock_qty
    try:
        # # 取得原表
        # table_sql = 'select RID,CUST_KP_NO FROM %s' % (db_stock_qty)
        # origin_stock_qty = db.return_df(table_sql)

        # # 新舊表取key
        # if len(origin_stock_qty) > 0:
        #     origin_stock_qty['key'] = origin_stock_qty['CUST_KP_NO']
        #     stock_cust_qty['key'] = stock_cust_qty['CUST_KP_NO']

        # 傳入dataframe
        insert_sql_1 = '''
        INSERT IGNORE INTO
            {}
            (CUST_KP_NO,CUST_QTY,CUST_QTY_ODC,CONTROLRUN_QTY,CONTROLRUN_QTY_ODC,DEVIATION_QTY,
            DEVIATION_QTY_ODC,IQC_CAN_USE,IQC_REJECT_QTY,IPQ_QTY,RoHS_QTY,NOAVL_QTY,Clearance_QTY, DEV_TYPE) 
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        on DUPLICATE KEY UPDATE
            CUST_QTY=VALUES(CUST_QTY),
            CUST_QTY_ODC=VALUES(CUST_QTY_ODC),
            CONTROLRUN_QTY=VALUES(CONTROLRUN_QTY),
            CONTROLRUN_QTY_ODC=VALUES(CONTROLRUN_QTY_ODC),
            DEVIATION_QTY=VALUES(DEVIATION_QTY),
            DEVIATION_QTY_ODC=VALUES(DEVIATION_QTY_ODC),
            IQC_CAN_USE=VALUES(IQC_CAN_USE),
            IQC_REJECT_QTY=VALUES(IQC_REJECT_QTY),
            IPQ_QTY=VALUES(IPQ_QTY),
            RoHS_QTY=VALUES(RoHS_QTY),
            NOAVL_QTY=VALUES(NOAVL_QTY),
            Clearance_QTY=VALUES(Clearance_QTY),
            DEV_TYPE =VALUES(DEV_TYPE)
        '''.format(db_stock_qty)

        for _, row in stock_cust_qty.iterrows():
            temp_data = (row['CUST_KP_NO'], row['CUST_QTY'], row['CUST_QTY_ODC'], row['CONTROLRUN_QTY'],
                         row['CONTROLRUN_QTY_ODC'], row['DEVIATION_QTY'], row['DEVIATION_QTY_ODC'],
                         row['IQC_CAN_USE'], row['IQC_REJECT_QTY'], row['IPQ_QTY'], row['RoHS_QTY'],
                         row['NOAVL_QTY'], row['Clearance_QTY'], row['DEV_TYPE'])
            data_to_db_1.append(temp_data)

        # 舊表中沒有出現在新表的資料 必須刪除
        # if len(origin_stock_qty) > 0:
        #     key_list = stock_cust_qty['key'].tolist()
        #     delete_id_list = []
        #     for _, row in origin_stock_qty.iterrows():
        #         if row['key'] not in key_list:
        #             delete_id_list.append(row['RID'])

        #     if len(delete_id_list) > 0:
        #         delete_sql = '''DELETE FROM %s WHERE RID in %s''' % (
        #             db_stock_qty, str(delete_id_list).replace('[', '(').replace(']', ')'))
        #         db.execute_sql(delete_sql)

    except Exception as e:
        print(e)

    # WIP
    try:
        # # 取得原表
        # table_sql = 'select RID, TR_SN, FROM_LOCATION, CUST_KP_NO FROM %s' % (db_wip_qty)
        # origin_wip_qty = db.return_df(table_sql)

        # # 新舊表取key
        # if len(origin_wip_qty) > 0:
        #     origin_wip_qty['key'] = origin_wip_qty['TR_SN'] + origin_wip_qty['FROM_LOCATION'] + origin_wip_qty['CUST_KP_NO']
        #     wip_stock['key'] = wip_stock['TR_SN'] + wip_stock['FROM_LOCATION'] + wip_stock['CUST_KP_NO']

        # 傳入dataframe
        insert_sql_2 = '''
        INSERT IGNORE INTO
            {}
            (TR_SN, FROM_LOCATION, CUST_KP_NO, QTY, LINE_STATE, AREA) 
            VALUES (%s, %s, %s, %s, %s, %s)
        on DUPLICATE KEY UPDATE
            QTY=VALUES(QTY),
            LINE_STATE=VALUES(LINE_STATE),
            AREA=VALUES(AREA)
        '''.format(db_wip_qty)

        for _, row in wip_stock.iterrows():
            temp_data = (row['TR_SN'], row['FROM_LOCATION'], row['CUST_KP_NO'], row['QTY'], row['LINE_STATE'], row['AREA'])
            data_to_db_2.append(temp_data)

        # if len(origin_wip_qty) > 0:
        #     # 舊表中沒有出現在新表的資料 必須刪除
        #     key_list = wip_stock['key'].tolist()
        #     delete_id_list = []
        #     for _, row in origin_wip_qty.iterrows():
        #         if row['key'] not in key_list:
        #             delete_id_list.append(row['RID'])

        #     if len(delete_id_list) > 0:
        #         delete_sql = '''DELETE FROM %s WHERE RID in %s''' % (
        #             db_wip_qty, str(delete_id_list).replace('[', '(').replace(']', ')'))

    except Exception as e:
        print(e)

    # predict_find_cust_db
    try:
        # # 取得原表
        # table_sql = 'select RID, WO, CUST_KP_NO, FROM_WO, FROM_SOURCE FROM %s' % (db_predeict_qty)
        # origin_find_cust = db.return_df(table_sql)

        # # 新舊表取key
        # if len(origin_find_cust) > 0:
        #     origin_find_cust['key'] = origin_find_cust['WO'] + origin_find_cust['CUST_KP_NO'] + \
        #         origin_find_cust['FROM_WO'] + origin_find_cust['FROM_SOURCE'].apply(str)
        #     result_table['key'] = result_table['WO'] + result_table['CUST_KP_NO'] + \
        #         result_table['FROM_WO'] + result_table['FROM_SOURCE'].apply(str)

        # 傳入dataframe
        insert_sql_3 = '''
        INSERT IGNORE INTO
            {}
            (WO, V_WO, CUST_KP_NO, REQUEST_QTY, DELIEVER_QTY, SHORTAGE_QTY, 
            SOURCE_LOCATION, FROM_WO, FROM_SOURCE, CAN_USE_QTY, 
            GIVEN_QTY, SPECIAL_CASE, STOP_LINE_POINT, CUST_LOCATION, WO_LOCATION, DEV_TYPE) 
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        on DUPLICATE KEY UPDATE
            V_WO=VALUES(V_WO),
            REQUEST_QTY=VALUES(REQUEST_QTY),
            DELIEVER_QTY=VALUES(DELIEVER_QTY),
            SHORTAGE_QTY=VALUES(SHORTAGE_QTY),
            SOURCE_LOCATION=VALUES(SOURCE_LOCATION),
            CAN_USE_QTY=VALUES(CAN_USE_QTY),
            GIVEN_QTY=VALUES(GIVEN_QTY),
            SPECIAL_CASE=VALUES(SPECIAL_CASE),
            STOP_LINE_POINT=VALUES(STOP_LINE_POINT),
            CUST_LOCATION=VALUES(CUST_LOCATION),
            WO_LOCATION=VALUES(WO_LOCATION),            
            DEV_TYPE=VALUES(DEV_TYPE)
        '''.format(db_predeict_qty)

        for _, row in result_table.iterrows():
            # Deviation需新增欄位

            DEV_TYPE = 0  # 不匹配、無關

            if row['FROM_SOURCE'] == 1.1:  # 過期
                DEV_TYPE = 1
            elif row['FROM_SOURCE'] == 1.2:  # 未過期
                DEV_TYPE = 2

            temp_data = (row['WO'], row['V_WO'], row['CUST_KP_NO'], row['REQUEST_QTY'], row['DELIEVER_QTY'], row['SHORTAGE_QTY'],
                          row['SOURCE_LOCATION'], row['FROM_WO'], int(row['FROM_SOURCE']), row['CAN_USE_QTY'], row['GIVEN_QTY'],
                         row['SPECIAL_CASE'], row['STOP_LINE_POINT'].strftime('%Y-%m-%d %H:%M:%S'),
                         row['CUST_LOCATION'], row['WO_LOCATION'], DEV_TYPE)
            data_to_db_3.append(temp_data)

        # 舊表中沒有出現在新表的資料 必須刪除
        # if len(origin_find_cust) > 0:
        #     key_list = result_table['key'].tolist()
        #     delete_id_list = []
        #     for _, row in origin_find_cust.iterrows():
        #         if row['key'] not in key_list:
        #             delete_id_list.append(row['RID'])

        #     if len(delete_id_list) > 0:
        #         delete_sql = '''DELETE FROM %s WHERE RID in %s''' % (
        #             db_predeict_qty, str(delete_id_list).replace('[', '(').replace(']', ')'))
        #         db.execute_sql(delete_sql)

    except Exception as e:
        print(e)

    # Update table
    try:
        if data_to_db_1:
            db.execute_sql(f'truncate table {db_stock_qty}')
            db.insert_sql_list(insert_sql_1, data_list=data_to_db_1)

        if data_to_db_2:
            db.execute_sql(f'truncate table {db_wip_qty}')
            db.insert_sql_list(insert_sql_2, data_list=data_to_db_2)

        if data_to_db_3:
            db.execute_sql(f'truncate table {db_predeict_qty}')
            db.insert_sql_list(insert_sql_3, data_list=data_to_db_3)

    except Exception as e:
        print(e)


def upload_allparts(allparts_db, db_predeict_ext, db_wip_ext, realtime_db):
    allpart_recommond_db = 'MES4.R_WHS_MAINTENANCE_SUGGEST'
    # 取得原表
    db = STOCKMySQLIO()
    table_sql = f'''
    SELECT 
        a.*, b.START_TIME
    FROM 
        {db_predeict_ext} a
    LEFT JOIN 
        ( SELECT WO, CUST_KP_NO, START_TIME FROM {realtime_db} ) b 
    ON 
        a.WO = b.WO 
    AND a.CUST_KP_NO = b.CUST_KP_NO
    '''

    D_E_find_cust = db.manipulate_db(table_sql, dtype='DataFrame')

    table_sql = ''' select TR_SN,FROM_LOCATION,CUST_KP_NO,QTY,LINE_STATE,AREA FROM %s''' % (db_wip_ext)
    wip_stock_2 = db.manipulate_db(table_sql, dtype='DataFrame')
    # 將同工單 同料 不同盤 數量做總和
    tempsum = wip_stock_2.groupby(['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA'])[
        'QTY'].sum().reset_index().rename(columns={'QTY': 'temp_qty'})
    wip_stock_sum2 = wip_stock_2.drop_duplicates(['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA']).merge(
        tempsum, on=['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'AREA'], how='left')
    wip_stock_sum2['QTY'] = wip_stock_sum2['temp_qty']
    wip_stock_sum2 = wip_stock_sum2.drop(columns=['temp_qty', 'TR_SN', 'AREA'])

    temp = D_E_find_cust.merge(pd.DataFrame(wip_stock_sum2, columns=['FROM_LOCATION', 'CUST_KP_NO', 'LINE_STATE', 'QTY']),
                               left_on=['FROM_WO', 'CUST_KP_NO', 'CAN_USE_QTY'], right_on=['FROM_LOCATION', 'CUST_KP_NO', 'QTY'], how='left')

    result_table2 = temp.groupby(['V_WO', 'CUST_KP_NO', 'SOURCE_LOCATION', 'FROM_WO']).agg({'SHORTAGE_QTY': 'sum',
                                                                                            'CAN_USE_QTY': 'max', 'GIVEN_QTY': 'sum', 'LINE_STATE': 'max', 'FROM_SOURCE': 'max',
                                                                                            'CUST_LOCATION': 'max', 'WO_LOCATION': 'max', 'SPECIAL_CASE': 'max', 'START_TIME': 'min', 'STOP_LINE_POINT': 'min'}).reset_index()
    result_table2['EDIT_TIME'] = pd.Timestamp.now()
    result_table2 = result_table2.sort_values(['V_WO', 'CUST_KP_NO', 'FROM_SOURCE', 'LINE_STATE'])

    try:

        delete_sql = "DELETE FROM %s" % (allpart_recommond_db)
        # Way1
        insert_sql = '''
        INSERT INTO
            {}
            ( WO, CUST_KP_NO, SHORT_QTY, WO_LOCATION, 
            FIND_LOCATION, CUST_LOCATION, FIND_WO,  
            FIND_QTY, REASON, EDIT_TIME, LINE_STATE, PRINT_TYPE,
            ONLINE_TIME, ESTIMATED_END_TIME)  
            VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14)
        '''.format(allpart_recommond_db)

        # 補上 系統判別原因
        error_type_sql = 'SELECT * FROM NSD_L10_material_electrical_errortype'
        error_type_df = db.return_df(error_type_sql)

        data_to_db = []
        for _, row in result_table2.iterrows():

            REASON = ''
            if row['SPECIAL_CASE'] != 2:  # 正常 + Control
                REASON = error_type_df[error_type_df['FROM_SOURCE'] == row['FROM_SOURCE']]['ERROR_CODE'].values[0]
            else:  # Deviation
                REASON = error_type_df[error_type_df['FROM_SOURCE'] == row['FROM_SOURCE']]['ERROR_CODE_V2'].values[0]
                if row['SOURCE_LOCATION'] == '倉庫管控庫存' :  # 庫存有相匹配的Deviation物料在有效期內
                    REASON = '工單與物料Deviation(QTY:…)匹配，需產線帶sop到倉庫確認領料'

            temp_data = (row['V_WO'], row['CUST_KP_NO'], int(row['SHORTAGE_QTY']),
                         row['WO_LOCATION'], row['SOURCE_LOCATION'], row['CUST_LOCATION'], row['FROM_WO'],
                         int(row['GIVEN_QTY']), REASON, row['EDIT_TIME'], str(row['LINE_STATE']),
                         str(row['SPECIAL_CASE']), row['START_TIME'], row['STOP_LINE_POINT'])
            data_to_db.append(temp_data)

        chose_allpart_db(allparts_db, delete_sql)
        chose_allpart_db(allparts_db, insert_sql, data_to_db)

    except Exception as e:
        print(e)
        print('upload to allparts error!!')


# D
def D10_predictNCust():
    realtime_db = 'NSD_L10_material_package_out_realtime'
    history_db = 'NSD_L10_material_packageout_history'
    area = 'D'
    allparts_db = 'D'
    # WO_HEAD_delete
    WO_HEAD = ('00238400', '00238401', '00238402', '00238403', '000011', '002275')
    db_stock_qty = 'NSD_L10_material_electrical_stock_qty'
    db_wip_qty = 'NSD_L10_material_electrical_wip_qty'
    db_predeict_qty = 'NSD_L10_material_electrical_predict_find_cust'

    db_predeict_ext = 'NSD_L10_DE_material_electrical_predict_find_cust'
    db_wip_ext = 'NSD_L10_DE_material_electrical_wip_qty'

    main(realtime_db, history_db, area, allparts_db, WO_HEAD, db_stock_qty, db_wip_qty, db_predeict_qty)
    realtime_db = 'NSD_L10_DE_material_package_out_realtime'
    # upload_allparts(allparts_db, db_predeict_ext, db_wip_ext, realtime_db)


# E
def E6_predictNCust():
    realtime_db = 'NSD_L10_E_material_package_out_realtime'
    history_db = 'NSD_L10_E_material_packageout_history'
    area = 'E'
    allparts_db = 'E'
    # WO_HEAD_delete
    WO_HEAD = ('002398', '002418', '002411', '002406')
    db_stock_qty = 'NSD_L10_E_material_electrical_stock_qty'
    db_wip_qty = 'NSD_L10_E_material_electrical_wip_qty'
    db_predeict_qty = 'NSD_L10_E_material_electrical_predict_find_cust'

    db_predeict_ext = 'NSD_L10_DE_material_electrical_predict_find_cust'
    db_wip_ext = 'NSD_L10_DE_material_electrical_wip_qty'

    main(realtime_db, history_db, area, allparts_db, WO_HEAD, db_stock_qty, db_wip_qty, db_predeict_qty)
    realtime_db = 'NSD_L10_DE_material_package_out_realtime'
    upload_allparts(allparts_db, db_predeict_ext, db_wip_ext, realtime_db)


# F
def F21_predictNCust():
    realtime_db = 'NSD_L10_F_material_package_out_realtime'
    history_db = 'NSD_L10_F_material_packageout_history'
    area = 'F'
    allparts_db = 'F'
    WO_HEAD = ('002399', '002400')
    db_stock_qty = 'NSD_L10_F_material_electrical_stock_qty'
    db_wip_qty = 'NSD_L10_F_material_electrical_wip_qty'
    db_predeict_qty = 'NSD_L10_F_material_electrical_predict_find_cust'

    main(realtime_db, history_db, area, allparts_db, WO_HEAD, db_stock_qty, db_wip_qty, db_predeict_qty)
