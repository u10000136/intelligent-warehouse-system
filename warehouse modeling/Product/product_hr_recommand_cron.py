import pandas as pd
import math
from datetime import datetime
from stock.WHS.dbio import STOCKMySQLIO
import pandas as pd

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = 1
    return datetime(year, month, day).date()

def get_workhours():

    db = STOCKMySQLIO()
    sql1 = '''
    SELECT
        wd.id AS wd_id,
        wd.all_whs_key_mapping_id AS km_id,
        wd.WORK_HOURS,
        wd.`MONTH`,
        km.SITE,
        km.BU,
        km.WHS_TYPE,
        km.AREA,
        km.MAT_TYPE
    FROM
        all_whs_work_data AS wd
        LEFT JOIN all_whs_key_mapping AS km ON km.id = wd.all_whs_key_mapping_id
    WHERE
        ( km.WHS_TYPE = "L10成品倉" AND km.AREA IN ( "D10", "E6", "F21" ) OR km.WHS_TYPE = "L5成品倉" )
    ORDER BY
        wd.all_whs_key_mapping_id,
        wd.`MONTH`
    '''

    sql2 = '''
        SELECT  reve20.`MONTH`, sum_revenue16, sum_revenue17, sum_revenue18 , sum_revenue19  from
        (SELECT  reve16.`MONTH`, reve16.sum_revenue16, reve17.sum_revenue17, reve18.sum_revenue18   from
        (SELECT temp.`MONTH`, sum(temp.`REVENUE`) as sum_revenue16 FROM
        (
        select * FROM all_bu_revenue
        union all
        select * from all_bu_revenue_pred
        ) as temp
        where SUB_BU IN ('MFG1','MFG2', 'MFG8')
        GROUP  by `MONTH`
         ) as reve16
        INNER join
        (SELECT temp.`MONTH`, sum(temp.`REVENUE`) as sum_revenue17 FROM
        (
        select * FROM all_bu_revenue
        union all
        select * from all_bu_revenue_pred
        ) as temp
        where SUB_BU IN ('MFG3', 'MFG6', 'MFG7')
        GROUP  by `MONTH`
         ) as reve17
         INNER join
         (SELECT temp.`MONTH`, sum(temp.`REVENUE`) as sum_revenue18 FROM
        (
        select * FROM all_bu_revenue
        union all
        select * from all_bu_revenue_pred
        ) as temp
        where SUB_BU='MFG5'
        GROUP by temp.`MONTH`
         ) as reve18
        ON
        reve16.`MONTH`=reve17.`MONTH`
        and
        reve16.`MONTH`=reve18.`MONTH`
        ORDER  BY  `MONTH`
        ) as reve20
        LEFT join
         (SELECT temp.`MONTH`, sum(temp.`REVENUE`) as sum_revenue19 FROM
        (
        select * FROM all_bu_revenue
        union all
        select * from all_bu_revenue_pred
        ) as temp
        where SUB_BU='NWEI'
        GROUP by temp.`MONTH`
         ) as reve19
        on
        reve20.`MONTH`=reve19.`MONTH`
        ORDER  BY  reve20.`MONTH`
        '''

    sql3 = '''
           select * from all_emp_workdays where BU='NSD'
        '''

    sql4 = '''
           select * from all_emp_workdays where BU='NWE'
        '''

    df1 = db.manipulate_db(sql1, dtype='DataFrame')
    df2 = db.manipulate_db(sql2, dtype='DataFrame')
    df3 = db.manipulate_db(sql3, dtype='DataFrame')
    df4 = db.manipulate_db(sql4, dtype='DataFrame')

    return df1, df2, df3,df4

def insert_recommend(all_work_data):
    db = STOCKMySQLIO()
    insert_sql = '''
    INSERT IGNORE INTO
        stock_db.all_position_recommand
            (SITE, BU, WHS_TYPE, AREA, JOB_TYPE, CLASS, JOB_RANK, JOB_NAME, SIGN_NUM, TIME)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    ON DUPLICATE KEY UPDATE
        SIGN_NUM=VALUES(SIGN_NUM)
    '''
    return db.manipulate_db(insert_sql, data_list=all_work_data)

def human_recommend():
    df1, df2, df3, df4 = get_workhours()

    df16 = df1.loc[df1['AREA'] == 'D10']
    df16 = df16.merge(df3, how='left', left_on=['MONTH'], right_on=['MONTH'])
    df17 = df1.loc[df1['AREA'] == 'E6']
    df17 = df17.merge(df3, how='left', left_on=['MONTH'], right_on=['MONTH'])
    df18 = df1.loc[df1['AREA'] == 'F21']
    df18 = df18.merge(df3, how='left', left_on=['MONTH'], right_on=['MONTH'])
    df19 = df1.loc[df1['km_id'] == 19]
    df19 = df19.merge(df4, how='left', left_on=['MONTH'], right_on=['MONTH'])

    # 倉種16 人力(2020-07-01 ~ 2021-05-01)
    merge_df16 = df16.merge(df2, how='left', left_on=['MONTH'], right_on=['MONTH']).sort_values("MONTH")
    merge_df16.loc[:, 'prob'] = pd.Series(0, index=merge_df16.index)
    merge_df16.loc[:, 'WORK_HOURS_pred'] = pd.Series(0, index=merge_df16.index)
    merge_df16.loc[:, 'Resource'] = pd.Series(0, index=merge_df16.index)

    df16_length = merge_df16.shape[0]
    Resource_List = []

    for i in range(3, df16_length):
        merge_df16.loc[i, 'prob'] = max((merge_df16.loc[i - 3, 'WORK_HOURS'] / merge_df16.loc[i - 3, 'sum_revenue16']),
                                        (merge_df16.loc[i - 2, 'WORK_HOURS'] / merge_df16.loc[i - 2, 'sum_revenue16']),
                                        (merge_df16.loc[i - 1, 'WORK_HOURS'] / merge_df16.loc[i - 1, 'sum_revenue16']))

        merge_df16.loc[i, 'WORK_HOURS_pred'] = merge_df16.loc[i, 'sum_revenue16'] * merge_df16.loc[i, 'prob']
        merge_df16.loc[i, 'Resource'] = math.ceil(merge_df16.loc[i, 'WORK_HOURS_pred'] / merge_df16.loc[i, 'DAYS'] / 8)

    #  倉種16 人力(2020-06-01 ~ 2021-08-01)
    max_val = -1
    for i in range(1, 4):
        t = merge_df16.loc[merge_df16['MONTH'] == str(add_months(datetime.now().date(), -i))]['WORK_HOURS'].squeeze() / \
        merge_df16.loc[merge_df16['MONTH'] == str(add_months(datetime.now().date(), -i))]['sum_revenue16'].squeeze()
        if t > max_val:
            max_val = t

    prob = max_val
    for i in range(1, 4):
        WORK_HOURS_pred16 = df2.loc[df2['MONTH'] == str(add_months(datetime.now().date(), i))]['sum_revenue16'] * prob
        Resource_D10 = WORK_HOURS_pred16 / df3.loc[df3['MONTH'] == str(add_months(datetime.now().date(), i))]['DAYS'].squeeze() / 8
        for i in range(1, 4):
            Resource_List.append(('NSD', 'L10成品倉', 'D10', Resource_D10, str(add_months(datetime.now().date(), i))))

    merge_df16 = merge_df16.drop(['sum_revenue17', 'sum_revenue18', 'sum_revenue19'], axis=1)
    merge_df16 = merge_df16.rename(columns={'sum_revenue16': 'REVENUE'})

    # 倉種17 人力(2020-07-01 ~ 2021-05-01)
    merge_df17 = df17.merge(df2, how='left', left_on=['MONTH'], right_on=['MONTH']).sort_values("MONTH")
    merge_df17.loc[:, 'prob'] = pd.Series(0, index=merge_df17.index)
    merge_df17.loc[:, 'WORK_HOURS_pred'] = pd.Series(0, index=merge_df17.index)
    merge_df17.loc[:, 'Resource'] = pd.Series(0, index=merge_df17.index)
    df17_length = merge_df17.shape[0]
    for i in range(3, df17_length):
        merge_df17.loc[i, 'prob'] = max(merge_df17.loc[i - 3, 'WORK_HOURS'] / merge_df17.loc[i - 3, 'sum_revenue16'],
                                        merge_df17.loc[i - 2, 'WORK_HOURS'] / merge_df17.loc[i - 2, 'sum_revenue16'],
                                        merge_df17.loc[i - 1, 'WORK_HOURS'] / merge_df17.loc[i - 1, 'sum_revenue16'])

        merge_df17.loc[i, 'WORK_HOURS_pred'] = merge_df17.loc[i, 'sum_revenue17'] * merge_df17.loc[i, 'prob']
        merge_df17.loc[i, 'Resource'] = math.ceil(merge_df17.loc[i, 'WORK_HOURS_pred'] / merge_df17.loc[i, 'DAYS'] / 8)

    # 倉種17 人力(2020-06-01 ~ 2021-08-01)
    max_val = -1
    for i in range(1, 4):
        t = merge_df17.loc[merge_df17['MONTH'] == str(add_months(datetime.now().date(), -i))]['WORK_HOURS'].squeeze() / \
        merge_df17.loc[merge_df17['MONTH'] == str(add_months(datetime.now().date(), -i))]['sum_revenue17'].squeeze()
        if t > max_val:
            max_val = t

    prob = max_val

    for i in range(1, 4):
        WORK_HOURS_pred17 = df2.loc[df2['MONTH'] == str(add_months(datetime.now().date(), i))]['sum_revenue17'] * prob
        Resource_E6 = WORK_HOURS_pred17 / df3.loc[df3['MONTH'] == str(add_months(datetime.now().date(), i))]['DAYS'].squeeze() / 8
        for i in range(1, 4):
            Resource_List.append(('NSD', 'L10成品倉','E6', Resource_E6, str(add_months(datetime.now().date(), i))))
    merge_df17 = merge_df17.drop(['sum_revenue16', 'sum_revenue18', 'sum_revenue19'], axis=1)
    merge_df17 = merge_df17.rename(columns={'sum_revenue17': 'REVENUE'})

    # 倉種18 人力(2020-10-01 ~ 2021-05-01)
    merge_df18 = df18.merge(df2, how='left', left_on=['MONTH'], right_on=['MONTH']).sort_values("MONTH")
    merge_df18.loc[:, 'prob'] = pd.Series(0, index=merge_df18.index)
    merge_df18.loc[:, 'WORK_HOURS_pred'] = pd.Series(0, index=merge_df18.index)
    merge_df18.loc[:, 'Resource'] = pd.Series(0, index=merge_df18.index)
    df18_length = merge_df18.shape[0]
    for i in range(3, df18_length):
        prob = max(merge_df18.loc[i - 3, 'WORK_HOURS'] / merge_df18.loc[i - 3, 'sum_revenue16'],
                   merge_df18.loc[i - 2, 'WORK_HOURS'] / merge_df18.loc[i - 2, 'sum_revenue16'],
                   merge_df18.loc[i - 1, 'WORK_HOURS'] / merge_df18.loc[i - 1, 'sum_revenue16'])
        merge_df18.loc[i, 'prob'] = prob
        merge_df18.loc[i, 'WORK_HOURS_pred'] = merge_df18.loc[i, 'sum_revenue18'] * merge_df18.loc[i, 'prob']
        merge_df18.loc[i, 'Resource'] = math.ceil(merge_df18.loc[i, 'WORK_HOURS_pred'] / merge_df18.loc[i, 'DAYS'] / 8)

    # 倉種18 人力(2020-06-01 ~ 2021-08-01)
    max_val = -1
    for i in range(1, 4):
        t = merge_df18.loc[merge_df18['MONTH'] == str(add_months(datetime.now().date(), -i))]['WORK_HOURS'].squeeze() / \
        merge_df18.loc[merge_df18['MONTH'] == str(add_months(datetime.now().date(), -i))]['sum_revenue18'].squeeze()
        if t > max_val:
            max_val = t

    prob = max_val

    for i in range(1, 4):

        WORK_HOURS_pred18 = df2.loc[df2['MONTH'] == str(add_months(datetime.now().date(), i))]['sum_revenue18'] * prob
        Resource_F21 = WORK_HOURS_pred18 / df3.loc[df3['MONTH'] == str(add_months(datetime.now().date(), i))][
            'DAYS'].squeeze() / 8
        for i in range(1, 4):
            Resource_List.append(('NSD', 'L10成品倉', 'F21', Resource_F21, str(add_months(datetime.now().date(), i))))

    merge_df18 = merge_df18.drop(['sum_revenue16', 'sum_revenue17', 'sum_revenue19'], axis=1)
    merge_df18 = merge_df18.rename(columns={'sum_revenue18': 'REVENUE'})

    # 倉種19, 人力(2020-10-01 ~ 2021-06-01)
    merge_df19 = df19.merge(df2, how='left', left_on=['MONTH'], right_on=['MONTH']).sort_values('MONTH')
    merge_df19.dropna(subset=['DAYS'], inplace=True)
    merge_df19 = merge_df19.reset_index(drop = True)
    merge_df19.loc[:, 'prob'] = pd.Series(0, index=merge_df19.index)
    merge_df19.loc[:, 'WORK_HOURS_pred'] = pd.Series(0, index=merge_df19.index)
    merge_df19.loc[:, 'Resource'] = pd.Series(0, index=merge_df19.index)
    df19_length = merge_df19.shape[0]
    for i in range(3, df19_length):
        prob = max(merge_df19.loc[i - 3, 'WORK_HOURS'] / merge_df19.loc[i - 3, 'sum_revenue19'],
                   merge_df19.loc[i - 2, 'WORK_HOURS'] / merge_df19.loc[i - 2, 'sum_revenue19'],
                   merge_df19.loc[i - 1, 'WORK_HOURS'] / merge_df19.loc[i - 1, 'sum_revenue19'])
        merge_df19.loc[i, 'prob'] = prob
        merge_df19.loc[i, 'WORK_HOURS_pred'] = merge_df19.loc[i, 'sum_revenue19'] * merge_df19.loc[i, 'prob']
        merge_df19.loc[i, 'Resource'] = math.ceil(merge_df19.loc[i, 'WORK_HOURS_pred'] / merge_df19.loc[i, 'DAYS'] / 8)

    # 倉種19, 人力(2020-07-01 ~ 2021-09-01)
    Resource_List1 = []
    max_val = -1
    for i in range(1, 4):
        t = merge_df19.loc[merge_df19['MONTH'] == str(add_months(datetime.now().date(), -i))][
                'WORK_HOURS'].squeeze() / \
            merge_df19.loc[merge_df19['MONTH'] == str(add_months(datetime.now().date(), -i))][
                'sum_revenue19'].squeeze()
        if t > max_val:
            max_val = t

    prob = max_val
    for i in range(1, 4):
        WORK_HOURS_pred19 = df2.loc[df2['MONTH'] == str(add_months(datetime.now().date(), i))][
                                'sum_revenue19'] * prob
        Resource_L5 = WORK_HOURS_pred19 / df4.loc[df4['MONTH'] == str(add_months(datetime.now().date(), i))][
            'DAYS'].squeeze() / 8
        for i in range(1, 4):
            Resource_List.append(('NWEI', 'L5成品倉', 'D11', Resource_L5, str(add_months(datetime.now().date(), i))))

    merge_df19 = merge_df19.drop(['sum_revenue16', 'sum_revenue17', 'sum_revenue18'], axis=1)
    merge_df19 = merge_df19.rename(columns={'sum_revenue19': 'REVENUE'})


    result = pd.concat([merge_df16, merge_df17, merge_df18, merge_df19])       
    result = result.drop(['wd_id', 'UPDATE_TIME', 'SITE_y', 'BU_y'], axis=1)

    result = result.drop(['RID', 'km_id', 'WORK_HOURS', 'DAYS', 'REVENUE', 'WORK_HOURS_pred', 'prob'], axis=1)
    result.rename(
        columns={'SITE_x': 'SITE', 'BU_x': 'BU', 'MAT_TYPE': 'JOB_NAME', 'Resource': 'SIGN_NUM', 'MONTH': 'TIME'},
        inplace=True)
    result['JOB_TYPE'] = '倉管員人力'
    result['JOB_RANK'] = '倉管員'
    result['CLASS'] = None

    data_to_db = []
    for i, row in result.iterrows():

        if row['BU'] == 'NWE':
            row['BU'] = 'NWEI'
        temp_data = (row['SITE'], row['BU'], row['WHS_TYPE'], row['AREA'], row['JOB_TYPE'],
                     row['CLASS'], row['JOB_RANK'], row['JOB_NAME'], row['SIGN_NUM'] if row['SIGN_NUM'] != None else 0,
                     row['TIME'].strftime('%Y-%m-%d'))
        data_to_db.append(temp_data)

    for l in Resource_List:
        data_to_db.append(('FOL', l[0], l[1], l[2], '倉管員人力', None, '倉管員', 'product',
                           math.ceil(l[3].squeeze()), l[4]))

    insert_recommend(data_to_db)
