# -*- coding: utf-8 -*-
import os
from datetime import datetime, date
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from impyute.imputation.cs import mice
import xgboost as xgb
from sklearn.metrics import mean_squared_error
from hyperopt import fmin, tpe, hp, space_eval, Trials
from joblib import dump, load
from stock.WHS.dbio import STOCKMySQLIO
from dateutil import relativedelta

    
# 寫入標工 增加月份
def add_months(sourcedate, m):
    sourcedate += relativedelta.relativedelta(months=m)
    return sourcedate.replace(day=1)

# =============================================================================
#  工作量資料處裡
# =============================================================================


# 在排程中， 先讀取當月工作次數df與標準工時表  合併計算工作量
def count_work_type_hours(all_history_workdata, standard_wt_df):
    merge_df = pd.merge(all_history_workdata, standard_wt_df, how='left',
                        left_on=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_TYPE'],
                        right_on=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_TYPE'])
    merge_df.loc[:, 'WORK_HOURS'] = merge_df.WORK_TIMES.multiply(merge_df.STANDARD_WT) / 3600
    return merge_df


# 輸入各工作種類工作量/依料類
def times_2_hours_result(merge_df):
    
    # 處理新月份沒有值的情況
    try:
        result = merge_df.groupby(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH']).sum().reset_index()
        
        # 此result為 A/BC/PCB/PTH個別的工作量
        result = result[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]

        # 選取已經完成工作量的物料(D10E6F21區倉庫/ PCB,PTH以外的物料)
        work_hours_in_db = result[-(result.MAT_TYPE.isin(['PCB', 'PTH'])
                                    & result.AREA.isin(['D10', 'E6', 'F21']))].reset_index(drop=True)
        try:
            # 將PCB與PTH合併計算工作量
            PCB_PTH = result[(result.MAT_TYPE.isin(['PCB', 'PTH']) & result.AREA.isin(['D10', 'E6', 'F21']))]
            PCB_PTH_work_hours = PCB_PTH.groupby(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MONTH']).sum().reset_index()
            PCB_PTH_work_hours.loc[:, 'MAT_TYPE'] = 'PCB+PTH'
            PCB_PTH_work_hours = PCB_PTH_work_hours[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]
        
        except:

            PCB_PTH_work_hours = pd.DataFrame(columns=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH'])

        # 合併A/BC/D9PTH 與 PCB+PTH
        work_hours_result = pd.concat([work_hours_in_db, PCB_PTH_work_hours]).reset_index(drop=True)
        
    except:
        work_hours_result = pd.DataFrame(columns=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH'])
    
    return work_hours_result


# 生成未來四個月工作量
def generate_future_df(work_hours_result_all):
    test = work_hours_result_all.copy()
    AREA_WITH_MAT = test[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE']].drop_duplicates().sort_values(['AREA', 'MAT_TYPE'])

    # 整理成 SITE/ BU/ WHS_TYPE/ AREA/ MAT_TYPE/ MONTH, '入庫', '出庫', '分盤', '盤點
    # area_mat_workhour 各料類各月份工作時間
    ndf = pd.DataFrame(columns=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH'])
    ms = [add_months(test.MONTH.max(), 1), add_months(test.MONTH.max(), 2),
          add_months(test.MONTH.max(), 3), add_months(test.MONTH.max(), 4)]
    for i in ms:
        for site, bu, whs_type, area, mat_type in AREA_WITH_MAT.itertuples(index=False):
            ndf = ndf.append({'SITE': site,
                              'BU': bu,
                              'WHS_TYPE': whs_type,
                              'AREA': area,
                              'MAT_TYPE': mat_type,
                              'MONTH': i}, ignore_index=True)

    pred_work_hours = pd.concat([test, ndf], sort=False).reset_index(drop=True)
    return pred_work_hours


# =============================================================================
# 營收資料處理
# =============================================================================

# 將處營收轉換成區域營收
def MFG_to_zone_revenue(revenue_in_db):

    D_revenue = revenue_in_db[revenue_in_db.SUB_BU.isin(['MFG1', 'MFG2', 'MFG8'])].groupby([
        'SITE', 'BU', 'MONTH']).sum().reset_index()
    D_revenue['AREA'] = 'D'
    D_revenue = D_revenue[['SITE', 'BU', 'AREA', 'MONTH', 'REVENUE']]

    E_revenue = revenue_in_db[revenue_in_db.SUB_BU.isin(['MFG3', 'MFG6', 'MFG7'])].groupby([
        'SITE', 'BU', 'MONTH']).sum().reset_index()
    E_revenue['AREA'] = 'E'
    E_revenue = E_revenue[['SITE', 'BU', 'AREA', 'MONTH', 'REVENUE']]

    F_revenue = revenue_in_db[revenue_in_db.SUB_BU.isin(['MFG5'])].groupby(['SITE', 'BU', 'MONTH']).sum().reset_index()
    F_revenue['AREA'] = 'F'
    F_revenue = F_revenue[['SITE', 'BU', 'AREA', 'MONTH', 'REVENUE']]
    zone_revenue = pd.concat([D_revenue, E_revenue, F_revenue]).reset_index(drop=True)
    zone_revenue['MONTH'] = zone_revenue.MONTH.apply(lambda x: x.date())
    
    return zone_revenue


# 平移營收資料 加入N個月前營收加入df
def generate_6months_revenue(zone_revenue):
    zone_revenue['title_area'] = zone_revenue['AREA']
    zone_revenue = zone_revenue[['SITE', 'BU', 'title_area', 'MONTH', 'REVENUE']]

    df_copy = zone_revenue.copy()

    list_1 = [-1, -2, -3, -4, -5, -6]
    list_2 = ['-1', '-2', '-3', '-4', '-5', '-6', '']
    for i in range(6):
        df_copy['MONTH' + list_2[i]] = df_copy['MONTH'].apply(lambda x: add_months(x, list_1[i]))

        # origin df rename work_hours
        zone_revenue = zone_revenue.rename(columns={'REVENUE' + list_2[i - 1]: 'REVENUE' + list_2[i],
                                                                      'MONTH' + list_2[i - 1]: 'MONTH' + list_2[i]})
        # 將origin df merge進 copy_df 以copy_df 為本位
        df_copy = pd.merge(zone_revenue, df_copy, how='right', left_on=['SITE', 'BU', 'title_area', 'MONTH' + list_2[i]],
                                right_on=['SITE', 'BU', 'title_area', 'MONTH' + list_2[i]])

    # work_hours_result_all
    df_copy.sort_values(['MONTH', 'title_area']).reset_index(drop=True)
    zone_revenue = df_copy[['SITE', 'BU', 'title_area', 'MONTH', 'REVENUE', 'REVENUE-1',
                                   'REVENUE-2', 'REVENUE-3', 'REVENUE-4', 'REVENUE-5', 'REVENUE-6']]
    
    return zone_revenue


# 平移工作量資料 加入N個月前工作量加入df
def generate_6months_work_hour(work_hours):
    df_copy = work_hours.copy()
    list_1 = [-1, -2, -3, -4, -5, -6]
    list_2 = ['-1', '-2', '-3', '-4', '-5', '-6', '']
    
    for i in range(6):
        df_copy['MONTH' + list_2[i]] = df_copy['MONTH'].apply(lambda x: add_months(x, list_1[i]))

        # origin df rename work_hours
        work_hours = work_hours.rename(columns={'WORK_HOURS' + list_2[i - 1]: 'WORK_HOURS' + list_2[i],
                                                                      'MONTH' + list_2[i - 1]: 'MONTH' + list_2[i]})
        # 將origin df merge進 copy_df 以copy_df 為本位
        df_copy = pd.merge(work_hours, df_copy, how='right', left_on=['SITE', 'BU', 'AREA', 'MAT_TYPE', 'WHS_TYPE', 'MONTH' + list_2[i]],
                                right_on=['SITE', 'BU', 'AREA', 'MAT_TYPE', 'WHS_TYPE', 'MONTH' + list_2[i]], sort=True)

    # work_hours_result_all
    df_copy.sort_values(['MONTH', 'AREA']).reset_index(drop=True)
    work_hours_6month = df_copy[['SITE', 'WHS_TYPE', 'AREA', 'BU', 'MAT_TYPE', 'MONTH', 'WORK_HOURS', 'WORK_HOURS-1',
                                   'WORK_HOURS-2', 'WORK_HOURS-3', 'WORK_HOURS-4', 'WORK_HOURS-5', 'WORK_HOURS-6']]
    
    # 在工作量df增加區域欄位 for merge
    cp = work_hours_6month.copy()
    cp.loc[:, 'title_area'] = cp.AREA.apply(lambda x: x[:1])
    
    return cp


# 轉換區域與物料類型(模型用)
def preprocessing_df(all_data):
    df = all_data.copy()
    
    # useless columns
    le_area = LabelEncoder()
    le_area.fit(['D9', 'D10', 'E6', 'F21'])
    le_mat = LabelEncoder()
    le_mat.fit(['A', 'BC', 'PCB+PTH', 'PTH'])

    df.loc[:, 'AREA'] = le_area.transform(df.loc[:, 'AREA'])
    df.loc[:, 'MAT_TYPE'] = le_mat.transform(df.loc[:, 'MAT_TYPE'])
    df.loc[:, 'year'] = df.MONTH.apply(lambda x: x.year)
    df.loc[:, 'month'] = df.MONTH.apply(lambda x: x.month)
    x_columns = ['AREA', 'MAT_TYPE', 'WORK_HOURS-1', 'WORK_HOURS-2', 'WORK_HOURS-3', 'WORK_HOURS-4',
                 'WORK_HOURS-5', 'WORK_HOURS-6', 'REVENUE', 'REVENUE-1', 'REVENUE-2', 'REVENUE-3',
                 'REVENUE-4', 'REVENUE-5', 'REVENUE-6', 'year', 'month']
    
    return df[x_columns]


# 需用generate_6months_work_hour / preprocessing_df
# 輸入工作量df(contains NA) 未來預計營收 開始預測月份
def pred_several_months(model_xgb, pred_work_hours, zone_revenue_6month, test_date=datetime(2020, 9, 1)):

    # load model
    pred_w_hour_c = pred_work_hours.copy()
    ms = pred_w_hour_c[pred_w_hour_c.isnull().any(axis=1)].MONTH.unique()

    i = 0
    while(i < len(ms)):
        pred_work_hours_6month = generate_6months_work_hour(pred_w_hour_c)
        
        # 合併
        pred_all_data = pd.merge(pred_work_hours_6month, zone_revenue_6month, how='left', left_on=[
                                 'SITE', 'BU', 'MONTH', 'title_area'], right_on=['SITE', 'BU', 'MONTH', 'title_area'])
        pred_all_data = pred_all_data.sort_values(['MONTH', 'AREA']).reset_index(drop=True)

        # 轉換變數為label
        pred_all_data = pred_all_data[pred_all_data.MONTH == ms[i]]
        pred_df = preprocessing_df(pred_all_data)

        # 下個月index
        latest_index = pred_df.dropna(axis=0).index

        # 預測下個月資料
        pred_test = model_xgb.predict(pred_df.loc[latest_index, :])
        pred_w_hour_c.loc[latest_index, 'WORK_HOURS'] = pred_test
        i = i + 1
        
    pred_work_hours_6month = generate_6months_work_hour(pred_w_hour_c)
    # 合併
    pred_all_data = pd.merge(pred_work_hours_6month, zone_revenue_6month, how='left', left_on=[
                             'SITE', 'BU', 'MONTH', 'title_area'], right_on=['SITE', 'BU', 'MONTH', 'title_area'])
    pred_all_data = pred_all_data.sort_values(['MONTH', 'AREA']).reset_index(drop=True)
    pred_all_data = pred_all_data[pred_all_data.MONTH >= test_date]
    
    return pred_all_data


# input 預測結果與工作天數
# 將預測工做時數轉換成人數
def to_all_position_recommand(all_data):
    pred_ = all_data[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH', 'PRED_HOURS', 'DAYS']].reset_index()
    whs_num = pd.DataFrame(columns=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'JOB_TYPE',
                                      'CLASS', 'JOB_RANK', 'JOB_NAME', 'SIGN_NUM', 'TIME'])
    for i in range(len(pred_)):
        whs_num.loc[i, 'SITE'] = pred_.loc[i, 'SITE']
        whs_num.loc[i, 'BU'] = pred_.loc[i, 'BU']
        whs_num.loc[i, 'WHS_TYPE'] = pred_.loc[i, 'WHS_TYPE']
        whs_num.loc[i, 'AREA'] = pred_.loc[i, 'AREA']
        whs_num.loc[i, 'JOB_TYPE'] = '倉管員人力'
        whs_num.loc[i, 'CLASS'] = None
        whs_num.loc[i, 'JOB_RANK'] = '倉管員'
        whs_num.loc[i, 'JOB_NAME'] = pred_.loc[i, 'MAT_TYPE']
        whs_num.loc[i, 'SIGN_NUM'] = pred_.loc[i, 'PRED_HOURS'] / pred_.loc[i, 'DAYS'] / 8
        whs_num.loc[i, 'TIME'] = pred_.loc[i, 'MONTH']

    whs_num['SIGN_NUM'] = whs_num['SIGN_NUM'].apply(lambda x: np.ceil(x))
    
    return whs_num


# 重新訓練模型用?
# 以中位數填補歷史沒有的工作量 by AREA by MAT_TYPE
def impute_past_months_work_hours(work_hours_6month):
    df = work_hours_6month.copy()
    # useless columns

    # 整理倉庫位置與所存在料類
    AREA_WITH_MAT = df[['AREA', 'MAT_TYPE']].drop_duplicates().sort_values(['AREA', 'MAT_TYPE'])
    
    # 各倉庫料類工作量中位數
    median_df = df.groupby(["AREA", "MAT_TYPE"])["WORK_HOURS"].median().reset_index()

    # 在區域i料類j時填補其工作量中位數
    for i, j in AREA_WITH_MAT.itertuples(index=False):
        impute_value = median_df.loc[(median_df.AREA == i)
                                     & (median_df.MAT_TYPE == j), :]['WORK_HOURS'].values[0]

        df.loc[(df.AREA == i) & (df.MAT_TYPE == j), :] = df.loc[(df.AREA == i) & (df.MAT_TYPE == j), :].fillna(impute_value)
    
    return df


# =============================================================================
# For retrain
# =============================================================================

# 計算A類每月工作時數
# 填補A類/PCB/PTH 缺失值
def convert_A(merge_mdf_A):
    area_mat_workhour = pd.DataFrame(columns=['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH', '入庫', '出庫', '分盤', '盤點'])

    # 整理倉庫位置與所存在料類
    AREA_WITH_MAT = merge_mdf_A[['AREA', 'MAT_TYPE']].drop_duplicates().sort_values(['AREA', 'MAT_TYPE'])

    # 整理成 SITE/ BU/ WHS_TYPE/ AREA/ MAT_TYPE/ MONTH, '入庫', '出庫', '分盤', '盤點
    # area_mat_workhour 各料類各月份工作時間
    for i, j in AREA_WITH_MAT.itertuples(index=False):
        area_mat_df = merge_mdf_A.loc[(merge_mdf_A.AREA == i) & (merge_mdf_A.MAT_TYPE == j), :]
        temp_df = area_mat_df.pivot(index='MONTH', columns='WORK_TYPE', values='WORK_HOURS').reset_index()
        temp_df.loc[:, 'SITE'] = 'FOL'
        temp_df.loc[:, 'BU'] = 'NSD'
        temp_df.loc[:, 'WHS_TYPE'] = 'L10電子倉'
        temp_df.loc[:, 'AREA'] = i
        temp_df.loc[:, 'MAT_TYPE'] = j
        area_mat_workhour = pd.concat([area_mat_workhour, temp_df], sort=False)
    area_mat_workhour = area_mat_workhour.reset_index(drop=True)
    area_mat_workhour[area_mat_workhour['盤點'].isnull()] = area_mat_workhour[area_mat_workhour['盤點'].isnull()].fillna(0)

    # one-hot encoding 地點/料種
    # 將倉庫位置與料類轉換成多維度參數 比較距離
    one_hot_AREA = pd.get_dummies(area_mat_workhour['AREA'], drop_first=True)
    one_hot_MAT = pd.get_dummies(area_mat_workhour['MAT_TYPE'], drop_first=True)
    area_mat_workhour = pd.concat([area_mat_workhour, one_hot_AREA, one_hot_MAT], axis=1)  # .drop(['Profession'], axis=1)
    
    # 增加入庫外工作量參數
    area_mat_workhour.loc[:, '出庫盤點總和'] = area_mat_workhour[['出庫', '分盤', '盤點']].sum(axis=1)
    area_mat_workhour.set_index(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH'])
    '''填補缺失值↓'''
    
    # 用MICE(多重差補法)填值
    inputation_df = mice(area_mat_workhour.set_index(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH']))
    area_mat_workhour.loc[:, '入庫'] = inputation_df[0]
    '''填補缺失值↑'''
    area_mat_workhour['WORK_HOURS'] = area_mat_workhour[['入庫', '出庫', '分盤', '盤點']].sum(axis=1)

    result = area_mat_workhour[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]

    work_hours_in_db = result[-(result.MAT_TYPE.isin(['PCB', 'PTH'])
                                & result.AREA.isin(['D10', 'E6', 'F21']))].reset_index(drop=True)

    # 將PCB與PTH合併計算工作量
    PCB_PTH = result[(result.MAT_TYPE.isin(['PCB', 'PTH']) & result.AREA.isin(['D10', 'E6', 'F21']))]
    PCB_PTH_work_hours = PCB_PTH.groupby(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MONTH']).sum().reset_index()
    PCB_PTH_work_hours.loc[:, 'MAT_TYPE'] = 'PCB+PTH'
    PCB_PTH_work_hours = PCB_PTH_work_hours[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]

    # 合併A/BC/D9PTH 與 PCB+PTH
    work_hours_result_A = pd.concat([work_hours_in_db, PCB_PTH_work_hours]).reset_index(drop=True)
   
    return work_hours_result_A


# 計算BC類每月工作時數 資料處理
def convert_BC(merge_mdf_BC):
    use_col = ['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH']
    need_cols = ['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']
    work_hours_result_BC = merge_mdf_BC.groupby(use_col).sum().reset_index()[need_cols]
    
    return work_hours_result_BC


# 填補缺失值，轉換成工作量
def training_model_times_2_hours(merge_df):
    merge_mdf = merge_df[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_TYPE', 'MONTH', 'WORK_HOURS']]

    result = merge_df.groupby(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH']).sum().reset_index()
    # 此result為 A/BC/PCB/PTH個別的工作量
    result = result[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]

    # 選取電子倉以外
    work_hours_others = merge_mdf[- (merge_mdf.WHS_TYPE.isin(['L10電子倉']))].reset_index(drop=True)

    work_hours_others = work_hours_others.groupby(['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'MONTH']).sum().reset_index()
    work_hours_others = work_hours_others[['SITE', 'BU', 'WHS_TYPE', 'AREA', 'MAT_TYPE', 'WORK_HOURS', 'MONTH']]

    # 選取電子倉工作
    merge_mdf = merge_mdf[merge_mdf.WHS_TYPE.isin(['L10電子倉'])].reset_index(drop=True)

    # 區分貴重物料與 BC類
    # merge_mdf_A / merge_mdf_BC
    # note： BC類只有 2019-09月到現在資料，
    merge_mdf = merge_mdf[- (merge_mdf.AREA.isin(['F21']) & merge_mdf.MAT_TYPE.isin(['BC']))]
    merge_mdf_A = merge_mdf[-merge_mdf.MAT_TYPE.isin(['BC'])]

    # 去除BC類201909以前資料(資料不足)
    merge_mdf_BC = merge_mdf[merge_mdf.MAT_TYPE.isin(['BC'])]
    merge_mdf_BC = merge_mdf_BC[merge_mdf_BC.MONTH > datetime(2019, 8, 1).date()]

    work_hours_result_A = convert_A(merge_mdf_A)
    work_hours_result_BC = convert_BC(merge_mdf_BC)
    work_hours_result_all = pd.concat([work_hours_result_A, work_hours_result_BC,
                                       work_hours_others], sort=False).reset_index(drop=True)

    return work_hours_result_all


# 轉換區域與物料類型(模型用)
def preprocessing_df_for_training(all_data):
    df = all_data.copy()
    le_area = LabelEncoder()
    le_area.fit(['D9', 'D10', 'E6', 'F21'])
    le_mat = LabelEncoder()
    le_mat.fit(['A', 'BC', 'PCB+PTH', 'PTH'])

    df.loc[:, 'AREA'] = le_area.transform(df.loc[:, 'AREA'])
    df.loc[:, 'MAT_TYPE'] = le_mat.transform(df.loc[:, 'MAT_TYPE'])
    df.loc[:, 'year'] = df.MONTH.apply(lambda x: x.year)
    df.loc[:, 'month'] = df.MONTH.apply(lambda x: x.month)

    return df


# 設定模型參數時所需用function
def interval01(x):
    return 1 if x > 1 else 0


def interval0(x):
    return 0 if x < 0 else x

# =============================================================================
# 排程
# =============================================================================

# 預測pred_month之後人數
# pred_month 為輸入每個月1號str ex:'2020-10-01'
# 雙模型預測未來所需人力
def all_whs_recommand_cron_V2(pred_month, model1, model2):
    try:
        db = STOCKMySQLIO()
        
        # 標工資訊
        s_wt_sql = '''
        SELECT
            k.SITE, k.BU, k.WHS_TYPE, k.AREA, k.MAT_TYPE,
            a.WORK_TYPE, a.STANDARD_WT
        FROM
            stock_db.all_whs_key_mapping k,
            stock_db.all_standard_worktime a
        WHERE
            a.all_whs_key_mapping_id = k.id
        '''
        standard_wt_df = db.return_df(s_wt_sql)

        # 營收資訊
        revenue_sql = '''
        SELECT
            a.SITE, a.BU, a.SUB_BU, a.MONTH, a.REVENUE
        FROM
            (SELECT * FROM stock_db.all_bu_revenue
            UNION
            SELECT * FROM stock_db.all_bu_revenue_pred) a
        '''
        revenue_in_db = db.return_df(revenue_sql)
        
        # 工作量資訊 (NSD L10電子倉 )
        w_sql = '''
        SELECT
            SITE, BU, WHS_TYPE, AREA, MAT_TYPE, WORK_TYPE, WORK_TIMES, MONTH
        FROM
            stock_db.all_whs_key_mapping k,
            stock_db.all_history_workdata a
        WHERE
            a.all_whs_key_mapping_id = k.id AND
            k.BU = 'NSD' AND
            k.WHS_TYPE = 'L10電子倉'
        '''
        
        update_month = ''
        if pred_month:
            pred_date = datetime.strptime(pred_month, '%Y-%m-%d').date()
            update_month = str(add_months(pred_date, -6))
        else:
            update_month = str(add_months(date.today(), -6))
            
        w_sql += f" AND a.MONTH >= '{update_month}'"

        all_history_workdata = db.return_df(w_sql)
        all_history_workdata.loc[:, 'MONTH'] = all_history_workdata.MONTH.apply(lambda x : x.date())

        wd_sql = " SELECT * FROM stock_db.all_emp_workdays WHERE BU = 'NSD' "        
        WORK_DAYS = db.return_df(wd_sql)
        WORK_DAYS.loc[:, 'MONTH'] = WORK_DAYS.MONTH.apply(lambda x : x.date())

        # 合併計算工作量
        merge_df = count_work_type_hours(all_history_workdata, standard_wt_df)
        work_hours_result = times_2_hours_result(merge_df)
        real_ = add_months(date.today(), -1)
        work_hours_result = work_hours_result[work_hours_result.MONTH <= real_]

        # 生成未來
        pred_work_hours = generate_future_df(work_hours_result)
        zone_revenue = MFG_to_zone_revenue(revenue_in_db)
        zone_revenue_6month = generate_6months_revenue(zone_revenue)

        # 生成未來工作輛 加入開始預測月份 as input
        if pred_month:
            pred_date = datetime.strptime(pred_month, '%Y-%m-%d').date()
        else:
            pred_date = add_months(date.today(), 0)
            
        # 預測未來工作量
        pred_all_data_1 = pred_several_months(model1, pred_work_hours, zone_revenue_6month, test_date=pred_date)
        pred_all_data_1 = pred_all_data_1[pred_all_data_1.MONTH >= pred_date]
        pred_df1 = preprocessing_df(pred_all_data_1)
        pred_re1 = model1.predict(pred_df1)

        # 預測未來工作量
        pred_all_data_2 = pred_several_months(model2, pred_work_hours, zone_revenue_6month, test_date=pred_date)
        pred_all_data_2 = pred_all_data_2[pred_all_data_2.MONTH >= pred_date]
        pred_df2 = preprocessing_df(pred_all_data_2)
        pred_re2 = model2.predict(pred_df2)

        pred_result = []
        pred_result.append(pred_re1)
        pred_result.append(pred_re2)
        pred_all_data_1['PRED_HOURS'] = np.mean(pred_result, axis=0)
        
        # 合併工作量與未來天數
        pred_all_data_days = pd.merge(pred_all_data_1, WORK_DAYS, how='left', on=['SITE', 'BU', 'MONTH'])

        # 轉換成資料儲存df
        whs_result = to_all_position_recommand(pred_all_data_days)
        whs_result = whs_result.where(pd.notnull(whs_result), None)

        insert_sql = '''
        INSERT IGNORE INTO
            all_position_recommand
            (SITE, BU, WHS_TYPE, AREA, JOB_TYPE, CLASS, JOB_RANK, JOB_NAME, SIGN_NUM, TIME)
        VALUES
            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        on DUPLICATE KEY UPDATE
            SIGN_NUM=VALUES(SIGN_NUM)
        '''
        data_to_db = []
        for i, row in whs_result.iterrows():
            temp_data = (row['SITE'], row['BU'], row['WHS_TYPE'], row['AREA'], row['JOB_TYPE'],
                         row['CLASS'], row['JOB_RANK'], row['JOB_NAME'], row['SIGN_NUM'] if row['SIGN_NUM'] != None else 0,
                         row['TIME'].strftime('%Y-%m-%d'))
            data_to_db.append(temp_data)
        if data_to_db:
            db.insert_sql_list(insert_sql, data_list=data_to_db)

    except Exception as e :
        print(e)


# 輸出調參所需training 與 testing(valid) 1個月 此版本效果最好
def split_nested_CV(df, split_n=1):
    
    # 排序日期順序 從大排到小
    date_value = np.sort(df.MONTH.unique())[::-1]

    ms = split_n

    use_df = df[df.MONTH <= date_value[ms]]

    valid_date = add_months(date_value[ms], -1)
    train_date = add_months(date_value[ms], -2)

    valid_df = use_df[(use_df.MONTH < valid_date) & (use_df.MONTH >= train_date)]
    train_df = use_df[use_df.MONTH < train_date]

    x_columns = ['AREA', 'MAT_TYPE', 'WORK_HOURS-1', 'WORK_HOURS-2', 'WORK_HOURS-3', 'WORK_HOURS-4',
                 'WORK_HOURS-5', 'WORK_HOURS-6', 'REVENUE', 'REVENUE-1', 'REVENUE-2', 'REVENUE-3',
                 'REVENUE-4', 'REVENUE-5', 'REVENUE-6', 'year', 'month']
    y_columns = ['WORK_HOURS']

    x_train = train_df[x_columns]
    y_train = train_df[y_columns]

    x_valid = valid_df[x_columns]
    y_valid = valid_df[y_columns]

    return x_train, y_train, x_valid, y_valid


# 輸出真正training df 與 testing 3個月分 (目前這版效果最好)
def for_modeling_split_data(df, split_n=1):
    ms = split_n
    date_value = np.sort(df.MONTH.unique())[::-1]

    train_date = add_months(date_value[ms], -1)
    test_date = add_months(date_value[0], -2)

    test_df = df[df.MONTH >= test_date]
    train_df = df[-(df.MONTH >= train_date)]

    x_columns = ['AREA', 'MAT_TYPE', 'WORK_HOURS-1', 'WORK_HOURS-2', 'WORK_HOURS-3', 'WORK_HOURS-4',
                 'WORK_HOURS-5', 'WORK_HOURS-6', 'REVENUE', 'REVENUE-1', 'REVENUE-2', 'REVENUE-3',
                 'REVENUE-4', 'REVENUE-5', 'REVENUE-6', 'year', 'month']
    y_columns = ['WORK_HOURS']

    x_train = train_df[x_columns]
    y_train = train_df[y_columns]

    x_test = test_df[x_columns]
    y_test = test_df[y_columns]

    return x_train, y_train, x_test, y_test


def rmse(y, y_pred):
    return np.sqrt(mean_squared_error(y, y_pred))


# evaluation_for tuning
def evaluation_nested_CV(params):
    xgb_params = {'colsample_bytree': interval01(float(params['colsample_bytree'])),
                  'gamma': interval0(params['gamma']),
                  'learning_rate': params['learning_rate'],
                  'max_depth': int(params['max_depth']),
                  'min_child_weight': interval0(params['min_child_weight']),
                  'n_estimators': int(params['n_estimators']),
                  'reg_alpha': interval0(params['reg_alpha']),
                  'reg_lambda': interval0(params['reg_lambda']),
                  'subsample': params['subsample'],
                  'silent': 1,
                  'random_state': 7,
                  'nthread': -1}
    df = pd.DataFrame.from_dict(params['df'])
    model_xgb = xgb.XGBRegressor(**xgb_params)
    x_train_1, y_train_1, x_valid_1, y_valid_1 = split_nested_CV(df, split_n=params['nested_CV'])
    model_xgb.fit(x_train_1, y_train_1)
    pred_cv1 = model_xgb.predict(x_valid_1)
    score_nestedCV1 = rmse(y_valid_1, pred_cv1)

    return score_nestedCV1


# 更新模型
def update_xgboost_model():

    try:
        db = STOCKMySQLIO()

        # 標工資訊
        s_wt_sql = '''
        SELECT
            k.SITE, k.BU, k.WHS_TYPE, k.AREA, k.MAT_TYPE,
            a.WORK_TYPE, a.STANDARD_WT
        FROM
            stock_db.all_whs_key_mapping k,
            stock_db.all_standard_worktime a
        WHERE
            a.all_whs_key_mapping_id = k.id
        '''
        standard_wt_df = db.return_df(s_wt_sql)

        # 營收資訊
        revenue_sql = '''
        SELECT
            a.SITE, a.BU, a.SUB_BU, a.MONTH, a.REVENUE
        FROM
            (SELECT * FROM stock_db.all_bu_revenue
            UNION
            SELECT * FROM stock_db.all_bu_revenue_pred) a
        '''
        revenue_in_db = db.return_df(revenue_sql)

        # 工作量資訊 (NSD L10電子倉 )
        w_sql = '''
        SELECT
            k.SITE, k.BU, k.WHS_TYPE, k.AREA, k.MAT_TYPE,
            a.WORK_TYPE, a.WORK_TIMES, a.MONTH
        FROM
            stock_db.all_whs_key_mapping k,
            stock_db.all_history_workdata a
        WHERE
            a.all_whs_key_mapping_id = k.id AND
            k.BU = 'NSD' AND
            k.WHS_TYPE = 'L10電子倉'
        '''

        all_history_workdata = db.return_df(w_sql)
        all_history_workdata.loc[:, 'MONTH'] = all_history_workdata.MONTH.apply(lambda x : x.date())

        # 工作天數資訊
        wd_sql = "SELECT * FROM stock_db.all_emp_workdays"  
        WORK_DAYS = db.return_df(wd_sql)
        WORK_DAYS.loc[:, 'MONTH'] = WORK_DAYS.MONTH.apply(lambda x : x.date())

        # 合併計算工作量
        merge_df = count_work_type_hours(all_history_workdata, standard_wt_df)

        # input 工作次數與標工合併df
        # output 工作量
        real_ = add_months(date.today(), -1)        
        work_hours_result_all = training_model_times_2_hours(merge_df)
        work_hours_result_all = work_hours_result_all[work_hours_result_all.MONTH <= real_]

        # 生成六個月工作量
        work_hours_6month = generate_6months_work_hour(work_hours_result_all)
        
        # 填補過去六月工作量
        work_hours_6month = impute_past_months_work_hours(work_hours_6month)

        # 生成六個月營收
        zone_revenue = MFG_to_zone_revenue(revenue_in_db)
        zone_revenue_6month = generate_6months_revenue(zone_revenue)

        all_data = pd.merge(work_hours_6month, zone_revenue_6month, how='left', left_on=[
                            'SITE', 'BU', 'MONTH', 'title_area'], right_on=['SITE', 'BU', 'MONTH', 'title_area'])
        all_data = all_data.sort_values(['MONTH', 'AREA']).reset_index(drop=True)

        df = preprocessing_df_for_training(all_data)

        searching_space = {
            'colsample_bytree' : hp.normal('colsample_bytree', 0.5, 0.1),
            'gamma' : hp.normal('gamma', 0.05, 0.01),
            'learning_rate' : hp.uniform('learning_rate', 0.04, 0.07),
            'max_depth' : hp.quniform('max_depth', 3, 5, 1),
            'min_child_weight' : hp.normal('min_child_weight', 1.65, 0.1),
            'n_estimators' : hp.quniform('n_estimators', 1500, 2500, q=200),
            'reg_alpha' : hp.normal('reg_alpha', 0.5, 0.1),
            'reg_lambda' : hp.normal('reg_lambda', 0.85, 0.1),
            'subsample' : hp.uniform('subsample', 0.5, 0.7),
            'nested_CV' : 0,
            'df': df.to_dict()
        }
        
        # 訓練第一模型
        searching_space['nested_CV'] = 1
        trials_1 = Trials()
        best1 = fmin(fn=evaluation_nested_CV,
                     space=searching_space,
                     algo=tpe.suggest,
                     max_evals=100, trials=trials_1)

        xgb_params = best1
        xgb_params['colsample_bytree'] = interval01(xgb_params['colsample_bytree'])
        xgb_params['gamma'] = interval0(xgb_params['gamma'])
        xgb_params['min_child_weight'] = interval0(xgb_params['min_child_weight'])
        xgb_params['reg_alpha'] = interval0(xgb_params['reg_alpha'])
        xgb_params['reg_lambda'] = interval0(xgb_params['reg_lambda'])

        xgb_params['max_depth'] = int(xgb_params['max_depth'])
        xgb_params['n_estimators'] = int(xgb_params['n_estimators'])

        train_best_model1 = xgb.XGBRegressor(**xgb_params, silent=1,
                                             random_state=7, nthread=-1)
        x_train_1, y_train_1, x_test, y_test = for_modeling_split_data(df, split_n=1)
        train_best_model1.fit(x_train_1, y_train_1)

        # 訓練第一模型
        searching_space['nested_CV'] = 2
        trials_2 = Trials()
        best2 = fmin(fn = evaluation_nested_CV,
                     space = searching_space,
                     algo = tpe.suggest,
                     max_evals = 100,
                     trials = trials_2)

        xgb_params = best2
        xgb_params['colsample_bytree'] = interval01(xgb_params['colsample_bytree'])
        xgb_params['gamma'] = interval0(xgb_params['gamma'])
        xgb_params['min_child_weight'] = interval0(xgb_params['min_child_weight'])
        xgb_params['reg_alpha'] = interval0(xgb_params['reg_alpha'])
        xgb_params['reg_lambda'] = interval0(xgb_params['reg_lambda'])
        xgb_params['max_depth'] = int(xgb_params['max_depth'])
        xgb_params['n_estimators'] = int(xgb_params['n_estimators'])

        train_best_model2 = xgb.XGBRegressor(**xgb_params, silent=1, random_state=7, nthread=-1)
        x_train, y_train, x_test, y_test = for_modeling_split_data(df, split_n=2)
        train_best_model2.fit(x_train, y_train)

        # 儲存模型
        module_dir = os.path.dirname(__file__)
        file_path1 = os.path.join(module_dir, 'xgb_model1.joblib')
        file_path2 = os.path.join(module_dir, 'xgb_model2.joblib')
        dump(train_best_model1, file_path1)
        dump(train_best_model2, file_path2)

    except Exception as e :
        print(e)

def update_recommand_model_and_table():
    
    # NSD L10電子倉
    try :
        # 更新模型
        update_xgboost_model()

        # 讀取模型
        module_dir = os.path.dirname(__file__)
        file_path1 = os.path.join(module_dir, 'xgb_model1.joblib')
        file_path2 = os.path.join(module_dir, 'xgb_model2.joblib')
        xgb_model1, xgb_model2 = load(file_path1), load(file_path2)

        # 預測建議人力
        all_whs_recommand_cron_V2(pred_month='', model1=xgb_model1, model2=xgb_model2)

        print('ok')
        
    except Exception as e:
        print(str(e))
