# Intelligent Warehouse System



## Overview 
### Overview link：
https://drive.google.com/file/d/1srl6aYS4Ny0-j9of9-OMBJBitnZV9zij/view?usp=sharing

* Show manpower distribution of warehouses for each business unit
* Show realtime summarized working data of warehouses

### Storage location optimization：
https://drive.google.com/file/d/19p1bNkukBexv4bk61SpHvmmRldJGKFuD/view?usp=sharing
https://drive.google.com/file/d/1kW8-OD_lENUcQIrD3fF9MUM9jGcFmwqQ/view?usp=sharing

* Display storage optimization results(Origin location and optimized location of part number)
* Filter by frequency of use or ratio, and optimize commonly used materials to storage locations close to the aisle.

### Material Resourcing system：
https://drive.google.com/file/d/15tNxSOS9nuWqzKNpgP_XFHQz68v-RrQC/view?usp=sharing

* Display work order shortage in order of urgency
* Standardize the material finding process and establish an algorithm(Shows the reason for the shortage)
![](https://i.imgur.com/v11WlLy.png)
* Show the Material resoucing result(Where to find/ How much to get/ Whom to reach)


### Actual and recommanded Manpower of each warehouse(warehouse modeling)：
https://drive.google.com/file/d/1UesYXf_Seo0uMuqkNlHg_-Eu7dvPe_6F/view?usp=sharing

* Build the workload prediction model to evaluate future manpower of each warehouse
* Use XGboost to build the model and use Nested Cross Validation to robust its accuracy
![](https://i.imgur.com/MO8EeTE.png)
